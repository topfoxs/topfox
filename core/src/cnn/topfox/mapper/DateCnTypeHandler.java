package cnn.topfox.mapper;//package com.tiulo.config;


import cnn.topfox.data.DateTime;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DateCnTypeHandler extends BaseTypeHandler<DateTime> {
    /**
    * 设置参数
    *  Java类型-----> 数据库类型
    */
    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, DateTime parameter, JdbcType jdbcType) throws SQLException {
        ps.setString(i,parameter.toString());
    }


    /**
    * 处理查询
    * 将数据库的信息结果----> Java类型
    */
    @Override
    public DateTime getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return getDateCn(rs.getString(columnName));
    }

    @Override
    public DateTime getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return getDateCn(rs.getString(columnIndex));
    }

    @Override
    public DateTime getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return getDateCn(cs.getString(columnIndex));
    }


    private DateTime getDateCn(String data){
        if (data==null || "".equals(data)){
            return null;
        }
        return new DateTime(data);
    }
}