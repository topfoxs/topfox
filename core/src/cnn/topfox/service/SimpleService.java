package cnn.topfox.service;

import cnn.topfox.common.*;
import cnn.topfox.data.DataHelper;
import cnn.topfox.data.DbState;
import cnn.topfox.data.Field;
import cnn.topfox.data.TableInfo;
import cnn.topfox.sql.Condition;
import cnn.topfox.sql.EntityDelete;
import cnn.topfox.sql.EntitySelect;

import cnn.topfox.misc.BeanUtil;
import cnn.topfox.misc.Misc;
import cnn.topfox.mapper.BaseMapper;
import cnn.topfox.misc.ReflectUtils;
import cnn.topfox.util.CheckData;
import cnn.topfox.util.DataCache;
import cnn.topfox.util.KeyBuild;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.binding.BindingException;
import org.mybatis.spring.MyBatisSystemException;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Slf4j
public class SimpleService<M extends BaseMapper<DTO>, DTO extends DataDTO>
        extends SuperService<DTO>
        implements IService<DTO>
{
    @Autowired(required = false) protected SqlSessionTemplate sqlSessionTemplate;
    @Autowired(required = false) protected M baseMapper;

    /**
     * 是否 是 自增主键
     */
    protected Boolean isAutoKeyId=false;

    /**
     * 初始化之前, 类库要做的事情
     * @param listUpdate 更新时,  对DTO的处理用
     */
    @Override
    public void beforeInit(List<?> listUpdate){
        super.beforeInit(listUpdate);

        restSessionHandler.getEntitySql(clazzDTO()); //SQL构造类
        afterInit();
        if (listUpdate != null){
            List<DTO> listUpdate2 = new ArrayList<>();
            for (Object dto : listUpdate){
                listUpdate2.add((DTO)dto);
            }
            initOriginData(listUpdate2);
        }
    }

    private Class<M> clazzMapper;
    private final Class<M> clazzMapper(){
        //保证初始化 只执行一次
        if (clazzMapper == null) {
            clazzMapper = ReflectUtils.getClassGenricType(getClass(), 0); //获得当前 Mapper 的 Class
        }
        return clazzMapper;
    }

    /**
     * 返回 一级 二级缓存处理类
     * @return
     */
    @Override
    public DataCache dataCache() {
        return restSessionHandler.getDataCache();
    }

    /**
     * 创建一个新的条件对象Condition
     * com.topfox.sql.Condition
     * @return
     */
    @Override
    public Condition where(){
        return Condition.create(clazzDTO());
    }

//    protected void beforeInsert(List<DTO> list){
//
//    }
//    protected void beforeUpdate(List<DTO> list){
//
//    }

    protected void beforeSave2(List<DTO> list, String dbState){
        //得到多主键字段
        for (DTO dto : list){
            //插入不检查主键为空, 因为可能是自增主键
            if (DbState.UPDATE.equals(dbState) || DbState.DELETE.equals(dbState)){
                //获得实体主键值, 并检查存在空值则报错
                dto.dataIdAndCheckNull();
            }

            dto.dataState(dbState);
            if (DbState.INSERT.equals(dbState) && dto.dataVersion()==null){
                dto.dataVersion(1);
            }
        }
        if (fillDataHandler != null && (DbState.INSERT.equals(dbState) || DbState.UPDATE.equals(dbState))) {
            //调用"填充接口实现类的方法", 例如:创建和修改 人/时间 字段
            fillDataHandler.fillData(tableInfo().getFillFields(), (List<DataDTO>) list);
        }

//        if (DbState.INSERT.equals(dbState)){
//            beforeInsert(list);
//        }
//        if (DbState.UPDATE.equals(dbState)){
//            beforeUpdate(list);
//        }
    }
    /**
     * 增 删 改 sql执行之后 调用本方法
     * @param list
     */
    protected void afterSave2(List<DTO> list, String dbState){
        if (sysConfig().isRedisCache()){
            Map<String, Field> fieldsForIncremental = tableInfo().getFieldsForIncremental();//注解为 自增减的字段 的 值 特殊处理
            Map<String, Field> fieldsAll = tableInfo().getFields();//注解为 自增减的字段 的 值 特殊处理
            //缓存处理
            for (DTO dto : list){
                boolean isSaveReids = true;
                if (DbState.UPDATE.equals(dto.dataState())){
                    if (sysConfig().isSelectByBeforeUpdate()==false && sysConfig().getUpdateMode()== 1 ){
                        dataCache().deleteRedis(tableInfo(), dto.dataId());//删除redis缓存
                        isSaveReids=false;
                        //return;
                    }

                    if (dto.origin()==null){
                        DTO redisDTO1 = dataCache().get2CacheById(clazzDTO(), dto.dataId(), sysConfig(), "afterSave2");
                        if (redisDTO1 == null || fieldsForIncremental.size() > 0){
                            dataCache().deleteRedis(tableInfo(), dto.dataId());//没有原始数据,则必须要 删除redis缓存
                            isSaveReids=false;
                            //return;  //后台 直接 new 的dto, 缓存也没有, 则更新后不放入缓存. 因为要去查一次才能获得原始数据
                        }else {
                            dto.addOrigin(redisDTO1);//将Redis的数据设置为原始值
                        }
                    }


                    //DTO redisDTO2 = sysConfig().getUpdateMode()==1?dto.merge():dto;
                    DTO redisDTO2 = (sysConfig().getUpdateMode()>1 || dto.origin()==null) ? dto : dto.merge();
                    //对递增减的所有字段循环
                    for (Map.Entry<String, Field> entry : fieldsForIncremental.entrySet()){
                        String fieldName = entry.getKey(); Field fieldIncremental=entry.getValue();
                        Field field = fieldsAll.get(fieldIncremental.getDbName());
                        if (field == null){
                            throw TopException.newInstance(ResponseCode.SYS_FIELD_ISNOT_EXIST).text("递增减的原始字段不存在");
                        }
                        Number changeValue = DataHelper.parseDoubleByNull2Zero(BeanUtil.getValue(tableInfo(), dto, fieldIncremental));
                        Number originValue;
                        if (fieldsForIncremental.containsKey(field.getName()) == false
                                && fieldName.length() > field.getName().length() && field.getName().length() > 0 )
                        {
                            originValue = DataHelper.parseDoubleByNull2Zero(BeanUtil.getValue(tableInfo(), dto.origin(), field));
                            Number result = ChangeData.incrtl(fieldIncremental, changeValue, originValue);
                            if (result !=null ){
                                BeanUtil.setValue(tableInfo(), redisDTO2, field, result);
                                BeanUtil.setValue(tableInfo(), redisDTO2, fieldIncremental.getName(), null);
                                dto.mapSave().put(field.getName(), result); //将递增/减 后的值返回到 调用方(如前端)
                            }
                        }else{
                            originValue = DataHelper.parseDoubleByNull2Zero(BeanUtil.getValue(tableInfo(), dto.origin(), fieldIncremental));
                            Number result = ChangeData.incrtl(fieldIncremental, changeValue, originValue);
                            if (result !=null ){
                                BeanUtil.setValue(tableInfo(), redisDTO2, fieldIncremental, result);
                                dto.mapSave().put(fieldIncremental.getName(), result); //将递增/减 后的值返回到 调用方(如前端)
                            }
                        }
                    }


                    //DTO 和 mapSave 的版本字段 +  1, 便于 缓存reids 和输出到 前台
                    String versionFieldName = tableInfo().getVersionFieldName();
                    if (Misc.isNotNull(versionFieldName) && redisDTO2.dataVersion() != null ){
                        redisDTO2.dataVersion(redisDTO2.dataVersion() + 1);//DTO +1
                        dto.mapSave().put(versionFieldName, redisDTO2.dataVersion());  //将递增后的值返回到 调用方(如前端)
                    }

                    //@RowId字段始终返回前端
                    String rowIdDFieldName = tableInfo().getRowNoFieldName();
                    if (Misc.isNotNull(rowIdDFieldName)){
                        dto.mapSave().put(rowIdDFieldName, dto.dataRowId());
                    }

                    if (isSaveReids) {
                        dataCache().saveRedis(dto,tableInfo());
                    }
                }

                /**
                 * 新增时, 不考虑 放入缓存.  因为 DTO没有 默认值,  默认值 是在 数据库 中
                 */

                if (DbState.DELETE.equals(dto.dataState())){//DbState.INSERT.equals(dto.dataState()) ||
                    dataCache().saveRedis(dto,tableInfo());
                }
            }
        }else{
            //解决没有redis 返回值 没有 rowId 版本号的问题
            for (DTO dto : list){
                if (!DbState.UPDATE.equals(dto.dataState())){ return;}

                //DTO 和 mapSave 的版本字段 +  1, 便于 缓存reids 和输出到 前台
                String versionFieldName = tableInfo().getVersionFieldName();
                if (Misc.isNotNull(versionFieldName)){
                    dto.mapSave().put(versionFieldName, dto.dataVersion());  //将递增后的值返回到 调用方(如前端)
                }

                //@RowId字段始终返回前端
                String rowIdDFieldName = tableInfo().getRowNoFieldName();
                if (Misc.isNotNull(rowIdDFieldName)){
                    dto.mapSave().put(rowIdDFieldName, dto.dataRowId());
                }

            }
        }
    }

    @Override
    public int insert(DTO dto){
        if (dto==null) return 0;

        List<DTO> list = new ArrayList<>(1);
        list.add(dto);
        return insertList(list);
    }

    /**
     * 获得自增主键id
     * @return
     */
    public Long getAutoKeyId() {
        //mysql  postgresql
        String dbType=sysConfig().getDatabaseType();
        if ("mysql".equals(dbType)){
            return baseMapper.selectForLong("select LAST_INSERT_ID()");
        }else if ("postgresql".equals(dbType)){
            String keySeq=tableInfo().getTableName()+"_"+tableInfo().getIdField().getDbName()+"_seq";
//            stz_out_info_stoi_id_seq
            return baseMapper.selectForLong("select nextval('"+keySeq+"'::regclass) as id ");
        }else{
            throw CommonException.newInstance(ResponseCode.SYSTEM_ERROR).text("未定义的数据库类型");
        }

    }

    /**
     *
     * 实现多个dto 生成一句SQL插入
     * @param list
     *
     * @return
     */
    @Override
    public int insertList(List<DTO> list){
        if (list == null|| list.isEmpty()) {return 0;}

        beforeSave2(list, DbState.INSERT);
        int result=0; Long autoId;
        if (isAutoKeyId){
            String dbType=sysConfig().getDatabaseType();
            for(DTO dto: list){
                if ("postgresql".equals(dbType)){
                    autoId = getAutoKeyId();
                    dto.setValue(tableInfo().getIdField().getName(), autoId);
                }
                baseMapper.insert(restSessionHandler.getEntitySql(clazzDTO()).getInsertSql(dto,false));
                if ("mysql".equals(dbType)){
                    autoId = getAutoKeyId();
                    dto.setValue(tableInfo().getIdField().getName(), autoId);
                }
                result++;
            }
        }else{
            result = baseMapper.insert(
                    restSessionHandler.getEntitySql(clazzDTO())
                    .getInsertSql((List<DataDTO>) list,false)
            );
        }

        //执行插入之后的的方法
        afterSave2(list, DbState.INSERT);

        return result;
    }


    /**
     * 提供原始数据 的方法.  更新前 合并数据用
     * @param setIds
     * @return
     */
    public List<DTO> originData(Set<String> setIds){
        if (setIds.size()==0){
            return new ArrayList<>();
        }
        List<DTO> listQuery=listObjects(setIds);
        return listQuery;
    }
    public void initOriginData(DTO dto){
        List<DTO> list =new ArrayList<>();
        list.add(dto);
        initOriginData(list);
    }
    public void initOriginData(List<DTO> listUpdate){
        Field idField = tableInfo().getIdField();
        //获得更新之前的数据
        Set<String> setIds = new HashSet<>();//listUpdate.stream().map(DataDTO::dataId).collect(Collectors.toSet());
        if (sysConfig().getUpdateMode() > 1 || (sysConfig().getUpdateMode()==1 && sysConfig().isSelectByBeforeUpdate())) {
            for (DTO dto : listUpdate){
                String id = dto.dataIdAndCheckNull();
                setIds.add(id);
            }
        }

        List<DTO> listQuery = originData(setIds);

        //提交的修改过的数据, list结构转成 map结构,  便于 获取
        Map<String, JSONObject> mapModifyData = new HashMap<>();
        JSONArray bodyData = restSessionHandler.get().getBodyData();
        if (bodyData != null) {
            for (Object map : bodyData){
                JSONObject mapTemp = (JSONObject) map;
                mapModifyData.put(mapTemp.getString(idField.getName()), mapTemp);
            }
        }
        Map<String, DTO> mapOriginDTO = listQuery.stream().collect(Collectors.toMap(DTO::dataId, Function.identity()));//转成map

        for (DTO dto : listUpdate){
            if (dto.origin()==null) {
                //value是DTO. 添加 原始DTO-修改之前的数据, 来自于数据库查询或者Redis
                DTO origin = mapOriginDTO.get(dto.dataId());
                if (origin !=null && origin.hashCode() == dto.hashCode()){
                    dto.addOrigin(BeanUtil.cloneBean(origin));//重要
                }else {
                    dto.addOrigin(origin);
                }
            }
            dto.addModifyMap(mapModifyData.get(dto.dataId()));//value是JSONObject. 添加 提交的修改过的数据. 当依据变化值生成更新SQL时 需要用到

            ///////////////////////////////////////////////////////////////////////////////////
            //处理当前DTO= 原始数据 + 提交修改的数据
            if (sysConfig().getUpdateMode()>1) {
                if (dto.mapModify()==null){
                    //后台 自己查询出来的数据做保存, 这里无逻辑
                    //后台自己new DTO 做的报错, 这里无逻辑
                }else {
                    //有点复杂
                    DTO databaseDTO=mapOriginDTO.get(dto.dataId());//数据库查出来的数据
                    Map mapModify=dto.mapModify();                 // 1. 前台提交的数据
                    Map mapCurrentDTO=BeanUtil.bean2Map(dto);      // 2. 用户可能修改过dto的数据

                    BeanUtil.copyBean(databaseDTO, dto,false);
                    mapModify.putAll(mapCurrentDTO); //会造成所有字段更新的.  所以注释  //2024-07-30
                    BeanUtil.map2Bean(mapModify, dto);

                    dataCache().addCacheBySelected(dto);//20181217添加到一级缓存

//                    BeanUtil.copyBean(mapOriginDTO.get(dto.dataId()), dto,false);   //拷贝原始数据      到当前DTO
//                    BeanUtil.map2Bean(mapModifyData.get(dto.dataId()), dto);   //拷贝提交修改的数据 到当前DTO
//                    dataCache().addCacheBySelected(dto);//20181217添加到一级缓存
                }
            }
            ////////////////////////////////////////////////////////////////////////////////////
        }
    }

    /**
     * @param sql
     * @return
     */
    public int execute(String sql){
        return baseMapper.update(sql);
    }

    /**
     * 根据Id更新, version不是null, 则增加乐观锁
     * @param dto
     * @return
     */
    @Override
    public int update(DTO dto){
        if (dto==null) return 0;
        List<DTO> list = new ArrayList<>(1);
        list.add(dto);
        return updateList(list, sysConfig().getUpdateMode());
    }
    @Override
    public int update(DTO dto, int updateMode){
        if (dto==null) return 0;
        List<DTO> list = new ArrayList<>(1);
        list.add(dto);
        return updateList(list, updateMode);
    }
    @Override
    public int updateList(List<DTO> list) {
        return updateList(list, sysConfig().getUpdateMode());
    }

    /**
     * update的记录 不执行 beforeInsertOrUpdate afterInsertOrUpdate
     * @param dto
     * @return
     */
    @Override
    public int update2(DTO dto){
        if (dto==null) return 0;
        dto.isBeforeOrAfterUpdate=false;
        int result = update(dto);
        dto.isBeforeOrAfterUpdate=true;
        return result;
    }

    /**
     * update的记录 不执行 beforeInsertOrUpdate afterInsertOrUpdate
     * @param dto
     * @param updateMode
     * @return
     */
    @Override
    public int update2(DTO dto, int updateMode){
        if (dto==null) return 0;
        List<DTO> list = new ArrayList<>(1);
        dto.isBeforeOrAfterUpdate=false;
        list.add(dto);
        int result =  updateList(list, updateMode);
        dto.isBeforeOrAfterUpdate=true;

        return result;
    }
    /**
     * update的记录 不执行 beforeInsertOrUpdate afterInsertOrUpdate
     * @param list
     * @return
     */
    @Override
    public int updateList2(List<DTO> list) {
        for (DTO dto : list){
            dto.isBeforeOrAfterUpdate=false;
        }
        int result =  updateList(list, sysConfig().getUpdateMode());
        for (DTO dto : list){
            dto.isBeforeOrAfterUpdate=true;
        }
        return result;
    }

    /**
     * 当行更新 返回 0 调用本方法抛出异常
     * @param dto
     */
    public void updateNotResultError(DTO dto){
        //获得中文表名
        String tableCnName = tableInfo().getTableCnName();
        String idvalue = dto.dataId();
        if (sysConfig().isUpdateNotResultError()) {
            if (dto.dataVersion() != null) {
                throw TopException.newInstance(ResponseCode.DB_UPDATE_VERSION_ISNOT_NEW)
                        .text(Misc.isNull(tableCnName)?clazzDTO().getName():tableCnName, " id=", idvalue, " 更新失败, 该数据可能已经被他人修改, 请刷新");
            }
            throw TopException.newInstance(ResponseCode.DB_UPDATE_FIND_NO_DATA)
                    .text(Misc.isNull(tableCnName)?clazzDTO().getName():tableCnName, " id=", idvalue, " 更新失败");
        }else{
            log.warn("{} id={} 更新影响的行数为0", Misc.isNull(tableCnName)?clazzDTO().getName():tableCnName, idvalue);
        }
    }

    /**
     * @param listUpdate
     * @param updateMode
     * @return
     */
    @Override
    public int updateList(List<DTO> listUpdate, int updateMode){
        //更新成功后的DTO容器
        List listResult= new ArrayList<>();
        if (listUpdate == null || listUpdate.isEmpty()) {return 0;}
//        //beforeInit();

        beforeSave2(listUpdate, DbState.UPDATE);

        //线程安全的自增类
        AtomicInteger result =  new AtomicInteger();

        //得到多主键字段
        for (DTO dto : listUpdate){
            String idsValue = dto.dataId(); //多主键时 用 - 把值串起来
            DTO origin = dto.origin();
            if (origin==null && dto.dataVersion() != null) {
                origin = getObject(idsValue);
            }
            if (dto.dataVersion() != null && origin!=null && origin.dataVersion()!=null
                    && dto.dataVersion()<origin.dataVersion()){
                throw TopException.newInstance(ResponseCode.DB_UPDATE_VERSION_ISNOT_NEW)
                        .text(clazzDTO().getName(), ".", idsValue,
                                " 可能已经被他人修改, 请刷新, 传入的版本 ",
                                dto.dataVersion(),
                                " 数据库的版本 ",
                                origin.dataVersion());
            }

            //dto.mapModify() ==null, 说明是后台 new DTO()或者getObject()出来的对象, 这时只更新不为null的字段
            int updateModeTemp = (dto.mapModify()==null && dto.isChangeUpdateSql() !=null && dto.isChangeUpdateSql() != true )? 1 :updateMode;
            updateModeTemp = (dto.isChangeUpdateSql() !=null && dto.isChangeUpdateSql() == true)?3:updateModeTemp;

            // 执行更新SQl
            String updateSQL = restSessionHandler.getEntitySql(clazzDTO()).getUpdateByIdSql(dto, updateModeTemp);
            int one = baseMapper.update(updateSQL);
            if (one == 1 ) {
                listResult.add(dto);
            }else{
                updateNotResultError(dto);
            }
            //DTO.mapSave中处理dto.dataVersion(dto.dataVersion() == null?null:(dto.dataVersion()+1));
            result.set(result.get() + one);
        }

        //before 可能会修改list里面的DTO, 这里重新合并一次
        afterSave2(listResult, DbState.UPDATE);//执行更新之后的的方法

        return result.get();
    }

    @Override
    public int updateBatch(DTO dataDTO, DataQTO qto){
        //执行批量更新SQL, 一句SQL
        int result = baseMapper.updateBatch(
                restSessionHandler.getEntitySql(clazzDTO())
                        .updateBatch(dataDTO)
                        .setWhere(where().setQTO(qto))
                        .getSql()
        );

//        //UpdateMode()==1时 当查询条件的主键字段值完整时且无其他条件,则要删除缓存 lc需求
//        if (result==1 && sysConfig().getUpdateMode()==1 && qto.checkOnlyKeysValue()){
//            //删除缓存
//            dataCache().deleteRedis(tableInfo(), qto.dataIdAndCheckNull());
//        }

        return result;
    }

    @Override
    public List<DTO> updateBatch(DTO dto, Condition where){
        //Id字段设置为null, 不支持修改的
        dto.setValue(tableInfo().getIdField().getName(), null);

//        //条件匹配器中是否只有主键字段的条件
//        boolean isOnlyIdValues = true;
//        Map<String,Field> mapKeyFields = tableInfo().getMapIdFields();
//        for(Map.Entry<String, Field> entry : mapKeyFields.entrySet()){
//            String key = entry.getKey();
//            //Field field = entry.getValue();
//            dto.setValue(key, null);
//            if (isOnlyIdValues == true && !(where.mapFields.containsKey(key)
//                    || where.mapFields.containsKey(TableInfo.getColumn(sysConfig(), key)) ))
//            {
//                isOnlyIdValues = false;
//            }
//        }
//        //sysConfig().isSelectByBeforeUpdate() 更新之前是否先查询

        List<DTO> listQuery =null;
        if (sysConfig().isSelectByBeforeUpdate()) {
            //查询出原始数据, 带缓存
            listQuery = listObjects(where);
            if (listQuery == null || listQuery.isEmpty()) {
                throw TopException.newInstance(ResponseCode.DB_UPDATE_FIND_NO_DATA).text("查无需要更新的数据");
            }
        }

        //更新之前
        //这个方法不能调用, 否则冲突, 如果用户在 before 事件更改了DTO的值, 那更新如何处理,总不能一条条更新,这样就失去了批量更新的意义了
        //beforeSave2(listUpdate, DbState.UPDATE);

        //执行批量更新SQL, 一句SQL
        int result = baseMapper.updateBatch(
                restSessionHandler.getEntitySql(clazzDTO())
                        .updateBatch(dto)
                        .setWhere(where)
                        .getSql()
        );

        /** 要更新的数据 注意: listUpdate 里面始终只有一条记录 */
        List<DTO> listUpdate = new ArrayList<>(1);
        /** 合并数据 */
        //Map<String, DTO> mapMergeDTO = new HashMap<>(); //合并数据

        if (sysConfig().isSelectByBeforeUpdate()) {
            for (DTO queryDTO : listQuery ){
                DTO newDTO = newDTO();
                newDTO.addOrigin(queryDTO);              //set原始DTO
                BeanUtil.copyBean(queryDTO, newDTO);     //把查询的数据   拷贝到newDTO
                BeanUtil.map2Bean(dto.mapSave(), newDTO);//把用户修改的数据拷贝到newDTO
                newDTO.mapSave(dto.mapSave());
                newDTO.dataState(DbState.UPDATE);
                listUpdate.add(newDTO);
            }

            afterSave2(listUpdate, DbState.UPDATE);/** 执行更新之后的方法, bean是合并后数据 */
        }else{
            for (int i=0;i<result;i++){
                listUpdate.add(null);
            }

//            //UpdateMode()==1时 当查询条件的主键字段值完整时且无其他条件,则要删除缓存 LC需求
//            if (result==1 && sysConfig().getUpdateMode()==1 && where.mapFields.size() == mapKeyFields.size()){
//                //把主键值复制到 dto, 便于获得 主键字段的值,删除缓存
//                for(Map.Entry<String, Field> entry : mapKeyFields.entrySet()){
//                    String key = entry.getKey(); Object value =where.mapFields.get(key);
//                    value =Misc.isNotNull(value)?value:where.mapFields.get(tableInfo().getColumn(key));
//                    dto.setValue(key,value);
//                }
//                //删除缓存
//                dataCache().deleteRedis(tableInfo(),dto.dataIdAndCheckNull());
//            }
        }

        return listUpdate;
    }

    /**
     * deleteByIds() deleteBatch() deleteList()不经过此方法
     */
    @Override
    public int delete(DTO dto){
        if (dto==null) return 0;
        List<DTO> list = new ArrayList<>(1);
        list.add(dto);
        return deleteList(list);
    }

    /**
     * 根据多个Id, 生成一句SQL删除, 无乐观锁
     * 先查询出来, 目的是得到每条记录的id, 然后根据Id删除redis缓存
     * @param ids
     * @return
     */
    @Override
    public int deleteByIds(String... ids){
        Set setIds = Misc.idsArray2Set(ids);
        List<DTO> list  = listObjects(setIds);//删除之前先查询出来, 带缓存
        if (ids != null && setIds.size() != list.size()){
            throw TopException.newInstance2(ResponseCode.DB_DELETE_FIND_NO_DATA);
        }
        return deleteList(list);
    }

    @Override
    public int deleteByIds(Number... ids){
        Set setIds = Misc.idsArray2Set(ids);
        List<DTO> list  = listObjects(setIds);//删除之前先查询出来, 带缓存
        if (ids != null && setIds.size() != list.size()){
            throw TopException.newInstance2(ResponseCode.DB_DELETE_FIND_NO_DATA);
        }
        return deleteList(list);
    }


    /**
     * 自定义删除条件, 即不是根据主键的删除才用这个
     * 先查询出来, 目的是得到每条记录的id, 然后根据Id删除redis缓存
     * @param where
     * @return
     */
    @Override
    public int deleteBatch(Condition where){
        List<DTO> list = listObjects(where); //删除之前先查询出来, 不带缓存
        return deleteList(list);//执行删除逻辑, 执行一句删除SQL, Id 拼 Or
    }

    @Override
    public int deleteList(List<DTO> list){
        if (list == null || list.isEmpty()) {return 0;}

        beforeSave2(list, DbState.DELETE);//执行删除之前的逻辑

        Set idValues = list.stream().map(DataDTO::dataId).collect(Collectors.toSet());
        EntityDelete entityDelete = restSessionHandler.getEntitySql(clazzDTO()).getEntityDelete().deleteBatch();
        entityDelete.where().eq(tableInfo().getIdField().getName(), idValues);
        int result = baseMapper.delete(entityDelete.getSql());
        afterSave2(list, DbState.DELETE);//执行删除之后的逻辑

        return result;
    }

    public boolean checkIdType(Object id){
        Misc.checkObjNotNull(id,"id");
        if (id instanceof String || id instanceof Integer || id instanceof Long){
            return true;
        }
        throw TopException.newInstance(ResponseCode.SYS_KEY_FIELD_DATATYPE_ISINVALID)
                .text("id 字段的类型(",id.getClass().getName(),")不对, 目前仅仅支持 String Integer Long");
    }

    /**
     * 支持一次提交存在增删改的多行, 依据DTO中dataState()状态来识别增删改操作.
     * @see DbState
     * @param dto
     */
    @Override
    public DTO save(DTO dto) {
        if (dto==null) return null;
        List<DTO> listSave = new ArrayList<>();
        listSave.add(dto);
        save(listSave);
        return dto;
    }

    /**
     * update的记录 不执行 beforeInsertOrUpdate afterInsertOrUpdate
     * @param dto
     * @return
     */
    @Override
    public DTO save2(DTO dto) {
        if (dto==null) return null;
        List<DTO> listSave = new ArrayList<>();
        listSave.add(dto);
        save2(listSave);
        return dto;
    }
    @Override
    public List<DTO> save(List<DTO> list) {
       return save(list, true);
    }

    /**
     * update的记录 不执行 beforeInsertOrUpdate afterInsertOrUpdate
     * @param list
     * @return
     */
    @Override
    public List<DTO> save2(List<DTO> list) {
        return save(list, false);
    }

    /**
     * @param list 重复检查用, 包含 新增和更新的数据
     *             而 beforeInsertOrUpdate是插入调用一次, 更新调用一次
     *             多行重复检查用这个
     */
    public void beforeCheck(List<DTO> list){

    }

    /**
     *
     * @param list
     * @param isBeforeOrAfterUpdate update时 是否执行 beforeInsertOrUpdate afterInsertOrUpdate
     * @return
     */
    private List<DTO> save(List<DTO> list, Boolean isBeforeOrAfterUpdate) {
        List<DTO> listResult = new ArrayList<>();
        if (list == null) {return listResult;}
        //所有新增的记录
        List<DTO> listInsert = list.stream().filter(dto -> DbState.INSERT.equals(dto.dataState())).collect(toList());
        //所有更新的记录
        List<DTO> listUpdate = list.stream().filter(dto -> DbState.UPDATE.equals(dto.dataState())).collect(toList());
        //所有删除的记录
        List<DTO> listDelete = list.stream().filter(dto -> DbState.DELETE.equals(dto.dataState())).collect(toList());

        if (isBeforeOrAfterUpdate==false){
            for (DTO dto : listUpdate){
                dto.isBeforeOrAfterUpdate=false;
            }
        }

        //新增和更新的数据合并到一个数据集 执行 beforeCheck
        List<DTO> listRU = new ArrayList<>();
        listRU.addAll(listInsert);
        listRU.addAll(listUpdate);
        beforeCheck(listRU);

        insertList(listInsert);
        updateList(listUpdate);
        deleteList(listDelete);

        listResult.addAll(listInsert);
        listResult.addAll(listUpdate);
        listResult.addAll(listDelete);

        return listResult;
    }

    @Override
    public Response<List<DTO>> query(DataQTO qto, String sqlId, String index){
        index = index == null?"":index;
//        Integer pageSize = qto.getPageSize();
//        Integer pageIndex = qto.getPageIndex();
        String basePath = clazzMapper().getName();//getClass().getName().replace(".service.", ".dao.").replace("Service", "")+"Dao";
        String methodQuery = basePath + "."+sqlId+index;

        String methodQueryCount = basePath + "."+sqlId+"Count"+index;

//        Integer maxPageSize = DataHelper.parseInt(environment.getProperty("top.maxPageSize"));
//        if (pageSize != null && pageSize  <=  0) qto.setPageSize(maxPageSize <= 0?20000:maxPageSize); //pageSize <= 0, 表示不分页查询时, 最多只查询20000条

        //增强查询支持
        restSessionHandler.getEntitySql(clazzDTO()).getEntitySelect().initQTO(qto, sysConfig());
        //initQTO(qto);
        int count;
        Map<String,Object> mapQTO = BeanUtil.bean2Map(TableInfo.get(qto.getClass()), qto, false, true);
        List<DTO> list = sqlSessionTemplate.selectList(methodQuery, mapQTO);
        if (       qto.getOpenPage()==false  //分页标记关闭
                || mapQTO.containsKey(tableInfo().getIdField().getName())  //存在主键字段作为条件, 自动禁用执行 count sql
        ){
            //此时就不执行分页查询了
            count=list.size();
        }else{
            count = sqlSessionTemplate.selectOne(methodQueryCount, mapQTO);
        }

        return new Response(list, count).pageSize(qto.getPageSize());
    }


    /**
     * 对应 xxxMapper.xml 的list查询
     */
    @Override
    public Response<List<DTO>> list(DataQTO qto){
        //优先执行 mapper.xml 对应的SQL sqlId = listObjects, 没有则类库自动生成当前表所有字段的查询SQL
        Response<List<DTO>> response;
        try {
            DataQTO qto2=(DataQTO)BeanUtil.cloneBean(qto);
            response = query(qto2, "list", "");
        }catch(BindingException e){
            //dao文件没有对应的方法, 则 报错
            response = listPage(select().where().setQTO(qto).endWhere());
        }catch(MyBatisSystemException e){
            log.debug(" ***Mapper.xml文件中没有配置select id =list的SQL, 将自动生成SQL查询.");
            if(e.getMessage().indexOf("Mapped Statements collection does not contain value for")>0){
                //mapper.xml没有对应的sqlId, 则 报错
                response = listPage(select().where().setQTO(qto).endWhere());
            }else {
                throw e;
            }
        }
        return response;
    }
    /**
     * 本方法会考虑缓存,  优先获取一级缓存中的数据
     * 单表分页自定义SQL查询, 将执行2条SQL语句
     * 与select搭配使用   select() 返回 EntitySelect 对象
     */
    @Override
    public Response<List<DTO>> listPage(EntitySelect entitySelect){
        //执行主sql, 获取指定分页的数据
        List<DTO> list = listObjects(entitySelect);
        //查询结果放入缓存
        for (DTO dto : list){
            dataCache().addCacheBySelected(dto);
        }

        //执行符合条件的总记录数 SQL
        Integer count = 0;
        if (entitySelect.openPage()){
            count = selectCount(entitySelect.where());
        }
        Response<List<DTO>> response= new Response(list, count);
        response.pageSize(entitySelect.getPageSize());
        return response;
    }

    /**
     * 对应 xxxMapper.xml 的list查询, 但不执行分页查询
     * @param qto
     * @return
     */
    @Override
    public DTO selectOne(DataQTO qto){
        List<DTO> list = selectList(qto);
        return list.get(0);
    }
    @Override
    public List<DTO> selectList(DataQTO qto){
        DataQTO qto2=(DataQTO)BeanUtil.cloneBean(qto);
        Response<List<DTO>> response = query(qto2, "list", "");
        return response.data();
    }

    @Override
    public DTO getObject(Number id) {
        return getObject(String.valueOf(id), true);
    }
    public DTO getObject(Number id, boolean isReadCache) {
        return getObject(String.valueOf(id), isReadCache);
    }
    @Override
    public DTO getObject(String id) {
        return getObject(id, true);
    }



    /**
     * @param id
     * @param isReadCache 是否要从一二级缓存中获取
     * @return
     */
    @Override
    public DTO getObject(String id, boolean isReadCache) {
        checkIdType(id);//目前仅仅支持 String Integer Long

        DTO dto;
        if(isReadCache ) {//&& dataCache().readAndOpenCache()
            //该方法先去一级缓存(从当前线程)，再取二级缓存(Redis)
            dto = dataCache().get2CacheById(clazzDTO(), id, sysConfig(), "getObject");
            if (dto  !=  null) {
                return dto;
            }
        }
        List<DTO> list = listObjects(select().where()
                .eq(tableInfo().getIdField().getName(), id)
                .openPage(false)
        );

        dto = list.isEmpty() ? null : list.get(0);

        return dto;
    }
    @Override
    public DTO getObject(DataQTO qto) {
        qto.setPageSize(1);
        String id=qto.dataId();
        Map mapQTO = qto.mapData();
        //主键值完整且无其他条件时 应该先查询 Redis缓存
        if (Misc.isNotNull(id)){
            mapQTO.clear(); mapQTO=null;
            //执行有缓存的查询
            return getObject(id);
        }
        mapQTO.clear();mapQTO=null;
        return getObject(where().setQTO(qto));
    }
    @Override
    public DTO getObject(Condition where) {
        List<DTO> list = listObjects(getEntitySelect(where));

        if (list.isEmpty()) {return null;}
        if (list.size()>1){
            log.warn("{}getObject()获得了多条记录,默认返回第一条记录", sysConfigRead.getLogPrefix());
        }
        return list.get(0);
    }

    /**
     * 本方法会考虑缓存,  优先获取一级缓存中的数据, 但不会读取二级缓存
     * 获取到的数据会放入 一级二级缓存
     */
    @Override
    public List<DTO> listObjects(EntitySelect entitySelect){
        //跨 service容易出问题,这里纠正
        entitySelect.setTableInfo(tableInfo());

        //直接从数据库中查询出结果  entitySelect.getSql() 获得构建的SQL
        List<DTO> listQuery = baseMapper.listObjects(entitySelect.getSql());
        Condition condition = entitySelect.where();

        boolean isReadCache=true;
        if (condition.readCache()==false || (condition.getQTO()!=null && condition.getQTO().readCache()==false)){
            //return listQuery;//不读取缓存, 直接返回
            isReadCache=false;
        }

        List<DTO> listDTO =  new ArrayList<>();//定义返回的List对象
        for (DTO dto:listQuery){
            DTO cacheDTO=null;
            if (isReadCache && Misc.isNotNull(dto.dataId())){
                //根据Id从一级缓存中获取
                cacheDTO = dataCache().getCacheBySelect(clazzDTO(), dto.dataId(), sysConfig());
            }

            if (cacheDTO != null) {//一级缓存找到对象, 则以一级缓存的为准,作为返回
                listDTO.add(cacheDTO);
            }else {
                //fields为空, 默认返回所有字段, 所以可以更新缓存
                if(Misc.isNull(entitySelect.getSelect()) || entitySelect.isAppendAllFields()) {
                    //添加到 一级缓存, 二级缓存(考虑版本号)
                    dataCache().addCacheBySelected(dto);
                }
                listDTO.add(dto);
            }
        }
        return listDTO;
    }

    @Override
    public List<DTO> listObjects(String... ids) {
        return listObjects(Misc.idsArray2Set(ids), true);
    }
    @Override
    public List<DTO> listObjects(Number... ids) {
        return listObjects(Misc.idsArray2Set(ids), true);
    }

    @Override
    public List<DTO> listObjects(Collection idValues) {
        return listObjects(idValues, true);
    }
    /**
     * 优先从一二级缓存中获取
     * @param idValues 要查找的多个id值,放到set容器中
     * @param isReadCache 是否要从缓存中获取
     * @return
     */
    @Override
    public List<DTO> listObjects(Collection idValues, boolean isReadCache) {
        if (idValues==null) idValues=new HashSet();
        boolean isAllCache = true;//是否全部可以从缓存中得到

        //查询的多个Ids都从缓存中获取一次, 如果全部获取到, 就不用查数据库了
        List<DTO> listBeans = new ArrayList();
        if (isReadCache) {//从缓存中获取
            for (Object id : idValues) {
                if (Misc.isNull(id)) {continue;}
                String idValue=null;
                if (id instanceof String){
                    idValue=id.toString();
                }else if (id instanceof Number){
                    idValue=String.valueOf(id);
                }else{
                    checkIdType(id);//检查并报错
                }
                DTO dto = dataCache().get2CacheById(clazzDTO(), idValue, sysConfig(), "listObjects");
                if (dto  !=  null) {
                    listBeans.add(dto);
                } else {
                    isAllCache = false;
                    break;
                }
            }
        }
        if(isReadCache == true && isAllCache == true){//全部命中缓存
            if(idValues.size()>1) {
                log.debug("{}listObjects({})全部命中缓存", sysConfigRead.getLogPrefix(), idValues.toString()); //多条 才 打印 全部命中缓存
            }
            return listBeans;
        }

        EntitySelect entitySelect = select();
        Condition where = entitySelect
                .where()
                .eq(tableInfo().getIdField().getName(), idValues)
                .openPage(false)  //设置为不分页
                .readCache(false) //设置为 不读取缓存, 因为读过一次缓存了
        ;
//        //主键值得条件生成: 考虑到多个主键字段, 值用"减号-"隔开的情况, 要按照多主键字段的顺序匹配
//        BuildMultiFieldsWhereSql.buildSQL(tableInfo().getMapIdFields(), where, idValues, " OR ", " = ");

        //调用 List<DTO> listObjects(EntitySelect entitySelect)
        //缓存找不到记录, 则执行查询
        return listObjects(entitySelect);

//                listObjects(select()
//                        .where()
//                        .eq(tableInfo().getIdFieldsBySql(), setIds)
//                        .openPage(false)  //设置为不分页
//                        .readCache(false) //设置为 不读取缓存, 因为找过一次缓存了
//                        .endWhere()


    }

    @Override
    public List<DTO> listObjects(DataQTO qto) {
        return listObjects(select().where().setQTO(qto).endWhere());//
    }

    /**
     * 自定义条件的查询. 不会从二级缓存Redis中获取
     * @param where
     * @return
     */
    @Override
    public List<DTO> listObjects(Condition where) {
        return listObjects(getEntitySelect(where));
    }



    @Override
    public EntitySelect select(){
        return select(null, true);
    }
    @Override
    public EntitySelect select(String fields){
        return select(fields,false);
    }
    /**
     * 获得或者创建 EntitySelect对象
     * @param fields 指定查询返回的字段,多个用逗号串联, 为空时则返回所有DTO的字段. 支持数据库函数对字段处理,如: substring(name,2)
     * @param isAppendAllFields 指定字段后, 是否要添加默认的所有字段
     * @return
     */
    @Override
    public EntitySelect select(String fields,Boolean isAppendAllFields){
        return EntitySelect.create(tableInfo()).select(fields,isAppendAllFields);
    }

    @Override
    public List<JSONObject> selectMaps(DataQTO qto) {
        return baseMapper.selectMaps(select().where().setQTO(qto).getSql());
    }
    @Override
    public List<JSONObject> selectMaps(Condition where) {
        return baseMapper.selectMaps(getEntitySelect(where).getSql());
    }

    @Override
    public List<JSONObject> selectMaps(EntitySelect entitySelect){
        entitySelect.setTableInfo(tableInfo());// 跨 service容易出问题,这里纠正
        List<JSONObject> list = baseMapper.selectMaps(entitySelect.getSql());
        if (list.size()==1 && list.get(0)==null){
            list.clear();//避免 遍历 报错  这样的sql查无记录时 有一行null记录：SELECT group_concat(distinct mtfBatch) mtfBatchFROM ModeTableField
        }
        return list;
    }

    public JSONObject selectMap(EntitySelect entitySelect){
        List<JSONObject> list =selectMaps(entitySelect);
        if (list.size()==0)
            return null;
        else
            return new JSONObject(list.get(0));
    }


    @Override
    public Response<List<JSONObject>> selectPageMaps(EntitySelect entitySelect){
        //执行主sql, 获取指定分页的数据
        List<JSONObject> list = selectMaps(entitySelect);
        //执行符合条件的总记录数 SQL
        Integer count= selectCount(entitySelect.where());

        Response<List<JSONObject>> response= new Response(list, count);
        response.pageSize(entitySelect.getPageSize());
        return response;
    }

    private EntitySelect getEntitySelect(Condition where){
        Object obj = null;
        try {
            obj = where.endWhere();
        }catch(TopException e){
        }
        EntitySelect entitySelect;
        if (obj != null && obj instanceof EntitySelect){
            entitySelect = (EntitySelect)obj;
            entitySelect.setTableInfo(tableInfo());
            return entitySelect;
        }
        return select().setWhere(where).endWhere();
    }

    /**
     * 自定义条件的计数查询, 不会从缓存中获取
     * @param where
     * @return
     */
    @Override
    public int selectCount(Condition where){
        EntitySelect entitySelect = select();//.setWhere(where).endWhere();
        entitySelect.setWhere(where).openPage(false);

        return baseMapper.selectCount(entitySelect.getSelectCountSql());
    }

    /**
     *
     * @param fieldName 指定的字段名
     * @param where 条件匹配器
     * @param <T> 返回类型的泛型
     * @return
     */
    @Override
    public <T> T selectMax(String fieldName, Condition where){
        //beforeInit();
        List<JSONObject> list = baseMapper.selectMaps(
                restSessionHandler.getEntitySql(clazzDTO())
                        .select("max("+fieldName+") maxdata")
                        .setWhere(where)
                        .endWhere()
                        .openPage(false)
                        .getSql()
        );
        if (!list.isEmpty()){
            return list.get(0)==null?(T)Integer.valueOf(0):(T)list.get(0).get("maxdata");
        }
        return null;
    }

    /**
     * 创建数据校验类
     * @param bean
     * @return
     */
    public final CheckData checkData(IBean bean){
        List<IBean> list  = new ArrayList(1);
        list.add(bean);
        return checkData(list);
    }
    public final CheckData checkData(List list){
        CheckData<DataDTO> checkData = new CheckData(list, tableInfo(), this);
        return checkData;
    }

    public final KeyBuild keyBuild(){
        //用没有事务的 stringRedisTemplate,  支持一个线程中多次获得的序列号唯一
        return new KeyBuild(restSessionHandler.getStringRedisTemplate(),this);
    }
    public final KeyBuild keyBuild(String prefix, String dateFormat){
        return keyBuild().setPrefix(prefix).setDateFormat(dateFormat);
    }

    FormatConfig formatConfig;//格式化对象
    public FormatConfig formatConfig(){
        if (formatConfig == null){
            formatConfig = new FormatConfig();
        }
        return formatConfig;
    }
    public ChangeManager changeManager(DTO dto){
        ChangeManager changeManager = new ChangeManager(dto, formatConfig(), restSessionHandler.get()){};
        return changeManager;
    }
    public ChangeManager changeManager(DTO dto, FormatConfig formatConfig){
        ChangeManager changeManager = new ChangeManager(dto, formatConfig, restSessionHandler.get()){};
        return changeManager;
    }
}