package cnn.topfox.service;

import cnn.topfox.common.AbstractRestSession;
import cnn.topfox.common.DataDTO;
import cnn.topfox.common.SysConfigRead;
import cnn.topfox.util.AbstractRestSessionHandler;
import cnn.topfox.util.DataCache;
import cnn.topfox.util.SysConfig;
import cnn.topfox.util.SysConfigDefault;
import cnn.topfox.data.TableInfo;
import cnn.topfox.common.FillDataHandler;
import cnn.topfox.misc.BeanUtil;
import cnn.topfox.misc.ReflectUtils;

import com.alibaba.fastjson2.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;

import java.util.List;

@Slf4j
public class SuperService<DTO extends DataDTO> implements ISuperService {
    private Class<DTO> clazzDTO;
    private TableInfo tableInfo = null; //得到表结构
    private static ThreadLocal<JSONObject> threadLocalAttributes = new ThreadLocal();

    @Autowired @Qualifier("sysConfigDefault")
    public  SysConfigRead sysConfigRead;       //单实例读取值 全局一个实例
    private SysConfig     sysConfig;           //每个service独用的实例

    @Autowired protected Environment environment;
    @Autowired protected AbstractRestSessionHandler restSessionHandler;

    @Autowired(required = false)
    protected FillDataHandler fillDataHandler; //填充对象

    @Override
    public void init(){}

    public final DTO newDTO(){
        return BeanUtil.newInstance(clazzDTO());
    }

    /**
     * 热加载需要
     * @return
     */
    public final Class<DTO> clazzDTO(){
        //保证初始化 只执行一次
        if (clazzDTO == null) {
            clazzDTO = ReflectUtils.getClassGenricType(getClass(), 1); //获得当前DTO的Class
        }
        return clazzDTO;
    }

    public final TableInfo tableInfo(){
        if (tableInfo == null){
            tableInfo = TableInfo.get(clazzDTO());
        }
        return tableInfo;
    }
    public final SysConfig sysConfig(){
        if (sysConfig == null) {
            sysConfig = new SysConfigDefault(environment);
            BeanUtils.copyProperties(sysConfigRead, sysConfig); //为每个Service
        }
        return sysConfig;
    }
    /**
     * 初始化之前, 类库要做的事情
     * @param listUpdate 更新时,  对DTO的处理用
     */
    @Override
    public void beforeInit(List<?> listUpdate){
        threadLocalAttributes.set(new JSONObject());

        init();//开发者自定义的初始化逻辑
        if (tableInfo() != null) { //不指定DTO tableInfo就是 null
            tableInfo().setSysConfig(sysConfig());
        }
        if (!sysConfig().isRedisCache()){
            log.debug("{}重要参数openReids已经被 设置为 false", sysConfigRead.getLogPrefix());
        }

        //###################################################################################
        AbstractRestSession abstractRestSession = restSessionHandler.get();//获得当前线程的 restSession
        if (abstractRestSession == null) {
            abstractRestSession = restSessionHandler.create();//创建 restSession
            restSessionHandler.initRestSession(abstractRestSession, null);
        }
        //###################################################################################
    }

    /**
     * 线程安全级别的自定属性
     * @return
     */
    public final JSONObject attributes() {
        return threadLocalAttributes.get();
    }

    /**
     * 初始化之后, 类库要做的事情
     */
    public void afterInit(){
    }


    public DataCache dataCache(){
        return null;
    }

    /**
     * 删除Redis 指定dto的缓存
     * @param idValue
     */
    public void deleteCache(Number idValue){
        dataCache().deleteRedis(tableInfo(),idValue);
    }
    public void deleteCache(String idValue){
        dataCache().deleteRedis(tableInfo(),idValue);
    }

//    /**
//     * 删除Redis 当前表的所有缓存
//     */
//    public void cleanAllCache(){
//        dataCache().deleteRedis(tableInfo());
//    }
}