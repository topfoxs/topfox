package cnn.topfox.service;


import cnn.topfox.common.*;
import cnn.topfox.data.DbState;
import cnn.topfox.mapper.BaseMapper;

import java.util.*;

public class AdvancedService<M extends BaseMapper<DTO>, DTO extends DataDTO>
        extends SimpleService<M, DTO>
        implements IAdvancedService< DTO>
{
    /**
     * 如果参数存在名为 dbState,  则请参考
     * @see DbState
     */

    /**
     * 新增|修改|删除 "之前" 执行
     * updateBatch除外的所有 insert update delete
     * @param list
     */
    @Override
    public void beforeSave(List<DTO> list) {
    }

    /**
     * 新增|修改 "之前" 执行
     * updateBatch除外的所有 insert update
     * @param list
     */
    @Override
    public void beforeInsertOrUpdate(List<DTO> list){
    }

    @Override
    public void beforeInsertOrUpdate(DTO dto, String dbState){
    }

    /**
     * 删除 "之前" 执行
     * 例: delete() deleteByIds() deleteBatch() deleteList()
     * @param list
     */
    @Override
    public void beforeDelete(List<DTO> list){
    }
    @Override
    public void beforeDelete(DTO dto){
    }

    /**
     * 新增|修改|删除 "之后" 执行
     * 例: 所有 insert update delete, 包含 updateBatch方法
     * @param list
     */
    @Override
    public void afterSave(List<DTO> list) {
    }

    /**
     * 新增|修改 "之后" 执行
     * 例: 所有 insert update, 包含 updateBatch方法
     * @param list
     */
    @Override
    public void afterInsertOrUpdate(List<DTO> list){
    }
    @Override
    public void afterInsertOrUpdate(DTO dto, String dbState){
    }

    /**
     * 删除 "之后" 执行
     * 例: delete() deleteByIds() deleteBatch() deleteList()
     */
    @Override
    public void afterDelete(List<DTO> list){
    }
    @Override
    public void afterDelete(DTO dto){
    }

    @Override
    protected void beforeSave2(List<DTO> list, String dbState){
        super.beforeSave2(list,dbState);

        //剔除 isBeforeOrAfterUpdate=false的记录
        List<DTO> listBefore=new ArrayList<>();
        for (DTO dto : list){
            if (dto.isBeforeOrAfterUpdate()){
                listBefore.add(dto);
            }
        }

        beforeSave(listBefore);

        if (DbState.INSERT.equals(dbState) || DbState.UPDATE.equals(dbState)) {
            //执行插入或更新之前的的方法:
            beforeInsertOrUpdate(listBefore);
            for (DTO dto : listBefore){
                beforeInsertOrUpdate(dto, dbState);
            }
        } else if (DbState.DELETE.equals(dbState)) {
            //执行删除之前的逻辑
            beforeDelete(list);
            for (DTO dto : list){
                beforeDelete(dto);
            }
        }
    }

    /**
     * 增 删 改 sql执行之后 调用本方法
     * @param list
     */
    @Override
    protected final void afterSave2(List<DTO> list, String dbState){
        //剔除 isBeforeOrAfterUpdate=false的记录
        List<DTO> listAfter=new ArrayList<>();
        for (DTO dto : list){
            if (dto.isBeforeOrAfterUpdate()){
                listAfter.add(dto);
            }
        }

        if(DbState.INSERT.equals(dbState) || DbState.UPDATE.equals(dbState)){
            //执行插入或更新之前的的方法:
            afterInsertOrUpdate(listAfter);
            for (DTO dto : listAfter){
                afterInsertOrUpdate(dto, dbState);
            }
        }else if(DbState.DELETE.equals(dbState)){
            afterDelete(list);
            list.forEach((dto) -> afterDelete(dto));
        }

        /**
         * 必须是合并过的Bean(数据库原始数据 + 需改的数据 的合并)
         */
        afterSave(listAfter);

        super.afterSave2(list, dbState);
    }

}
