package cnn.topfox.util;

import cnn.topfox.common.AbstractRestSession;
import cnn.topfox.common.IRestSessionHandler;
import cnn.topfox.common.SysConfigRead;
import cnn.topfox.data.DateTime;
import cnn.topfox.misc.BeanUtil;
import cnn.topfox.misc.DateUtils;
import cnn.topfox.misc.Misc;
import cnn.topfox.misc.ReflectUtils;
import cnn.topfox.sql.EntitySql;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public abstract class AbstractRestSessionHandler<Session extends AbstractRestSession>
        implements IRestSessionHandler <Session>
{
    private ThreadLocal<Session>   threadLocalRestSession = new ThreadLocal();
    private ThreadLocal<DataCache> threadLocalDataCache = new ThreadLocal();
    private ThreadLocal<Map<String,EntitySql>> threadLocalEntitySql = new ThreadLocal();

    @Autowired(required = false)
    @Qualifier("sysRedisTemplateDTO")
    private CustomRedisTemplate sysRedisTemplateDTO;

    @Autowired(required = false)
    @Qualifier("sysStringRedisTemplate") //这个实例 已经 关闭事务了的
    private CustomRedisTemplate sysStringRedisTemplate;

    @Autowired
    @Qualifier("sysConfigDefault")
    private SysConfigRead sysConfig;//单实例读取值 全局一个实例

    @Autowired
    Environment environment;

    @Override
    final public Environment environment(){
        return environment;
    }

    @Override
    final public SysConfigRead getSysConfig(){
        return sysConfig;
    }

    /**
     * 每个线程的开始,必须调用此方法;
     * 同一个线程禁止多次调用
     */
    @Override
    final public Session create(){
        //获得当前线程的Id
        long currentThreadId =  Thread.currentThread().getId();

        threadLocalEntitySql.set(new HashMap());
        DataCache dataCache =new DataCache(sysRedisTemplateDTO);
        dataCache.setCreateThreadId(currentThreadId);
        threadLocalDataCache.set(dataCache);

        Class<Session> clazz = ReflectUtils.getClassGenricType(getClass(), 0);
        Session restSession = BeanUtil.newInstance(clazz);
        restSession.setCreateThreadId(currentThreadId);
        //始终生成一个新的 SubExecuteId 子事务号
        restSession.setSubExecuteId(KeyBuild.getKeyId());
        restSession.setAppName(sysConfig.getAppName());

        restSession.setActiveTimeMillis(System.currentTimeMillis());
        restSession.setActiveDate(DateUtils.getDateByHHmmss());
        restSession.setNowTime(new Date());
        restSession.setDateTime (DateTime.now());

        dataCache.setExecuteId(restSession.getExecuteId());
        threadLocalRestSession.set(restSession);
        log.debug("{}当前线程Id:{} 线程名:{} | restSession被创建 hashCode={} | dataCache.hashCode={}",
                sysConfig.getLogPrefix(), currentThreadId, Thread.currentThread().getName(),
                restSession.hashCode(),dataCache.hashCode());

        return restSession;
    }

    @Override
    final public void updateDate(Session restSession){
        restSession.setActiveTimeMillis(System.currentTimeMillis());
        restSession.setActiveDate(DateUtils.getDateByHHmmss());
        restSession.setNowTime(new Date());
        restSession.setDateTime(DateTime.now());
        restSession.setLocalDate(LocalDate.now());
    }

    /**
     * 线程结束后释放,否则可能会造成 内存泄漏
     */
    @Override
    final public void dispose() {
        Session restSession = get();
        long currentThreadId =  Thread.currentThread().getId();
        long end = System.currentTimeMillis();
        long activeDate=restSession.getActiveTimeMillis();

        log.debug( "{}运行结束 共耗时{}毫秒. restSession被释放 hashCode={} | currentThreadId={}",
                sysConfig.getLogPrefix(), end-activeDate,
                restSession.hashCode(), currentThreadId);

        if (restSession != null) {
            threadLocalRestSession.remove();
            threadLocalRestSession.set(null);
        }

        if (getDataCache() != null) {
            threadLocalDataCache.remove();
            threadLocalDataCache.set(null);
        }

        if (threadLocalEntitySql.get() != null) {
            threadLocalEntitySql.remove();
            threadLocalEntitySql.set(null);
        }


    }

    public abstract void initRestSession(Session restSession, Method method);


    @Override
    final public Session get() {
        //return threadLocal.get();
        long currentThreadId =  Thread.currentThread().getId();
        Session session = threadLocalRestSession.get();
        if (session == null || session.getCreateThreadId() != currentThreadId){
            //session没有,或者是其他线程创建的 session 时, 则为当前线程创建一个 session
            create();
            session = threadLocalRestSession.get();
        }

        return session;
    }

    public abstract String active(Session restSession);

    public abstract void save(Session restSession);
    public abstract void delete(String sessionId);

    //@Override
    final public DataCache getDataCache() {
        long currentThreadId =  Thread.currentThread().getId();

        DataCache dataCache = threadLocalDataCache.get();
        if (dataCache == null || dataCache.getCreateThreadId() != currentThreadId){
            //dataCache没有, 或者是其他线程创建的 dataCache, 则为当前线程创建一个 dataCache
            create();
            dataCache = threadLocalDataCache.get();
        }

        return dataCache;
    }

    final public EntitySql getEntitySql(Class clazz) {
        Map<String,EntitySql> map = threadLocalEntitySql.get();
        if (map == null){
            map =new HashMap();
            threadLocalEntitySql.set(map);
        }

        EntitySql entitySql = map.get(clazz.getName());
        if (entitySql == null) {
            map.put(clazz.getName(), new EntitySql(clazz));
        }
        return map.get(clazz.getName());
    }

    final public CustomRedisTemplate getRedisTemplateDTO() {
        return sysRedisTemplateDTO;
    }
    final public CustomRedisTemplate getStringRedisTemplate() {
        return sysStringRedisTemplate;
    }
}
