package cnn.topfox.util;

import cnn.topfox.common.DataDTO;
import cnn.topfox.common.IBean;
import cnn.topfox.common.ResponseCode;
import cnn.topfox.common.TopException;
import cnn.topfox.data.DbState;
import cnn.topfox.data.Field;
import cnn.topfox.data.TableInfo;
import cnn.topfox.sql.BuildMultiFieldsWhereSql;
import cnn.topfox.sql.Condition;
import cnn.topfox.misc.BeanUtil;
import cnn.topfox.misc.Misc;
import cnn.topfox.service.SimpleService;
import lombok.extern.slf4j.Slf4j;

import java.util.*;


@Slf4j
public class CheckData<B extends DataDTO> {
    private TableInfo tableInfo;   //得到表结构
    private SimpleService service;
    private List<B> listData;
    private Condition where;
    private String errText="";

    final static String SPLIT="•";

    public CheckData(List<B> list, TableInfo tableInfo, SimpleService service){
        this.listData =list;
        this.tableInfo=tableInfo;
        this.service=service;
    }
    /**
     * 创建一个新的条件对象Condition
     * @see Condition
     * @return
     */
    public Condition where(){
        return Condition.create(tableInfo.clazzEntity);
    }

    /**
     *
     * @param where
     * @return
     */
    public CheckData setWhere(Condition where){
        if (mapFields==null) {
            throw TopException.newInstance(ResponseCode.DATA_IS_INVALID)
                    .text("请先设置检查的字段,例: checkData().setField(name,label)");
        }
        this.where=where;
        return this;
    }

    LinkedHashMap<String,String> mapFields=null;
    LinkedHashMap<String, Field> mapKeyField=null;
    public CheckData setFields(String... fields){
        //Set<String> setFields = Misc.idsArray2Set(fields);//传入 array2Set("01,02","03,01","02")是 返回 set 01,02,03
        for (String field : fields){
            addField(field, field);
        }
        return this;
    }
    public CheckData addField(String name,String label){
        if (listData ==null) {
            throw TopException.newInstance(ResponseCode.DATA_IS_INVALID)
                    .text("请先设置检查的数据,例: checkData().setData(bean/list)");
        }
        mapFields=mapFields==null?mapFields=new LinkedHashMap<>():mapFields;
        mapFields.put(name,label);

        mapKeyField=mapKeyField==null?mapKeyField=new LinkedHashMap<>():mapKeyField;
        mapKeyField.put(name,tableInfo.getField(name));

        return this;
    }


    public CheckData setErrText(String errText){
        this.errText=errText;
        return this;
    }

    /**
     * 检查每行数据 是否有 修改
     * @param dto
     * @return
     */
    private boolean checkChange(B dto){
        boolean isChange=false;
        for (String field : mapFields.keySet()){
            if (dto.checkChange(field)==true){
                isChange=true;
                break;
            }
        }

        return isChange;
    }

    /** 查询后 检查重复, 样例SQL语句:
        SELECT orgId,mobile
        FROM SecUser a
        WHERE 1=1 AND ... 可自定义
        GROUP BY orgId,mobile
        HAVING count(1)>1
        LIMIT 0,2000
     * @return
     */
    public String selectAndCheckUnique() {
        return selectAndCheckUnique(true);
    }
    public String selectAndCheckUnique(boolean isThrowNewException) {
        return excute(isThrowNewException, true);
    }

    public String excute() {
        return excute(true, false);
    }
    public String excute(boolean isThrowNewException) {
        return excute(isThrowNewException, false);
    }
    /**
     *
     * @param isThrowNewException 是否抛异常
     * @return 返回错误信息
     */
    public String excute(boolean isThrowNewException, boolean checkUnique){
        if (mapFields==null) {
            throw TopException.newInstance(ResponseCode.DATA_IS_INVALID)
                    .text("请先置检查的字段,例: checkData().setField(name,label)");
        }

        if (!listData.isEmpty())
        if (listData.get(0) instanceof DataDTO ==false){
            return "";
        }

        //String idFieldsBySql=tableInfo.getIdFieldsBySql();
        ArrayList<String> arrayIds=new ArrayList<>();    //当前值
        ArrayList<String> arrayOldIds=new ArrayList<>(); //原始值

        //检查前台传回来的值是否是否有重复 Map<值,出现次数>
        Map<String,Integer> valueCount =new LinkedHashMap();
        String uniqueCols=Misc.array2String(mapFields.keySet());
        mapFields.keySet().toString().replace("[","").replace("]","");
        for (B bean : listData){
            String state = bean.dataState();
            //循环前台传回的数据
            if (bean == null || state == null) {
                continue;
            }
            //luojp 2023 注释:为了实现所有的数据进行重复检查
            if(!DbState.UPDATE.equals(state) && !DbState.INSERT.equals(state)) {
                continue;//非更新和新增，则跳过
            }
            if (checkChange(bean)==false) {
                continue;//检查的字段 值 都 没有修改
            }

            if(DbState.UPDATE.equals(state)) {
                arrayOldIds.add(bean.dataId());//原始值
            }

            String ls_value = getValueByCols(bean, uniqueCols, SPLIT);
            Integer count=valueCount.get(ls_value);
            valueCount.put(ls_value,(count==null?0:count) +1);
            if (Misc.isNotNull(bean.dataId())) {
                arrayIds.add(bean.dataId());
            }

        }
        if (valueCount.size() == 0) {
            return null;//检查的字段没有修改时, 则 return
        }

        if (where==null) where=where();
        List<Map<String,Object>> listQuery;
        if (checkUnique) {
            //重复检查专用, 通常放在 update sql后执行
            listQuery=service.selectMaps(
                service.select(uniqueCols)
                        .setWhere(where)
                        .endWhere()
                        .groupBy(uniqueCols)
                        .having("count(1)>1")
                        .setPageSize(2000)
            );

        }else{
            where.and();
            BuildMultiFieldsWhereSql.buildSQL(mapKeyField, where, valueCount.keySet(), SPLIT, " OR ", " = ");
            if (arrayOldIds.size()>0 || arrayIds.size()>0) {
                Object[] arrValue=arrayOldIds.size()>0?arrayOldIds.toArray():arrayIds.toArray();
                where.and().ne(tableInfo.getIdField().getName(),arrValue);
            }
            listQuery=service.selectMaps(
                service.select(uniqueCols)
                        .setWhere(where)
                        .endWhere()
                        .setPageSize(2000)
            );

        }

        valueCount.clear();
        //分组放值 检查重复
        for (Map<String,Object> mapDTO : listQuery){
            if (uniqueCols==null) continue;
            String ls_value = getValueByCols(mapDTO);//map.get("result").toString();
            Integer count=valueCount.get(ls_value);
            valueCount.put(ls_value,(count==null?0:count) +1);
        }

        String err="";
        if (errText==null || errText.length()<=0) {
            for (String valueString : valueCount.keySet()) {
                if ("*".equals(valueString) || valueCount.get(valueString) < 1) {
                    continue;
                }
                err = err + "{" + valueString.replaceAll(SPLIT,",   ") + "}";
            }
        }
        if (err.length()>2) {//检查提交行数是否存在重复值
            if (!isThrowNewException){
                return err;
            }else {
                log.debug(uniqueCols + " " + err);
                if (errText != null && errText.length() > 2) {
                    log.debug(errText + " 详细信息：" + getFieldLabel(tableInfo, uniqueCols, true) + "的值" + err);
                    throw TopException.newInstance(ResponseCode.DATA_IS_DUPLICATE).text(errText);
                } else {
                    String labels=getFieldLabel(tableInfo, uniqueCols, true);
                    throw TopException.newInstance(ResponseCode.DATA_IS_DUPLICATE)
                            .text(" 数据重复:",labels, "", err);
                }
            }
        }else{
            return null;//无错误
        }
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * 根据Dataset 的field名字返回fieldLable
     *
     * @param tableInfo
     * @param fieldName field名字
     * @return  field标题 fieldLable
     * @throws Exception
     */
    private String getFieldLabel(TableInfo tableInfo, String fieldName){
        fieldName=fieldName.trim();
        if (tableInfo.clazzEntity==null){
            log.info("tableInfo.clazzEntity==null, 无法获取字段"+fieldName+"的中文名");
            return fieldName;
        }
        Field field =tableInfo.getField(fieldName);
        if (field==null)return fieldName;

        String label=mapFields.get(fieldName);
        if (fieldName.equals(label)){
            label=field.getLabel();
        }
        return Misc.isNull(label)?fieldName:label;
    }
    /**
     * 根据Dataset 的field名字返回fieldLable
     *
     * @param tableInfo
     * @param cols field名字
     * @param isMark 是否加上标签
     * @return  field标题 fieldLable 如isMark＝true则返回 「金额」...isMark＝false则返回金额
     */
    private String getFieldLabel(TableInfo tableInfo, String cols, boolean isMark){
        String colsArray[] = cols.split(",");
        String returnLabels="";
        for (int i=0; i<colsArray.length;i++){
            String label = getFieldLabel(tableInfo, colsArray[i]);
//            if (Misc.isNull(label) || label.equals("xx")) continue;
            if (isMark) {
                returnLabels = returnLabels + "「" + label + "」";
            }else {
                returnLabels = returnLabels + " " +  label;
            }
        }
        return returnLabels;
    }

    private String getValueByCols(IBean bean, String cols, String splitString){
        String colsArray[] = cols.split(",");
        String retValue = "";
        String value;
        Map<String,Field> fields=tableInfo.getFields();
        for (int i=0; i<colsArray.length;i++){
            Field field=fields.get(colsArray[i].trim());
            value = BeanUtil.getValue2String(tableInfo, bean, field);
//luojp 2019 01 10
//            if (field!=null && value.length()>10 && (field.getDataType()== DataType.DATE)){
//                value=value.substring(0,10);
//            }
            if (Misc.isNull(value)){value="null";}//不能改的  多主键id值时有用
            if (i<colsArray.length - 1) value=value+splitString;
            retValue = retValue + value;
        }

        return retValue;
    }
    private String getValueByCols(Map row){
        StringBuilder sb = new StringBuilder();
        for (String fieldName : this.mapFields.keySet()) {
//            String dbName=TableInfo.getColumn(tableInfo.getSysConfig(), fieldName);
            String dbName=tableInfo.getColumn(fieldName);
            String label = this.mapFields.get(fieldName);
//            if (Misc.isNull(label) || label.equals("xx")) continue;
            Object value = row.get(dbName);
            if (value==null || "".equals(value.toString())) value="null";
            sb.append(value);
            sb.append(this.SPLIT);
        }


        sb.setLength(sb.length()-1);
        return sb.toString();
    }

    public String checkNotNull() {
        return checkNotNull(true);
    }
    public String checkNotNull(boolean isThrowNewException) {
        if (mapFields==null) {
            throw TopException.newInstance(ResponseCode.DATA_IS_INVALID).text("请先设置检查的字段,例: checkData().setField(name,label)");
        }

        Set<String> setFields=new HashSet();
        StringBuilder stringBuilder=new StringBuilder();
        for(int i = 0; i< listData.size(); i++){
            IBean bean= listData.get(i);
            mapFields.keySet().forEach((fieldName) -> {
                if (Misc.isNull(BeanUtil.getValue(bean, fieldName))) {
                    setFields.add(fieldName);
                    //stringBuilder.append("{").append(mapFields.get(fieldName)).append("},");
                }
            });
        }

        setFields.forEach((fieldName)->{
            stringBuilder.append("{").append(mapFields.get(fieldName)).append("},");
        });
        if (setFields.size()>0){
            //String notFields=setFields.toString().replace("[","").replace("}","");
            //notFields=notFields.replace(",","},{");
            stringBuilder.setLength(stringBuilder.length()-1);//去掉最后的逗号和空格
            if (isThrowNewException) {
                throw TopException.newInstance(ResponseCode.DATA_IS_NULL).text(stringBuilder.append("不能为空！").toString());
            }else{
                return stringBuilder.append("不能为空！").toString();
            }

        }
        return null;
    }
}
