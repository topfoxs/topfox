package cnn.topfox.util;

import cnn.topfox.common.AppContext;
import cnn.topfox.common.SysConfigRead;
import cnn.topfox.data.SqlUnderscore;
import org.springframework.core.env.Environment;

import java.util.concurrent.TimeUnit;

/**
 * 实现类
 * @see  SysConfigDefault
 */
public interface SysConfig extends SysConfigRead {

    Environment environment();
    void environment(Environment environment);

    /**
     * 对应配置文件中的  top.page-size
     */
    void setPageSize(Integer value);

    /**
     * 对应配置文件中的  top.page-max-size
     */
    void setMaxPageSize(Integer value);

    /**
     * 对应配置文件中的  top.service.update-mode
     */
    void setUpdateMode(Integer value);

    /**
     * 对应配置文件中的  top.redis.serializer-json
     */
    void setRedisSerializerJson(Boolean value);

    /**
     * 一级缓存开关
     * 对应配置文件中的  top.service.thread-cache
     */
    void setThreadCache(Boolean value);

    /**
     * 二级缓存开关
     * 对应配置文件中的  top.service.redis-cache
     */
    void setRedisCache(Boolean value);

    /**
     * 对应配置文件中的  top.redis.log
     */
    void setRedisLog(Boolean value);

    /**
     * Redis 过期时间单位
     */
    void setTimeUnit(TimeUnit value);

    /**
     * redis过期时间,单位为小时, 默认 24
     * top.service.redis-expire
     */
    void setRedisExpire(Integer value);
    void setRedisCacheExpire(Integer value, TimeUnit timeUnit);

    /**
     * update-mode = 1 时本参数生效
     * 对应配置文件中的  top.service.select-by-before-update   更新之前是否先查询, 默认false, 不查询
     */
    void setSelectByBeforeUpdate(Boolean value);

    /**
     * 对应配置文件中的  top.service.update-not-result-error
     */
    void setUpdateNotResultError(Boolean value);

    /**
     * 对应配置文件中的  top.service.sql-camel-to-underscore
     */
    void setSqlCamelToUnderscore(SqlUnderscore value);

    /**
     * 对应配置文件中的  top.service.sql-qto-operator
     */
    void setSqlQtoOperator(String value);


}
