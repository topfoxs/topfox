package cnn.topfox.conf;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import cnn.topfox.data.DateTime;
import org.springframework.boot.jackson.JsonComponent;
import java.io.IOException;


/**
 * 解决前台输入转换 的问题 || 后台输出前台一样有用
 */
@JsonComponent
public class DateTimeJsonSerializer extends JsonSerializer<DateTime> {

    @Override
    public void serialize(DateTime cnDate,
                          JsonGenerator jsonGenerator,
                          SerializerProvider serializerProvider
    ) throws IOException {

        if ("".equals(cnDate.valueString()) || "null".equals(cnDate.valueString())){
            jsonGenerator.writeObject(null);
        }else {
            jsonGenerator.writeString(cnDate.valueString());
        }
    }


}
