package cnn.topfox.conf;

import cnn.topfox.annotation.EntityParam;
import cnn.topfox.common.IBean;
import cnn.topfox.data.Field;
import cnn.topfox.data.TableInfo;
import cnn.topfox.filter.ParameterRequestWrapper;
import cnn.topfox.misc.BeanUtil;
import cnn.topfox.misc.Misc;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.util.Iterator;


/**
 * 实现 GET 请求 参数数据 序列化为 pojo对象
 */
public class TopFoxEntityParamRequestParamResolver implements HandlerMethodArgumentResolver {
	public TopFoxEntityParamRequestParamResolver() {

	}

	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return parameter.hasParameterAnnotation(EntityParam.class);
	}

	@Override
	public Object resolveArgument(MethodParameter parameter,
	                              ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
	                              WebDataBinderFactory binderFactory) throws Exception
	{
		Class<?> clazz = parameter.getParameterType();
		Object obj = clazz.newInstance();
		IBean ibean;
		if (obj instanceof IBean){
			ibean = (IBean)obj;
		}else{
			return null;
		}
		Object requestObj = webRequest.getNativeRequest();
		ParameterRequestWrapper request = null;
		if (requestObj instanceof ParameterRequestWrapper){
			request = (ParameterRequestWrapper)requestObj;
		}

		if (request != null && "GET".equals(request.getMethod())) {
			TableInfo tableInfo = TableInfo.get(clazz);
			String key, value;
			//前台ajax请求，会执行这里遍历request中的参数
			Iterator<String> params = webRequest.getParameterNames();
			while (params.hasNext()) {
				key = params.next();
				value = webRequest.getParameter(key);
				Field field = tableInfo.getField(key);
				if (field == null || Misc.isNull(value) || key.equals("_")) continue;
				BeanUtil.setValue(tableInfo, ibean, field, value);
			}
			return ibean;
		}

		return null;
	}
}
