//package cnn.topfox.conf;
//
//
//import cnn.topfox.misc.DateFormatter;
//import com.alibaba.fastjson2.support.spring.http.converter.FastJsonHttpMessageConverter;
//
//import com.alibaba.fastjson2.support.config.FastJsonConfig;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import java.nio.charset.StandardCharsets;
//
////配置后 DTO state 前台传入到后台丢失
//@Configuration
//public class FastJsonConfiguration {
//
//    @Bean
//    public FastJsonHttpMessageConverter fastJsonHttpMessageConverter() {
//        FastJsonHttpMessageConverter converter = new FastJsonHttpMessageConverter();
//
//        converter.setDefaultCharset(StandardCharsets.UTF_8);
//        FastJsonConfig fastJsonConfig = new FastJsonConfig();
//        fastJsonConfig.setDateFormat(DateFormatter.DATETIME_SECOND_FORMAT); // 设置日期格式
//
//        converter.setFastJsonConfig(fastJsonConfig);
//        return converter;
//    }
//}