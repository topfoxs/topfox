package cnn.topfox.conf;

import cnn.topfox.common.*;
import cnn.topfox.data.DataHelper;
import cnn.topfox.misc.CamelHelper;
import cnn.topfox.util.AbstractRestSessionHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.HandlerMethod;

import java.beans.PropertyEditorSupport;
import java.lang.reflect.Method;
import java.util.*;

//@EnableWebMvc
@Slf4j
@RestControllerAdvice
@Component
public class TopfoxGlobalException {
    @Autowired
    @Qualifier("sysConfigDefault")
    protected SysConfigRead sysConfigRead;//单实例读取值 全局一个实例

    @Autowired
    AbstractRestSessionHandler restSessionHandler;

    //日期转换 子类
    public class DateEditor extends PropertyEditorSupport {
        @Override
        public void setAsText(@Nullable String text) throws IllegalArgumentException {
            this.setValue(DataHelper.parseDate(text));
        }
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        //##################################################################
        //提交的数据 有下划线时,  转为驼峰//////////
        if (sysConfigRead.isCommitDataKeysIsUnderscore()) {
            Object data = binder.getTarget();
            if (data instanceof List) {
                List list = (List) data;
                list.forEach(row -> {
                    if (!list.isEmpty() && list.get(0) instanceof Map) {
                        CamelHelper.mapToCamel((Map) list.get(0));//处理下划线 转驼峰
                    }
                });
            }
            if (data instanceof Map) {
                CamelHelper.mapToCamel((Map) data);//处理下划线 转驼峰
            }
        }
        //##################################################################


        //解决前台 查询 GET 传入日期 2018/12/12 报错的问题
        binder.registerCustomEditor(Date.class, new DateEditor());
    }

    /**
     * 全局异常捕捉处理 : TopException CommonException
     */
    @ExceptionHandler(value = {TopException.class, CommonException.class})
    public Response topException(TopException ex, HandlerMethod handlerMethod){
        all(handlerMethod);
        if (ex.getResponse()!=null){
            return ex.getResponse();
        }

        return new Response<>(ex);
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public Response httpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException ex, HandlerMethod handlerMethod){
        all(handlerMethod);
        return new Response<>(ex);
    }

    @ExceptionHandler(NullPointerException.class)
    public Response nullPointerException(NullPointerException ex, HandlerMethod handlerMethod ) {
        all(handlerMethod);
        log.warn(" topfox nullExceptionHandler msg: {}"+ex.getMessage());
        return new Response(ex, ResponseCode.NULLException);
    }

    @ExceptionHandler({HttpMessageNotReadableException.class})
    public Response httpMessageNotReadableException(HttpMessageNotReadableException ex, HandlerMethod handlerMethod){
        all(handlerMethod);
        ex.printStackTrace();
        String message =ex.getMessage();
        message=message==null?"":message;
        if (message.contains("com.fasterxml.jackson.core.JsonParseException")
                && (message.contains("JSON parse error") || message.contains("Unexpected character"))){
            return new Response<>(ex,ResponseCode.PARAM_IS_INVALID, "入参json格式错误.");
        }else if (message.contains("com.fasterxml.jackson.databind.exc.InvalidFormatException")
                && (message.contains("JSON parse error") || message.contains("Cannot deserialize value of type"))){
            return new Response<>(ex,ResponseCode.PARAM_IS_INEXISTENCE, "入参json参数value值类型错误.");
        }else if (message.contains("Required request body is missing")){
            return new Response<>(ex,ResponseCode.PARAM_IS_INEXISTENCE, "入参json格式为空!");
        }else if (message.contains("JSON parse error: Numeric value") && message.contains("out of range of")){
            //数字型超长后,spring json转换也会报错
            return new Response<>(ex,ResponseCode.DATA_IS_TOO_LONG, "数字类型的内容过长, json转换报错. "+message);
        }

        return new Response<>(ex);
    }

    @ExceptionHandler(Throwable.class)
    public Response throwable(Throwable ex, HandlerMethod handlerMethod){
        all(handlerMethod);

        ex.printStackTrace();
        String sql;
        String message =ex.getMessage();
        message=message==null?"":message;

        //TopFox 的异常被 Dubbo拦截了, 转成 RuntimeException 抛出, 所以这里捕获到. 因此还原异常///////////////////////////////////
        if (message.contains("com.topfox.common.CommonException") || message.contains("com.topfox.common.TopException") ){
            return new Response(ex);
        }else if (message.contains("java.lang.NullPointerException")){
            log.warn(" topfox nullExceptionHandler msg: {}"+ex.getMessage());
            return new Response(ex, ResponseCode.NULLException);
        }else if (message.contains("org.springframework.web.HttpRequestMethodNotSupportedException")){
            return new Response<>(ex);
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        String find="Mapped Statements collection does not contain value for";
        int pos =message.lastIndexOf(find);
        if (pos>=0) {
            return new Response<>(ex,ResponseCode.PARAM_IS_INVALID, "可能是*Mapper.xml不存在对应的SQL的id:" + message.substring(pos + find.length()));
        }

        if (message.contains("TooManyResultsException")){
            //TooManyResultsException: Expected one result (or null) to be returned by selectOne()
            return new Response<>(ex,ResponseCode.DB_SELECT_ERROR, "selectOne查询不能返回多条记录");
        }

        if (message.contains("MySQLIntegrityConstraintViolationException")
                || message.contains("SQLIntegrityConstraintViolationException")){

            int pos1=message.indexOf("Duplicate entry '")+"Duplicate entry".length()+2;
            int pos2=message.indexOf("' for key");
            if(message.contains("Duplicate entry '")){
                //Duplicate entry '杭走起源蔚来食品科技有限公司-宋庭缪-17602286876' for key
                String duplicateData=message.substring(pos1, pos2);
                return new Response<>(ex,ResponseCode.DATA_IS_DUPLICATE, "数据重复:"+duplicateData);
            }else if (message.contains("foreign key constraint fails")) {
                return new Response<>(ex,ResponseCode.DATA_IS_INVALID, "非法操作,不符合外键约束");
            }
            return new Response<>(ex);
        }

        StringBuilder stringBuilder = new StringBuilder();
        if (message.contains("Data truncated for column")
                && message.contains("Out of range value for column")
                && message.contains("Data truncation: Incorrect datetime value") //日期超长
        ){
            return new Response<>(ex,ResponseCode.DATA_IS_TOO_LONG, "日期的内容过长,错误的格式");
        }

        if (message.contains("Data too long for column")){
            stringBuilder.setLength(0);
            message =substringMessage(message, "MysqlDataTruncation: Data truncation: ", "\n### The error may involve");
            stringBuilder.append("输入的内容过长,超出设计限制. ").append(message);
            return new Response<>(ex,ResponseCode.DATA_IS_TOO_LONG, stringBuilder.toString());
        }

        if (message.contains("Data truncation: Out of range value for column")){
            //无符号类型 为负数时
            stringBuilder.setLength(0);
            String temp1=message.substring(message.indexOf("Out of range value for column"),message.indexOf(" at row 1"));
            String temp2=message.substring(message.indexOf("### SQL: "),message.indexOf("### Cause:"));
            temp1=temp1.replace("Out of range value for ","");
            temp2=temp2.replace("### SQL: ","").replace("\n","");
            stringBuilder.append("无符号类型不能为负数. ").append(temp1).append(", ").append(temp2);
            return new Response<>(ex,ResponseCode.DATA_IS_TOO_LONG, stringBuilder.toString());
        }

        if (message.contains("MySQLSyntaxErrorException: Unknown column")){
            message =substringMessage(message, "MySQLSyntaxErrorException:", "\n### The error may involve");
            return new Response<>(ex,ResponseCode.DB_UNKNOWN_COLUMN, message);
        }

        if (message.contains("MySQLSyntaxErrorException: Table")){
            message =substringMessage(message, "MySQLSyntaxErrorException:", "\n### The error may involve");
            return new Response<>(ex,ResponseCode.DB_UNKNOWN_TABLE, message);
        }
        if (message.contains("for column ") && message.contains("nested exception")){
            sql     =substringMessage(message, "### SQL: ", "### Cause:");
            message =substringMessage(message, "java.sql.SQLException:", " at row 1");
            stringBuilder.append("插入数据库的值类型不对. ").append(message);
            return new Response<>(ex,ResponseCode.DATA_IS_TOO_LONG, stringBuilder.toString(),sql);
        }

        if (message.contains("error code [1366]")){
            return new Response<>(ex,ResponseCode.DATA_IS_INVALID, "输入的内容不合法,如表情符");
        }

        if (message.contains("MySQLSyntaxErrorException")){
            return new Response<>(ex,ResponseCode.DB_SQL_ERROR, "SQL语句语法错误");
        }

        if (message.contains("UNSIGNED value is out of range in")){
            //相关库存不足
            return new Response<>(ex,ResponseCode.DB_UPDATE_ERROR, "无符号类型不能为负数");
        }

        return new Response<>(ex,ResponseCode.SYSTEM_ERROR, ex.getMessage());
    }

    public static String substringMessage(String message, String start, String end){
        int pos = message.indexOf(start);
        if (pos >= 0){
            message = message.substring(pos+start.length());
        }
        pos = message.indexOf(end);
        if (pos >= 0){
            message = message.substring(0, pos);
        }
        message = message.replaceAll(" +", " ");//去除连续的空格

        return message.trim();
    }
    public void all(HandlerMethod handlerMethod) {
        restSessionHandler.getDataCache().rollbackRedis();

        if (handlerMethod==null) return;
        Method method = handlerMethod.getMethod();
        System.out.println();
        log.error(method.getDeclaringClass().getName()+"."+method.getName());
    }
}
