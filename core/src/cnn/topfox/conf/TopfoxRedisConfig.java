package cnn.topfox.conf;

import com.alibaba.fastjson2.JSONObject;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import cnn.topfox.common.SysConfigRead;
import cnn.topfox.util.CustomRedisTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
//import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.util.ObjectUtils;

@Configuration
@Slf4j
public class TopfoxRedisConfig {
	@Autowired
	@Qualifier("sysConfigDefault")
	protected SysConfigRead sysConfigRead;//单实例读取值 全局一个实例

	@Autowired LettuceConnectionFactory redisConnectionFactory;


	/**
	 * 默认的
	 * @return
	 */
	@Bean
	@ConditionalOnMissingBean(name = "redisTemplate")
	public  RedisTemplate<String, JSONObject> redisTemplate() {
		RedisTemplate<String,JSONObject> redisTemplate = new RedisTemplate();
		redisConnectionFactory.setShareNativeConnection(false);
		redisTemplate.setConnectionFactory(redisConnectionFactory);

		redisTemplate.setEnableTransactionSupport(true);//打开事务支持

		///////////////////////////////////////////////////////////////////////////////////////
//		JdkSerializationRedisSerializer jdkSerializer = new JdkSerializationRedisSerializer();

		redisTemplate.setKeySerializer(new StringRedisSerializer());
		redisTemplate.setValueSerializer(getJacksonSerializer());

		// Hash的key也采用StringRedisSerializer的序列化方式
		redisTemplate.setHashKeySerializer(new StringRedisSerializer());
		redisTemplate.setHashValueSerializer(getJacksonSerializer());

		redisTemplate.afterPropertiesSet();
		//////////////////////////////////////////////////////////////////////////////////////





//		// fastjson序列化
////		FastJsonRedisSerializer fastJsonRedisSerializer = new FastJsonRedisSerializer(Object.class);
//		// key的序列化采用StringRedisSerializer
//		redisTemplate.setKeySerializer    (new StringRedisSerializer());
//		redisTemplate.setHashKeySerializer(jdkSerializer);
//
//		// value的序列化采用FastJsonRedisSerializer
////		redisTemplate.setValueSerializer    (fastJsonRedisSerializer);
////		redisTemplate.setHashValueSerializer(fastJsonRedisSerializer);
//
////		redisTemplate.setValueSerializer    (getJacksonSerializer());
////		redisTemplate.setHashValueSerializer(getJacksonSerializer());

		return redisTemplate;
	}


	Jackson2JsonRedisSerializer jackson2JsonRedisSerializer2=null;
	private Jackson2JsonRedisSerializer getJacksonSerializer(){
		if (jackson2JsonRedisSerializer2==null) {
			jackson2JsonRedisSerializer2 = new Jackson2JsonRedisSerializer(Object.class);
		}

		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
		objectMapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
		jackson2JsonRedisSerializer2.setObjectMapper(objectMapper);
		return jackson2JsonRedisSerializer2;
	}

	/**
	 * 开启事务的
	 * @return
	 */
	@Bean("sysRedisTemplateDTO")//开启事务的
	public CustomRedisTemplate<String, Object> sysRedisTemplateDTO() {
		CustomRedisTemplate<String, Object> sysRedisTemplateDTO = new CustomRedisTemplate();
		redisConnectionFactory.setShareNativeConnection(false);
		sysRedisTemplateDTO.setConnectionFactory(redisConnectionFactory);

		sysRedisTemplateDTO.setEnableTransactionSupport(true);//打开事务支持
		sysRedisTemplateDTO.setHashKeySerializer(RedisSerializer.string());//key值始终用纯字符串序列化
		sysRedisTemplateDTO.setHashValueSerializer(sysConfigRead.isRedisSerializerJson()?getJacksonSerializer():new JdkSerializationRedisSerializer());
		sysRedisTemplateDTO.setKeySerializer(RedisSerializer.string());//key值始终用纯字符串序列化
		sysRedisTemplateDTO.setValueSerializer    (sysConfigRead.isRedisSerializerJson()?getJacksonSerializer():new JdkSerializationRedisSerializer());
		sysRedisTemplateDTO.afterPropertiesSet();//初始化操作）加载配置后执行

		return sysRedisTemplateDTO;//sysRedisTemplateDTO
	}

	/**
	 * 注意,这个是没有事务的
	 *
	 */
	@Bean("sysStringRedisTemplate")
	public CustomRedisTemplate sysStringRedisTemplate(
			@Value("${spring.redis.database}") int database,
			@Value("${spring.redis.host}") String hostName,
			@Value("${spring.redis.port}") int port,
			@Value("${spring.redis.password}") String password
	) {
		//Redis事务 是与 LettuceConnectionFactory 有关的
		//与RedisTemplate无关
		RedisStandaloneConfiguration configuration = new RedisStandaloneConfiguration();
		configuration.setHostName(hostName);
		configuration.setPort(port);
		configuration.setDatabase(database);
		if (!ObjectUtils.isEmpty(password)) {
			RedisPassword redisPassword = RedisPassword.of(password);
			configuration.setPassword(redisPassword);
		}
		LettuceConnectionFactory connectionFactory = new LettuceConnectionFactory(configuration);
		connectionFactory.afterPropertiesSet();

		CustomRedisTemplate<String, Object> sysStringRedisTemplate = new CustomRedisTemplate();
		sysStringRedisTemplate.setConnectionFactory(connectionFactory);
		sysStringRedisTemplate.setEnableTransactionSupport(false);//关闭事务支持
		sysStringRedisTemplate.setKeySerializer(RedisSerializer.string());
		sysStringRedisTemplate.setValueSerializer(RedisSerializer.string());
		sysStringRedisTemplate.setHashKeySerializer(RedisSerializer.string());
		sysStringRedisTemplate.setHashValueSerializer(RedisSerializer.string());
		sysStringRedisTemplate.afterPropertiesSet();//初始化操作）加载配置后执行

		return sysStringRedisTemplate; //stringRedisTemplate
	}




//	/**
//	 * RedisTemplate配置
//	 *
//	 * @return
//	 */
//	@Bean
//	public StringRedisTemplate redisTemplateBigData(
//			@Value("${spring.redis.database}") int database,
//			@Value("${spring.redis.host}") String hostName,
//			@Value("${spring.redis.port}") int port,
//			@Value("${spring.redis.password}") String password) {
//		RedisStandaloneConfiguration configuration = new RedisStandaloneConfiguration();
//		configuration.setHostName(hostName);
//		configuration.setPort(port);
//		configuration.setDatabase(database);
//		if (!ObjectUtils.isEmpty(password)) {
//			RedisPassword redisPassword = RedisPassword.of(password);
//			configuration.setPassword(redisPassword);
//		}
//		LettuceConnectionFactory connectionFactory = new LettuceConnectionFactory(configuration);
//		connectionFactory.afterPropertiesSet();
//		// 设置序列化
//		Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer<Object>(Object.class);
//		ObjectMapper om = new ObjectMapper();
//		om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
//		jackson2JsonRedisSerializer.setObjectMapper(om);
//		// 配置redisTemplate
//		StringRedisTemplate redisTemplateBigData = new StringRedisTemplate();
//		redisTemplateBigData.setConnectionFactory(connectionFactory);
//		RedisSerializer<?> stringSerializer = new StringRedisSerializer();
//		redisTemplateBigData.setKeySerializer(stringSerializer);// key序列化
//		redisTemplateBigData.setValueSerializer(jackson2JsonRedisSerializer);// value序列化
//		redisTemplateBigData.setHashKeySerializer(stringSerializer);// Hash key序列化
//		redisTemplateBigData.setHashValueSerializer(jackson2JsonRedisSerializer);// Hash value序列化
//		redisTemplateBigData.afterPropertiesSet();
//
//		return redisTemplateBigData;
//	}

//	/**
//	 * 注意,这个是没有事务的
//	 */
//	@Bean("sysStringRedisTemplate")
//	public CustomRedisTemplate sysStringRedisTemplate() {
//		CustomRedisTemplate<String, Object> sysStringRedisTemplate = createRedisTemplate();
//		if (sysStringRedisTemplate == null){
//			return null;
//		}
//
//		sysStringRedisTemplate.setEnableTransactionSupport(false);//关闭事务支持
//		sysStringRedisTemplate.setKeySerializer(RedisSerializer.string());
//		sysStringRedisTemplate.setValueSerializer(RedisSerializer.string());
//		sysStringRedisTemplate.setHashKeySerializer(RedisSerializer.string());
//		sysStringRedisTemplate.setHashValueSerializer(RedisSerializer.string());
//		sysStringRedisTemplate.afterPropertiesSet();//初始化操作）加载配置后执行
//
//		return sysStringRedisTemplate; //stringRedisTemplate
//	}
//
//	@Autowired(required = false) LettuceConnectionFactory redisConnectionFactory;
//	private CustomRedisTemplate createRedisTemplate() {
//		if (redisConnectionFactory == null ) {
//			return null;
//		}
//
//		CustomRedisTemplate<String, Object> redisTemplate = new CustomRedisTemplate();
//		redisConnectionFactory.setShareNativeConnection(false);
//		redisTemplate.setConnectionFactory(redisConnectionFactory);
//		return redisTemplate;
//	}
}