package cnn.topfox.conf;

import cnn.topfox.common.DataDTO;
import cnn.topfox.common.DataPojo;
import cnn.topfox.common.IBean;
import cnn.topfox.common.Response;
import cnn.topfox.data.DbState;
import cnn.topfox.misc.JsonUtil;
import cnn.topfox.service.ISuperService;
import cnn.topfox.service.SuperService;
import cnn.topfox.sql.Condition;
import cnn.topfox.sql.EntitySelect;
import cnn.topfox.util.SysConfig;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Slf4j
@Aspect
@Component
public class TopFoxServiceAopConfig {
    @Autowired
    private SysConfig sysConfig;//单实例读取值

    /**
     * 针对controller层入参/返回的数据格式化输出到 log里
     */
    @Pointcut(value =
               " execution(public * cnn.*.service..*.*(..)) "
            +"|| execution(public * cnn.*.service.*..*.*(..)) "
            +"|| execution(public * cnn.topfox.service.SuperService.*(..))"
            +"|| execution(public * cnn.topfox.service.SimpleService.*(..))"
            +"|| execution(public * cnn.topfox.service.AdvancedService.*(..))"
    )
    public void remoteLogPrint(){
    }

    @Around("remoteLogPrint()")
    public Object around(ProceedingJoinPoint joinPoint)throws Throwable{
        long start = System.currentTimeMillis();
        Object object;
        String methodName = joinPoint.getSignature().getName();
        Object[] args = joinPoint.getArgs();

        ISuperService iSuperService;
        Object target=joinPoint.getTarget();
        if (target instanceof SuperService){
            if ("beforeInit".equals(joinPoint.getSignature().getName())){
                return null;
            }
            iSuperService=(ISuperService)target;
            List<Object> listUpdateDelete=null;//
            for(Object arg : args) {
                if (arg instanceof ArrayList) {
                    //更新时, dto 数据处理
                    ArrayList lisTemp = (ArrayList) arg;
                    if (lisTemp.isEmpty()) break;
                    Object dto = lisTemp.get(0);
                    if (dto instanceof DataDTO == false) break;

                    if (methodName.equals("updateList") || methodName.equals("deleteList")){
                        listUpdateDelete = lisTemp;
                    }else if (methodName.startsWith("save")){
                        ArrayList<DataDTO> listAll = (ArrayList<DataDTO>)lisTemp;

                        listUpdateDelete = listAll.stream().filter(beanDTO ->
                                DbState.UPDATE.equals(beanDTO.dataState())
                                        || DbState.DELETE.equals(beanDTO.dataState())).collect(toList()
                        );//所有更新的记录
                    }
                }else if(arg instanceof DataDTO && (methodName.equals("update") || methodName.equals("delete"))
//                        && !methodName.equals("updateBatch"))//这是个例外
                ){
                    //当个DTO的更新
                    listUpdateDelete = new ArrayList();
                    listUpdateDelete.add(arg);
                }
            }
            iSuperService.beforeInit(listUpdateDelete);// listUpdate 更新时 对 DTO的处理用, 重要.
        }

        try {
            if (log.isDebugEnabled()) {
//                StringBuilder remote_begin = new StringBuilder();
//                remote_begin.append("▼▼▼▼▼▼ remote-begin : ")
//                        .append(joinPoint.getTarget().getClass().getName())
//                        .append(".")
//                        .append(joinPoint.getSignature().getName())
//                        .append("()");
//                log.debug(remote_begin.toString());
//
//                int i = 1;
//                for (Object arg : args) {
//                    arg = arg == null ? "null" : arg;
//                    StringBuilder remote_param = new StringBuilder();
//                    remote_param.append(sysConfig.getLogPrefix()).append("remote-param(")
//                            .append(i++)
//                            .append(") : ");
//                    if (arg instanceof Condition) {
//                        remote_param.append("Condition");
//                    }else if (arg instanceof EntitySelect){
//                        remote_param.append("EntitySelect");
//                    }else if (arg instanceof DataPojo){
//                        remote_param.append(JsonUtil.toString(arg));
//                    }else{
//                        remote_param.append(arg.toString());
//                    }
//                    log.debug(remote_param.toString());
//                }
            }
        }finally {

        }

        object = joinPoint.proceed();

        try {
            // debug 时, 打印信息处理
            if (log.isDebugEnabled()) {
                long end = System.currentTimeMillis();
                StringBuilder time_use = new StringBuilder();
                time_use.append(sysConfig.getLogPrefix()).append("remote-time-consuming : ")
                        .append(end - start)
                        .append("ms");
                log.debug(time_use.toString());

//                if (object != null) {
//                    StringBuilder time_response = new StringBuilder();
//                    time_response.append(sysConfig.getLogPrefix()).append("remote-response : ");
//                    if (object instanceof Number || object instanceof String) {
//                        time_response.append(object);
//                    }else if (object instanceof DataDTO){
//                        time_response.append(object.toString());
//                    }else if (object instanceof IBean || object instanceof List || object instanceof Response){
//                        time_response.append(JsonUtil.toJson(object));
//                    }else{
//                        time_response.append(object.getClass().getName());
//                    }
//                    log.debug(time_response.toString());
//                }

                StringBuilder time_end = new StringBuilder();
                time_end.append("▲▲▲▲▲▲ remote-end : ")
                        .append(joinPoint.getTarget().getClass().getName())
                        .append(".")
                        .append(joinPoint.getSignature().getName())
                        .append("()");
                log.debug(time_end.toString());
            }
        }finally {
        }

        return object;
    }

}
