package cnn.topfox.conf;


import cnn.topfox.util.KeyBuild;
import cnn.topfox.util.SysConfig;
import cnn.topfox.common.AbstractRestSession;
import cnn.topfox.util.AbstractRestSessionHandler;
import cnn.topfox.misc.BeanUtil;
import cnn.topfox.misc.Misc;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.*;
import java.lang.reflect.Method;

@Slf4j
public class TopfoxWebMvcInterceptor implements HandlerInterceptor {
    @Autowired
    private SysConfig sysConfigSource;//单实例读取值

    @Autowired
    AbstractRestSessionHandler abstractRestSessionHandler;


    @Autowired
    private SysConfig sysConfig;//单实例读取值

    public boolean checkStop(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if ("OPTIONS".equals(request.getMethod()) || request.getRequestURI().indexOf(".")>0){
            //跨域请求 发出的OPTIONS请求 或者 文件访问请求(如.html .js .css 等)
            return true;
        }

        if (request.getRequestURI().indexOf("/error")>=0){
            return true;//错误页面,哪里 发出的请求??
        }

        if (handler instanceof HandlerMethod == false) {
            return true;
        }
        return false;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (checkStop(request, response, handler)) return true;
        response.setHeader("Access-Control-Expose-Headers","head");

        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        log.debug("{}{}", sysConfig.getLogPrefix(), handlerMethod.getMethod().toString());
        //log.debug("User-Agent = {}", request.getHeader("User-Agent"));

        AbstractRestSession restSession = abstractRestSessionHandler.create();  //创建 restSession
        restSession.setSysUserAgent(request.getHeader("User-Agent").toLowerCase());//用户代理头
        restSession.setSessionId (request.getHeader("X-sessionId"));
        restSession.setRouter    (request.getHeader("X-router"));
        restSession.setPhoneOS   (request.getHeader("X-phoneOS"));
        restSession.setOpenId    (request.getHeader("X-openId"));
        restSession.setExecuteId (request.getHeader("X-executeId"));
        restSession.setRequestURI(request.getRequestURI()==null?"":request.getRequestURI());
        restSession.setRouter(restSession.getRouter()==null?"":restSession.getRouter());
        restSession.setPhoneOS(restSession.getPhoneOS()==null?"":restSession.getPhoneOS());
        restSession.setOpenId(restSession.getOpenId()==null?"":restSession.getOpenId());

        //AppContext.setRestSessionHandler(restSessionHandler);
        Object ___requestData = request.getAttribute("___requestData");
        if (___requestData !=null && ___requestData instanceof  JSONObject) {
            restSession.setRequestData((JSONObject) ___requestData);//restSession.setRequestData(JSON.parseObject(___requestData.toString()));
            request.removeAttribute("___requestData");
        }

        Object ___headData = request.getAttribute("___headData");
        if (___headData !=null && ___headData instanceof  JSONObject) {
            JSONObject headData= (JSONObject) ___headData;
            JSONObject headers = headData.getJSONObject("x-headers");//前台dataset.header的数据
            if (headers!=null){
                for (String key : headers.keySet()){
                    Object value = headers.get(key);
                    if (key.startsWith("x-")==false){
                        headData.put("x-"+key, value);
                    }
                }
            }
            headData.remove("x-headers");
            restSession.setHeaders(headData);
            request.removeAttribute("___headData");
        }

        //restSession.beforeInit();

        ////////////////////////////////////////////////////////////////////////////
        BeanUtil.map2Bean(restSession.getRequestData(),restSession);
        //没有ExecuteId 执行Id/主事务号  自动生成一个
        restSession.setExecuteId(Misc.isNull(restSession.getExecuteId())? KeyBuild.getKeyId():restSession.getExecuteId());
        abstractRestSessionHandler.getDataCache().setExecuteId(restSession.getExecuteId());
        //restSessionHandler.set(restSession);
        abstractRestSessionHandler.initRestSession(restSession, method);

//        //始终生成一个新的 SubExecuteId 子事务号
//        restSession.setSubExecuteId(KeyBuild.getKeyId());
//        restSession.setSysActiveDate(System.currentTimeMillis());
//        restSession.setSysNowTime(new Date());
//        restSession.setAppName(sysConfigSource.getAppName());


        //原始数据
        Object ___formData = request.getAttribute("___formData");
        if (___formData != null) {
            request.removeAttribute("___formData");
            JSONArray bodyDataArray = null;
            JSONObject bodyData=null;

            String formData = ___formData.toString();
            try {
                bodyDataArray= JSON.parseArray(formData);
            }catch(Exception e1){
                try {
                    bodyData = JSONObject.parseObject(formData);
                    if (bodyDataArray == null){
                        bodyDataArray = new JSONArray(1);
                    }
                    bodyDataArray.add(bodyData);
                }catch(Exception e2){
                }
            }
            restSession.setBodyData(bodyDataArray); //put post提交的formData数据
            if (Misc.isDebugEnabled() && bodyDataArray != null) {
                //String temp =JSONObject.toJSONString(bodyDataArray,false);
                //10万个字符以下才打印
                if (formData.length() <= 999999) {
                    log.debug("{}提交的formData数据: {}", sysConfig.getLogPrefix(), formData);
                }
            }
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response,
                           Object handler, ModelAndView modelAndView) throws Exception {


        if (checkStop(request, response, handler)) return;

//        abstractRestSessionHandler.getDataCache().commitRedis();//将增删改的DTO同步保存到redis中
//        AbstractRestSession restSession= abstractRestSessionHandler.get();
//
//        long end = System.currentTimeMillis();
//        long activeDate=restSession==null?0:restSession.getSysActiveDate();
//        log.debug( "{}运行结束 共耗时{}毫秒",sysConfig.getLogPrefix(), Misc.fillStr((end - activeDate) + "", 4, " "));

        abstractRestSessionHandler.dispose();//释放 RestSession

    }

    /**
     * 判断浏览器是否支持 gzip 压缩
     * @param req
     * @return boolean 值
     */
    public boolean isGzipSupport(HttpServletRequest req) {
        String headEncoding = req.getHeader("accept-encoding");
        return headEncoding != null && (headEncoding.indexOf("gzip") != -1);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception e) throws Exception {
        if (checkStop(request, response, handler)) return;


    }
}