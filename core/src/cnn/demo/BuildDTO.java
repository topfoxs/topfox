package cnn.demo;


import cnn.topfox.misc.Misc;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class BuildDTO {
    static List<String> listErr = new ArrayList();

    public static void main(String[] args) {
        List<File> listFind = findFile("/Users/user/work/hd", "DTO.java","QTO.java");//
        for (File file : listFind){
            readFile(file);
        }

        System.out.println("##一行多个 private ");
        for (String line : listErr){
            System.out.println(line);
        }
    }

    public static void readFile(File file) {
        StringBuffer sbFileText=new StringBuffer();

        BufferedReader reader;
        try {
            reader =new BufferedReader(new InputStreamReader(new FileInputStream(file), "utf-8"));
            boolean isExtends=false;//是否读取到字段定义的行
            String lineText;//每行的内容
            while ((lineText = reader.readLine()) != null) {
                if (lineText.contains("serialVersionUID")
                        || lineText.trim().startsWith("@JsonFormat")
                        || lineText.trim().startsWith("@TableField")
                        || lineText.trim().equals("@Other")
                        || lineText.trim().equals("@FillInsert")
                        || lineText.trim().equals("@FillBoth")
                        || lineText.trim().equals("@FillUpdate")
                        || lineText.trim().startsWith("/**")
                        || lineText.trim().startsWith("*")
                        || lineText.trim().startsWith("//")

                ){
                    sbFileText.append(lineText).append("\n");
                    continue;
                }
                if (isExtends==false && lineText!=null && lineText.contains("extends") ){
                    isExtends=true;
                    System.out.println("###");
                    System.out.println("###");
                    System.out.println("类:  "+lineText);
                    sbFileText.append(lineText).append("\n");
                    continue;
                }
                if (isExtends){
                    if (lineText.startsWith(" ")){
                        //第一个字符是空格
                        lineText="\t"+lineText.trim();
                    }
                    if (Misc.countString(lineText, "private")>1){
                        listErr.add(file.getPath()+" : "+lineText);
                    }

                    lineText=lineText.replaceAll("\t"," ");//tab 替换为一个空格
                    lineText=lineText.replaceAll("  "," ");//两个空格替换为一个空格
                    lineText=lineText.replaceAll("  "," ");
                    lineText=lineText.replaceAll("  "," ");
                    lineText=lineText.replaceAll("  "," ");
                    lineText=lineText.replaceAll("////","//");lineText=lineText.replaceAll("////","//");lineText=lineText.replaceAll("////","//");lineText=lineText.replaceAll("////","//");

                    lineText=lineText.replaceAll("@Other private","@Other");
                    lineText=lineText.replaceAll("@Id private","@Id");

                    if ((lineText.length()<=4 && lineText.length()>1)
                            || lineText.trim().startsWith("@Ignore")
                            || lineText.trim().startsWith("List<")
                    ){
                        sbFileText.append("\t"+lineText.trim()).append("\n");
                        continue;
                    }
                    String[] codeString = lineText.split("//");
                    String codeRight=null;
                    String codeZhushi=null;
                    if (codeString.length>=1){
                        codeRight=codeString[0].trim();
                    }
                    if (codeString.length>=2){
                        codeZhushi=codeString[1].trim();
                    }


                    codeZhushi=codeZhushi==null?"":codeZhushi;
                    if (codeRight!=null && codeRight.contains(" ")){
                        String[] arrayCode = codeRight.split(" ");
                        int size = arrayCode.length;
                        if (size==0){
                            System.out.println(lineText);
                            sbFileText.append(lineText).append("\n");
                        }
                        String newLineRight = "";
                        if (size>=1) {
                            newLineRight = "\t"+Misc.fillStr2(arrayCode[0], 8, " "); //修饰符 入private
                        }
                        if (size>=2){
                            newLineRight += " "+Misc.fillStr2(arrayCode[1],9," "); //类型 入 String DateTime
                        }
                        if (size>=3) {
                            newLineRight += " "+arrayCode[2]; //字段名
                        }
                        if (size>=4) {
                            newLineRight += " "+arrayCode[3] + " ";
                        }

                        String newLine = Misc.fillStr2(newLineRight,44," ") + "//"+codeZhushi; //注释
                        System.out.println(lineText);
                        sbFileText.append(newLine).append("\n");
                    }else{
                        System.out.println(lineText);
                        sbFileText.append(lineText).append("\n");
                    }
                }else{
                    sbFileText.append(lineText).append("\n");

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("完毕");
        System.out.println("");System.out.println("");System.out.println("");System.out.println("");System.out.println("");
        System.out.println(sbFileText.toString());

        //写入文件
        BufferedWriter bufferedWriter= null;
        try {
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file.getPath()),"utf-8"));
            bufferedWriter.write(sbFileText.toString());

        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (bufferedWriter!=null) {
                try {
                    bufferedWriter.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }

    }














    /**
     *
     * @param path    源码路径
     * @param filters 筛选条件 文件名结尾包含的字符
     * @return
     */
    public static List<File> findFile(String path, String ... filters){
        File rootFile=new File(path);

        List<File> list = new ArrayList<>();
        findTheSameFile(rootFile, ".java", list);

        List<File> listFind = new ArrayList<>();
        for (File file : list){
            if (filters==null){
                listFind.add(file);
            }else{
                for (String filter: filters){
                    if (file.getName().endsWith(filter)){
                        listFind.add(file);
                    }
                }
            }
        }
        list.clear();
        return listFind;
    }
    public  static List<File> findTheSameFile(File f,String ext,List<File>list){
        if(f.isDirectory()){
            File[] list2=f.listFiles();
            if(list2!=null&&list2.length>0){
                for(File f2:list2){
                    if(f2.isDirectory()) {
                        list = findTheSameFile(f2, ext, list);
                    }else{
                        if(isTheSame(f2, ext))list.add(f2);
                    }
                }
            }
        }else{
            if(isTheSame(f, ext))list.add(f);
        }
        return list;
    }

    /*
     * 递归方式----替换
     */
    public synchronized static boolean isTheSame(File f,String ext){
        boolean isT=false;
        String name=f.getName().trim();
        ext="^"+ext.trim().replaceAll("[\\s]{1,10}", "^").replaceAll("[/]{1,10}", "^").replaceAll("[\\\\]{1,10}", "^")+"^";
        String hz=name.indexOf(".")==-1?name:"^"+name.substring(name.lastIndexOf("."), name.length())+"^";
        if(ext.indexOf(hz)>=0)isT=true;
        return isT;
    }

}
