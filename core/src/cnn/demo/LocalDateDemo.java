package cnn.demo;

import cnn.topfox.data.DataHelper;
import cnn.topfox.data.DateTime;
import cnn.topfox.misc.DateUtils;

import java.text.DateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;


/**
 日期类型对象使用说明
 1. 禁用类型
     LocalDateTime  禁止使用
     Date                  禁止使用

 2. 可用类型，以下均不要格式注解：
     LocaDate   yyyy-MM-dd 数据时用
     LocalTime  HH:mm:ss   数据时用
     DateTime   yyyy-MM-dd  HH:mm:ss 或 yyyy-MM-dd  HH:mm 数据时用

 */
public class LocalDateDemo {
    public static void main(String[] args) {
        /** 1. 获取当前时间*/
        LocalDate today = LocalDate.now();
        LocalTime time  = LocalTime.now();
        //创建时间日期对象
        LocalDate localDate11 = LocalDate.of(2020, 1, 1);// 2020-01-01
        LocalDate localDate12 = LocalDate.parse("2024-01-01");
        LocalDate localDate13 = DataHelper.parseLocalDate("2024-01-01");
        LocalDate localDate14 = DataHelper.parseLocalDate("2024年01月01日");
        LocalDate localDate15 = DataHelper.parseLocalDate("2024/01/01");

        /** 2. 日期大小比较, 统一用 compareTo */
        LocalDate localDate1 = LocalDate.of(2020, 1, 2);
        LocalDate localDate2 = LocalDate.of(2020, 1, 1);
        if (localDate1.compareTo(localDate2)>0 ){
            System.out.println("localDate1 > localDate2");
        }
        if (localDate1.compareTo(localDate2)==0){
            System.out.println("localDate1 = localDate2");
        }

        /** 3. 日期时间的加减运算 */
        //原始变量 localDate
        LocalDate localDate = LocalDate.now(); // 当前日期
        /**
             以下方法的参数都是long型，返回值是新的DateTime,
             而注意始终不会改变 原始变量 localDate 的值
         **/
        LocalDate plusDaysResult1   = localDate.plusDays(7);      //当前日期加7天

        //当前日期减7天. 不推荐用 minusDays, 用 plusDays(-7)即可/////////////////////////////////
        LocalDate plusDaysResult2   = localDate.plusDays(-7);     //当前日期减7天///
        LocalDate plusDaysResult3   = localDate.minusDays(7); //当前日期减7天///
        /////////////////////////////////////////////////////////////////////////////////////

        LocalDate plusMonthsResult  = localDate.plusMonths(3L);//当前日期加3个月
        LocalDate plusYearsResult   = localDate.plusYears(-2L);  //当前日期减两年

        /** 或使用plus(long amountToAdd(偏移量), TemporalUnit unit(单位))方法 */
        LocalDate nextDay1  = localDate.plus(1, ChronoUnit.DAYS);   //当前日期加1天
        LocalDate nextDay2  = localDate.plus(-1,ChronoUnit.DAYS);   //当前日期减1天
        LocalDate nextMonth1= localDate.plus(1, ChronoUnit.MONTHS); //当前日期加1月
        LocalDate nextMonth2= localDate.plus(-2,ChronoUnit.MONTHS); //当前日期减2月
        LocalDate nextYear  = localDate.plus(1, ChronoUnit.YEARS);
        LocalDate nextWeek  = localDate.plus(1, ChronoUnit.WEEKS);

        /** 4. 直接修改当前日期到指定日期 */
        localDate = LocalDate.now(); // 当前日期
        LocalDate withDayOfYear  = localDate.withDayOfYear(200); // 本年当中的第几天，取值范围为1-365,366
        LocalDate withDayOfMonth = localDate.withDayOfMonth(5); // 本月当中的第几天，取值范围为1-29,30,31
        LocalDate withYear       = localDate.withYear(2020); // 指定年份
        LocalDate withMonth      = localDate.withMonth(5);  // 指定月份

            /** 作业: */
            //如何求得上月最后一天
            LocalDate lastDay1 = LocalDate.now()
                    .withDayOfMonth(1)//将日设置为1日
                    .plusDays(-1);//减1天即为上月的最后一天
            //2024年2月的最后一天. 2月最后一天可能是28 也可能是29
            LocalDate lastDay3 = LocalDate.of(2024,3,1).plusDays(-1);

            //如何求得本月最后一天////////////////////////////////////////////////////////////////////////
            LocalDate today1 = LocalDate.now();
            // 获取当月的第一天
            LocalDate firstDayOfMonth = today1.withDayOfMonth(1);
            // 获取下个月的第一天
            LocalDate firstDayOfNextMonth = firstDayOfMonth.plusMonths(1);
            // 计算当月最后一天
            LocalDate lastDayOfMonthA = firstDayOfNextMonth.plusDays(-1);
            // 输出结果
            System.out.println("当月最后一天是：" + lastDayOfMonthA.getDayOfMonth());

            //流式写法如下
            LocalDate lastDayOfMonthB = LocalDate.now()
                    .withDayOfMonth(1)         //获取当月的第一天
                    .plusMonths(1)//获取下个月的第一天
                    .plusDays(-1);  //减1天即为上月的最后一天
            /////////////////////////////////////////////////////////////////////////////////////////////


        /** 5. 获取日期的明细-年月日周时分秒 */
        int dayOfYear  = localDate.getDayOfYear();   // 本年中的第几天
        int dayOfMonth = localDate.getDayOfMonth();  // 本月当中第几天
        DayOfWeek dayOfWeek = localDate.getDayOfWeek(); // 本周当中第几天
        int year = localDate.getYear();              // 当前年
        Month month      = localDate.getMonth();     // 当前月
        int   monthValue = localDate.getMonthValue();// 当前月
        int   day        = localDate.getDayOfMonth();// 当前日

        /** 6. 判断闰年 */
        localDate2.isLeapYear();

        // Instant和Date之间的转换
        Instant instant = Instant.now();
        Date date = Date.from(instant);
        Instant instant2 = date.toInstant();

        /** 7. 计算日期间隔  见 DateTimeDemo.java */


        /** 8. 格式化用法 */
        String format1 = DateTimeFormatter.ofPattern("yyyy/MM/dd").format(LocalDate.now());
        String format2 = DateTimeFormatter.ofPattern("yyyy年MM月dd日").format(LocalDate.now());
        String format3 = DateTimeFormatter.ofPattern("yyyyMMdd").format(LocalDate.now());
        String format4 = DateTimeFormatter.ofPattern("yyyyMM").format(LocalDate.now());
        String dateStr = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));

        /** 9. 日期转换 */
        // LocalDate->Date
        Date date22 = Date.from(LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

        // Date->LocalDate
        LocalDate localDate22 = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        // Date->LocalDateTime
        LocalDateTime localDateTime22 = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

        System.out.println("完毕");
    }
}
