package cnn.demo;

import cnn.topfox.data.DateTime;
import cnn.topfox.misc.DateUtils;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 日期类型对象使用说明
 1. 禁用类型
     LocalDateTime  禁止使用
     Date                  禁止使用

 2. 可用类型，以下均不要格式注解：
     LocaDate   yyyy-MM-dd 数据时用
     LocalTime  HH:mm:ss   数据时用
     DateTime   yyyy-MM-dd  HH:mm:ss 或 yyyy-MM-dd  HH:mm 数据时用
 */
public class DateTimeDemo {
    public static void main(String[] args) {
        /** 1. 获取当前时间 */
        DateTime  now1  = new DateTime();               //当前日期时间
        DateTime  now2  = DateTime.now();               //当前日期时间
        String nowStr1 = now1.toString();   //含星期的 样例:2024-01-01 15:23:01 星期三
        String nowStr2 = now1.valueString();//不含星期 样例:2024-01-01 15:23:01

        /** 2. 大小比较 统一用 compareTo */
        DateTime dateTime1 = DateTime.parse("2020-01-01 01:01:59");
        DateTime dateTime2 = DateTime.parse("2020-01-01 01:01:58");
        int result = dateTime1.compareTo(dateTime2);
        if (dateTime1.compareTo(dateTime2)>0){
            System.out.println("dateTime1 > dateTime2");
        }
        if (dateTime1.compareTo(dateTime2)==0){
            System.out.println("dateTime1 = dateTime2");
        }

        /** 3. 创建时间日期对象 */
        DateTime  dateTime21 = DateTime.of(2020, 1, 1);                    // 2020-01-01 00:00:00
        DateTime  dateTime22 = DateTime.of(2020, 1, 1, 1);           // 2020-01-01 01:00:00
        DateTime  dateTime23 = DateTime.of(2020, 1, 1, 1, 1);            // 2020-01-01 01:01:00
        DateTime  dateTime24 = DateTime.of(2020, 1, 1, 1, 1, 1); // 2020-01-01 01:01:01


        /** 4. 依据其他日期类型 创建 DateTime */
        DateTime  toDatetime1  = new DateTime(LocalDateTime.now());
        DateTime  toDatetime2  = new DateTime(LocalDate.now());
        DateTime  toDatetime3  = new DateTime(new Date());
        DateTime  toDatetime4  = DateTime.of(LocalDate.now());
        DateTime  toDatetime5  = DateTime.of(new Date());
        //一天的起始时间和结束时间
        LocalDate nowDate=LocalDate.now();
        DateTime  toDatetime6 = DateTime.of(LocalDate.now(), LocalTime.MIN);// 小时分钟秒均为0
        DateTime  toDatetime7 = DateTime.of(LocalDate.now(), LocalTime.MAX);// 23:59:59.999
        //2天后的最大时间
        DateTime  toDatetime8 = DateTime.of(LocalDate.now().plusDays(2), LocalTime.MAX);// 23:59:59.999

         /** 5. DateTime 转 Date LocalDate LocalDateTime */
        DateTime  datetime  = new DateTime();
        Date      dateZH    = datetime.toDate();
        LocalDate localDate = datetime.toLocalDate();
        LocalTime localTime = datetime.toLocalTime();
        LocalDateTime dtime = datetime.toLocalDateTime();

        /** 6. 字符串 转 DateTime 对象 */
        DateTime parse01 = DateTime.parse("2020-01-01");            //格式1
        DateTime parse02 = DateTime.parse("2020-01-01 01:01");      //格式2
        DateTime parse03 = DateTime.parse("2020-01-01 01:01:01");   //格式3
        DateTime parse04 = DateTime.parse("2020-01-01 01:01:01.999");//格式4 纳秒自动丢弃
        DateTime parse05 = DateTime.parse("2020-01-01T01:01:01.999");//日期与时间的间隔支持 空格或者T

        //new DateTime(字符串 仅支持一下3中格式)
        DateTime parse11 = new DateTime("2020-01-01");             //格式1
        DateTime parse12 = new DateTime("2020-01-01 01:01");       //格式2
        DateTime parse13 = new DateTime("2020-01-01 01:01:01");    //格式3
        DateTime parse14 = new DateTime("2020-01-01 01:01:01.999");//格式4 纳秒自动丢弃
        DateTime parse15 = new DateTime("2020-01-01T01:01");       //格式5
        DateTime parse16 = new DateTime("2020-01-01T01:01:01");    //格式6
        DateTime parse17 = new DateTime("2020-01-01T01:01:01.999");//格式7 纳秒自动丢弃


        //指定格式转DateTime对象, 需与字符串保持一致
        DateTime parse = DateTime.parse("2020年01月01日 01:01:01", "yyyy年MM月dd日 HH:mm:ss");
        // 毫秒值转换为日期
        DateTime df= DateTime.parse(System.currentTimeMillis());

        /** 7. 日期时间加减减运算 (减法传负数) */
        // 原始变量dateTime
        DateTime  dateTime = DateTime.of(2020, 1, 31,2,23,59);
        /**
          以下方法的参数都是long型，返回值是新的DateTime,
          而注意始终不会改变原始变量dateTime的值
         **/
        DateTime dateTimeNew = dateTime.plusHours(-1L).withMinute(59).withSecond(59); //-1小时 并设置为59分59秒

        DateTime plusYearsResult   = dateTime.plusYears(-2L);  // 当前日期 减 两年
        DateTime plusMonthsResult  = dateTime.plusMonths(1L);  // 当前日期加1个月
        DateTime plusDaysResult    = dateTime.plusDays(7L);    // 当前日期加7天
        DateTime plusHoursResult   = dateTime.plusHours(2L);   // 当前日期加2小时
        DateTime plusMinutesResult = dateTime.plusMinutes(10L);// 当前日期加10分钟
        DateTime plusSecondsResult = dateTime.plusSeconds(10L);// 当前日期加10秒
        // 或使用plus(long amountToAdd(偏移量), TemporalUnit unit(单位))方法
        DateTime nextYear = dateTime.plus(1,  ChronoUnit.YEARS); //等同dateTime.plusYears(1L)
        DateTime nextMonth= dateTime.plus(2,  ChronoUnit.MONTHS);//等同dateTime.plusMonths(2L)
        DateTime nextDay1 = dateTime.plus(3,  ChronoUnit.DAYS);  //等同dateTime.plusDays(3L)
        DateTime nextDay2 = dateTime.plus(-2, ChronoUnit.DAYS);  //当前日期 减 2天

        /** 8. 计算日期间隔 LocalDate DateTime */
        LocalDate localDate1 = LocalDate.of(2020, 1, 1);
        LocalDate localDate2 = LocalDate.of(2021, 2, 5);
        DateTime  datetime1  = DateTime.of(2023, 12, 31, 1, 1, 1);
        DateTime  datetime2  = DateTime.now();

        //直接获取 间隔天数 DateUtils.betweenDays
        int  days21 = DateUtils.betweenDays(localDate1, localDate2);  /** 备注: 参数2 大于 参数1 才是正数 */
        int  days22 = DateUtils.betweenDays(datetime1,  datetime2);   /** 备注: 参数2 大于 参数1 才是正数 */

        //LocalDate 间隔值
        long yearsA = ChronoUnit.YEARS.between(localDate1, localDate2); //间隔年
        long monthA = ChronoUnit.MONTHS.between(localDate1, localDate2);//间隔月
        long daysA  = ChronoUnit.DAYS.between(localDate1, localDate2);  //间隔日 等价  DateUtils.betweenDays(localDate1, localDate2)


        //#########################  多个间隔值 #######################################################################
        //多个间隔值
        System.out.println("\nLocalDate 时间间隔");
        Period period = DateUtils.between(localDate1, localDate2);     /** 备注: 参数2 大于 参数1 才是正数 */
        String info=localDate1+" ~ "+localDate2+" 的时间间隔是: "
                +period.getYears()+"年 "
                +"零"+period.getMonths()+"月 "
                +"零"+period.getDays()+"日"
                +" (即"+daysA+"天)"
                ;

        System.out.printf(info);

        //DateTime 多个间隔值
        System.out.println("\n\nDateTime 时间间隔");

        Duration between = DateUtils.between(datetime1, datetime2);/** 备注: 参数2 大于 参数1 才是正数 */
        long days    = between.toDays();                               //间隔日
        long hours   = between.toHours();  // 间隔小时
        long minutes = between.toMinutes();// 间隔分钟
        long millis  = between.toMillis(); // 间毫秒
        System.out.printf(datetime1+ " ~ "+ datetime2+" 间隔:");
        System.out.printf("%d天---即%d小时---即%d分---即%d秒 \n",
                between.toDays(), between.toHours(),between.toMinutes(),between.getSeconds());
        //############################################################################################################

        /** 9. 直接修改当前日期到指定日期 */
        dateTime = DateTime.now(); // 当前日期
        DateTime withYear      = dateTime.withYear(2020);   // 指定年份
        DateTime withMonth     = dateTime.withMonth(5);     // 指定月份
        DateTime withDayOfMonth= dateTime.withDayOfMonth(5);// 指定日
        DateTime withDayOfYear = dateTime.withDayOfYear(5); // 指定日

        /** 10. 获取日期的明细-年月日周时分秒 */
        dateTime = DateTime.now(); // 当前日期
        int dayOfYear  = dateTime.getDayOfYear();  // 本年中的第几天  0~365
        int dayOfMonth = dateTime.getDayOfMonth(); // 本月当中第几天 0~31

        DayOfWeek dayOfWeek = dateTime.getDayOfWeek();  // 英文的周
        int dayOfWeekInt    = dateTime.getWeekValue();  // 数字的周几: 0123456  0表示周日
        String dayOfWeek1   = dateTime.getWeek1();      // 中文的: 周一   周二 ...
        String dayOfWeek2   = dateTime.getWeek2();      // 中文的: 星期一 星期二 ...

        int year = dateTime.getYear();             // 当前年
        Month month    = dateTime.getMonth();      // 当前月-英文的月 JANUARY FEBRUARY等
        int monthValue = dateTime.getMonthValue(); // 当前月
        int day    = dateTime.getDayOfMonth();     // 当前日
        int hour   = dateTime.getHour();           // 当前小时
        int minute = dateTime.getMinute();         // 当前分钟
        int second = dateTime.getSecond();         // 当前秒



        /** 11. 判断闰年 */
        DateTime dateTime6 = DateTime.now(); // 当前日期
        dateTime6.toLocalDate().isLeapYear();

        // Instant和Date之间的转换
        Instant instant = Instant.now();
        Date date = Date.from(instant);
        Instant instant2 = date.toInstant();

        /** 12. 格式化用法 */
        DateTime date5 = DateTime.now();
        String format1 = date5.format();//默认 "yyyy-MM-dd HH:mm:ss"
        String format2 = date5.format("yyyy-MM-dd");
        String format3 = date5.format("yyyy年MM月dd日");
        String format4 = date5.format("HH:mm:ss");
        String format5 = date5.format("yyyy年MM月dd日 HH:mm");
        String format6 = date5.format("yyyy年MM月dd日 HH:mm:ss");

        /** 13. 程序运行时间 */
        Instant ins1 = Instant.now();
        Instant ins2 = Instant.now();
        Duration duration2 = Duration.between(ins1, ins2);
        long millis2 = duration2.toMillis();



        System.out.println("完毕");
    }
}
