package cnn.topfox.service;

import cnn.topfox.common.DataDTO;

import java.util.List;

public interface IAdvancedService<DTO extends DataDTO> extends IService<DTO> {

    void beforeSave(List<DTO> list) ;
    void beforeInsertOrUpdate(List<DTO> list);
    void beforeInsertOrUpdate(DTO dto, String dbState);
    void beforeDelete(List<DTO> list);
    void beforeDelete(DTO dto);

    void afterSave(List<DTO> list) ;
    void afterInsertOrUpdate(DTO dto, String dbState);
    void afterInsertOrUpdate(List<DTO> list);
    void afterDelete(List<DTO> list);
    void afterDelete(DTO dto);

}
