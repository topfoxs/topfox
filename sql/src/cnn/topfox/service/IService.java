package cnn.topfox.service;

import cnn.topfox.data.DbState;
import com.alibaba.fastjson2.JSONObject;
import cnn.topfox.common.DataDTO;
import cnn.topfox.common.DataQTO;
import cnn.topfox.common.Response;
import cnn.topfox.sql.Condition;
import cnn.topfox.sql.EntitySelect;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface IService<DTO extends DataDTO> extends ISuperService {
    /**
     * 创建一个新的条件对象Condition
     * com.topfox.sql.Condition
     * @return
     */
    Condition where();

    int insert(DTO dto);

    int insertList(List<DTO> list);

    int update(DTO dto);
    int update(DTO dto, int updateMode);
    int updateList(List<DTO> list);
    int updateList(List<DTO> listUpdate, int updateMode);

    /** 不执行 beforeInsertOrUpdate afterInsertOrUpdate */
    int update2(DTO dto);
    /** 不执行 beforeInsertOrUpdate afterInsertOrUpdate */
    int update2(DTO dto, int updateMode);
    /** 不执行 beforeInsertOrUpdate afterInsertOrUpdate */
    int updateList2(List<DTO> list);


    /**
     * 自定更新条件, 不是根据Id更新时用此方法
     */
    List<DTO> updateBatch(DTO dto, Condition where);
    int updateBatch(DTO dataDTO, DataQTO qto);


    int delete(DTO dto);
    int deleteList(List<DTO> list);

    /**
     * 根据多个Id, 生成一句SQL删除, 无乐观锁
     * 先查询出来, 目的是得到每条记录的id, 然后根据Id删除redis缓存
     * @param ids
     * @return
     */
    int deleteByIds(String... ids);
    int deleteByIds(Number... ids);

    /**
     * 自定义删除条件, 即不是根据主键的删除才用这个
     * 先查询出来, 目的是得到每条记录的id, 然后根据Id删除redis缓存
     * @param where
     * @return
     */
    int deleteBatch(Condition where);

    /**
     * 支持一次提交存在增删改的多行, 依据DTO中dataState()状态来识别增删改操作.
     * @see DbState
     * @param list
     */
    List<DTO> save(List<DTO> list) ;
    DTO save(DTO dto);

    /** 不执行 beforeInsertOrUpdate afterInsertOrUpdate */
    List<DTO> save2(List<DTO> list);

    /** 不执行 beforeInsertOrUpdate afterInsertOrUpdate */
    DTO save2(DTO dto);

    Response<List<DTO>> query(DataQTO qto, String sqlId, String index);
    Response<List<DTO>> list(DataQTO qto);
    Response<List<DTO>> listPage(EntitySelect entitySelect);

    DTO getObject(Number id) ;
    DTO getObject(String id) ;
    DTO getObject(String id, boolean isCache) ;
    DTO getObject(DataQTO qto) ;
    DTO getObject(Condition where) ;

    List<DTO> listObjects(String... ids) ;
    List<DTO> listObjects(Number... ids) ;
    List<DTO> listObjects(Collection idsCollection) ;
    List<DTO> listObjects(Collection idsCollection, boolean isCache) ;
    List<DTO> listObjects(DataQTO qto) ;
    List<DTO> listObjects(Condition where) ;
    List<DTO> listObjects(EntitySelect entitySelect);


    EntitySelect select();
    EntitySelect select(String fields);
    EntitySelect select(String fields, Boolean isAppendAllFields);

    DTO selectOne(DataQTO qto);
    List<DTO> selectList(DataQTO qto);

    List<JSONObject> selectMaps(DataQTO qto) ;
    List<JSONObject> selectMaps(Condition where) ;
    List<JSONObject> selectMaps(EntitySelect entitySelect);

    JSONObject selectMap(EntitySelect entitySelect);


    Response<List<JSONObject>> selectPageMaps(EntitySelect entitySelect);

    int selectCount(Condition where);
    <T> T selectMax(String fieldName, Condition where);

}