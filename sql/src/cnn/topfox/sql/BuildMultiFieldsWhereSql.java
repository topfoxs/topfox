package cnn.topfox.sql;

import cnn.topfox.common.CommonException;
import cnn.topfox.common.ResponseCode;
import cnn.topfox.data.DataType;
import cnn.topfox.data.Field;
import cnn.topfox.misc.BeanUtil;
import cnn.topfox.misc.Misc;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

/**
 * 组合的多个字段 条件生成.
 * 如:
 *  1.多主键
 *  2.多字段重复检查
 */
@Slf4j
public class BuildMultiFieldsWhereSql {

    public static void buildSQL(LinkedHashMap<String, Field> mapFields,
                                Condition where,
                                Set idValueGroups,
                                String splitMark,
                                String groupOperator,
                                String fieldOperator
                                )
    {
        if (mapFields.size() == 0) {
            return;
        }
        if (mapFields.size() == 1) {
            //一个字段
            for (Map.Entry<String, Field> entry : mapFields.entrySet()) {
                if (fieldOperator.contains("=")) {
                    where.eq(entry.getKey(), idValueGroups.toArray());
                }else{
                    where.ne(entry.getKey(), idValueGroups.toArray());
                }
            }
            return;
        }
        if (idValueGroups.size() == 0) {
            return;
        }


        /**
         * 以下为 多个主键字段的逻辑
         */
        StringBuilder whereKeysSql = new StringBuilder();
        whereKeysSql.append("(");
        int indexGroup=0;
        for (Object objIdValues : idValueGroups) {
            List<String> listValues = Misc.string2List(objIdValues.toString(), splitMark);
            if (listValues.size() != mapFields.size()) {
                throw CommonException.newInstance2(ResponseCode.SYS_ARRAY_INDEX_OUT);
            }

            if (idValueGroups.size()>1){whereKeysSql.append("(");}
            int indexField = 0;
            for (Map.Entry<String, Field> entry : mapFields.entrySet()) {
                Field field = entry.getValue();
                Object value = listValues.get(indexField);
                if (DataType.isNumber(field.getDataType()) &&
                        (value==null || "null".equals(value))
                ) {
                    //解决插入 还有拼id的字段 报错的问题
                    value=0;
                }
                whereKeysSql.append(field.getDbName()).append(fieldOperator);
                BeanUtil.getSqlValue(field, field.getDbName(), value, whereKeysSql);
                if (indexField < mapFields.size()-1){
                    whereKeysSql.append(" AND ");
                }
                indexField++;
            }
            if (idValueGroups.size()>1){whereKeysSql.append(")");}
            if (indexGroup < idValueGroups.size()-1){
                if (Misc.isDebugEnabled()){
                    whereKeysSql.append("\n").append(groupOperator);
                }else{
                    whereKeysSql.append(groupOperator);
                }
            }
            indexGroup++;
        }
        whereKeysSql.append(")");
        where.add(whereKeysSql.toString());
    }
}
