package cnn.topfox.sql;

public interface IEnum {
    String getCode();

    String getValue();
}