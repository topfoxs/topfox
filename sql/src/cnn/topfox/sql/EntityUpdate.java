package cnn.topfox.sql;

import cnn.topfox.common.DataDTO;
import cnn.topfox.common.Incremental;
import cnn.topfox.data.DataHelper;
import cnn.topfox.data.Field;
import cnn.topfox.data.TableInfo;
import cnn.topfox.misc.BeanUtil;
import cnn.topfox.misc.CamelHelper;
import cnn.topfox.misc.Misc;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
public class EntityUpdate extends IEntitySql{
    private StringBuilder saveSql;
    private StringBuilder updateSetValues;

    private EntityUpdate(){
        saveSql = new StringBuilder();
        updateSetValues = new StringBuilder();
    }

    public static EntityUpdate create(){
        return new EntityUpdate();
    }
    public static EntityUpdate create(Class<?> clazz){
        EntityUpdate current = create();
        current.setEntityClazz(clazz);
        return current;
    }
    public static EntityUpdate create(TableInfo tableInfo){
        EntityUpdate current = create();
        current.setTableInfo(tableInfo);
        return current;
    }

    /**
     * 返回条件对象
     * @return
     */
    @Override
    public Condition<EntityUpdate> where(){
        return  super.where();
    }

    @Override
    public String getSql(){
        saveSql.setLength(0);
        saveSql.append("UPDATE ")
                .append(getTableInfo().getTableName())
                .append("\nSET ")
                .append(updateSetValues);
        saveSql.append(where().getWhereSql());

        String sql=saveSql.toString();
//        init();//对象数据归位,初始化为空
        return sql;
    }

    public EntityUpdate updateBatch(DataDTO bean) {
        return updateBatch(bean,false);
    }
    public EntityUpdate updateBatch(DataDTO bean, boolean isNullValue2Sql) {
        clean();
        Map<String,Object> mapData = BeanUtil.bean2Map(bean,isNullValue2Sql);

        //指定强制更新为 null的字段处理
        if (bean.nullFields() != null) {
            bean.nullFields().forEach(key -> mapData.put(key, null));
        }

        buildUpdateSetValues(bean.mapSave(mapData));
        return this;
    }

    public String getUpdateByIdSql(DataDTO bean) {
        return getUpdateByIdSqlBuild(bean.mapSave(BeanUtil.bean2Map(bean,false)));
    }

    public String getUpdateByIdSql(DataDTO bean, int updateMode) {
        return getUpdateByIdSqlBuild(bean.mapSave(BeanUtil.getChangeRowData(bean, updateMode)));
    }

    public String getUpdateByIdSql(DataDTO bean, boolean isNullValue2SetSql) {
        return getUpdateByIdSqlBuild(bean.mapSave(BeanUtil.bean2Map(bean,isNullValue2SetSql)));
    }
    private StringBuilder buildUpdateSetValues(Map<String,Object> mapValues){
        updateSetValues.setLength(0);

//        //过滤出修改的字段,顺序按照fields.  mapValues的keyset是无序的,以field为准
//        Object[] fieldsArray =fields.keySet().stream().filter((fieldName) ->
//                (mapValues.containsKey(fieldName) && !fieldName.equals(getTableInfo().getIdFieldName()))//过滤条件
//        ).toArray();

        //处理版本号字段, 只要存在, 则递增 //old if (mapValues.containsKey(getTableInfo().getVersionFieldName())){
        if (getTableInfo().getVersionFieldName() !=null ){
            String versionColumn=getTableInfo().getColumn(getTableInfo().getVersionFieldName());
            updateSetValues.append(versionColumn).append("=").append(versionColumn).append("+1,");
        }

        Map<String, Field> fields = getTableInfo().getFields();
        mapValues.forEach((fieldName, value)->{
            if (getTableInfo().getIdField().getName().equals(fieldName)
                    || fieldName.equals(getTableInfo().getVersionFieldName())
            ){
                return;
            }
            Field field = fields.get(fieldName.trim());
            if (field == null || field.exist()==false) {
                return;
            }

            if (fieldName.contains("_")) {
                //解决dto.addNullFields指定为null的字段时, 驼峰和下划线都支持
                field = fields.get(CamelHelper.toCamel(fieldName));
                if (field==null){
                    log.warn("{}中找不到字段{}({})",this.getEntityClazz().getName(),CamelHelper.toCamel(fieldName),fieldName);
                    //return;
                }
            }

//            String dbFieldName=field!=null?field.getDbName():TableInfo.getColumn(getSysConfig().getSqlCamelToUnderscore(), fieldName);

            String dbFieldName=field.getDbName();
            if ( field.getIncremental() == Incremental.ADDITION || field.getIncremental() == Incremental.SUBTRACT ){
                if ( DataHelper.parseLong(value) == 0L ) {return;}
                // 解决更新SQL +-  addition +  / subtract -
                updateSetValues.append(dbFieldName).append("=").append(field.getDbName());
                updateSetValues.append(field.getIncremental().getCode()); // + - 符号
                BeanUtil.getSqlValue(field, dbFieldName, value, updateSetValues);
                updateSetValues.append(",");
            }else {
                updateSetValues.append(dbFieldName).append("=");
                BeanUtil.getSqlValue(field, dbFieldName, value, updateSetValues);
                updateSetValues.append(",");
            }
        });

        if (updateSetValues.length() == 0){
            String dbName=getTableInfo().getIdField().getDbName();
            updateSetValues.append(dbName).append("=").append(dbName).append(",");
        }

        //去掉最后的逗号
        updateSetValues.setLength(updateSetValues.length()-1);
        return updateSetValues;
    }

    public String getUpdateByIdSqlBuild(Map<String,Object> mapValues){
        saveSql.setLength(0);
        saveSql.append("UPDATE ")
                .append(getTableInfo().getTableName())
                .append("\n\rSET ")
                .append(buildUpdateSetValues(mapValues));
        where().clean();
        Map<String, Field> fields = getTableInfo().getFields();

        Field idField= getTableInfo().getIdField();
        where().eq(idField.getDbName(), mapValues.get(idField.getName()));
        
        //版本号 字段值 为null 就不生成 version的条件
        String versionFieldName=getTableInfo().getVersionFieldName();
        Object versionValue=mapValues.get(getTableInfo().getVersionFieldName());
        if (versionFieldName!=null && versionValue!=null ){
            where().and(false).eq(getTableInfo().getColumn(versionFieldName), versionValue);
        }
        saveSql.append(where().getWhereSql());
        String sql=saveSql.toString();
        clean();
        return sql;
    }

    @Override
    protected void clean(){
        saveSql.setLength(0);
        updateSetValues.setLength(0);
        where().clean();//清空上次的查询条件
    }
}
