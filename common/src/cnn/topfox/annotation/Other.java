package cnn.topfox.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)

/**
 * 非当前表的字段用这个注解  自动生成SQL语句 排除 此注解的字段
 */
public @interface Other {
}