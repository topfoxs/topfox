package cnn.topfox.annotation;

import java.lang.annotation.*;

@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EntityParam {

	String value() default "";

	boolean required() default true;

}
