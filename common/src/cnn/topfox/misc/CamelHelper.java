package cnn.topfox.misc;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import cnn.topfox.common.TopException;
import cnn.topfox.common.ResponseCode;
import cnn.topfox.common.SysConfigRead;
import cnn.topfox.data.SqlUnderscore;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

/**
 * 驼峰命名处理工具包
 */
@Slf4j
public class CamelHelper {

    /**
     * 测试
     * @param args
     */
    public  static void main(String[] args) {
        log.debug(toCamel("name"));
        log.debug(toCamel("my_name"));
        log.debug(toCamel("my_order_name"));

        log.debug("--------------------------------");
        log.debug(toUnderlineName("name"));
        log.debug(toUnderlineName("myName"));
        log.debug(toUnderlineName("myOrderName"));
        log.debug("--------------------------------");

    }

    /**
     * 下划线的 key 转 驼峰命名
     * @param source
     * @return
     */
    public static String toCamel(String source) {
        if (source == null) {
            return null;
        }

        //没有下划线, 说明已经是驼峰命名了, 直接返回
        if(source.contains("_") == false ){ return source; }


        StringBuffer result = new StringBuffer();
        String a[] = source.split("_");
        for (String s : a) {
            if (result.length() == 0) {
                result.append(s.toLowerCase());
            } else {
                result.append(s.substring(0, 1).toUpperCase());
                result.append(s.substring(1).toLowerCase());
            }
        }
        return result.toString();
    }

    /**
     * 驼峰命名--> 带下划线
     * 返回的英文全部大写
     */
    static SysConfigRead sysConfig;
    private static final char SEPARATOR = '_';
    public static String toUnderlineName(String source) {
        if (source == null) { return null; }

        //已经是下划线的命名了, 就直接返回
        if(source.indexOf("_")>0){ return source; }

        StringBuilder sb = new StringBuilder();
        boolean upperCase = false;
        for (int i = 0; i < source.length(); i++) {
            char c = source.charAt(i);

            boolean nextUpperCase = true;

            if (i < (source.length() - 1)) {
                nextUpperCase = Character.isUpperCase(source.charAt(i + 1));
            }

            if ((i >= 0) && Character.isUpperCase(c)) {
                if (!upperCase || !nextUpperCase) {
                    if (i > 0) {
                        sb.append(SEPARATOR);
                    }
                }
                upperCase = true;
            } else {
                upperCase = false;
            }

            sb.append(Character.toUpperCase(c));
        }


        return sb.toString();
    }

    public static String toUnderlineName(String source, SqlUnderscore sqlUnderscore) {
        if (sqlUnderscore == SqlUnderscore.ON_UPPER){
            //开启 字段名依据驼峰命名转下划线, 并全部 大写
            return toUnderlineName(source).toUpperCase();
        }else if (sqlUnderscore == SqlUnderscore.ON_LOWER){
            //开启 字段名依据驼峰命名转下划线, 并全部 小写
            return toUnderlineName(source).toLowerCase();
        }else{
            return source;
        }

    }

//    /**
//     * 驼峰命名--> 带下划线
//     * 返回的英文 全部大写 还是全部小写, 根据参数 top.service.sql-camel-to-underscore 来确定
//     */
//    public static String toUnderlineName2(String s) {
//        if (s == null) { return null; }
//        //根据配置 判断 是否全部转大写/小写
//        if (sysConfig == null){
//            sysConfig = AppContext.getSysConfig();
//        }
//
//        if (sysConfig.getSqlCamelToUnderscore() == SqlUnderscore.OFF){
//            //配置参数关闭, 不处理直接返回
//            return s;
//        }
//
//        //这里默认是全部转大写的
//        String fieldName = toUnderlineName(s);
//
//        if (!fieldName.contains("_") && sysConfig.getSqlCamelToUnderscore() == SqlUnderscore.ON_UPPER){
//            //开启 字段名依据驼峰命名转下划线, 并全部 大写
//            return fieldName;
//        }else if (!fieldName.contains("_") && sysConfig.getSqlCamelToUnderscore() == SqlUnderscore.ON_LOWER){
//            //开启 字段名依据驼峰命名转下划线, 并全部 小写
//            return fieldName.toLowerCase();
//        }
//
//        return fieldName;
//    }


    /**
     * map 的key是下划线的, 转成驼峰命名的
     * @param row
     * @return
     */
    public static JSONObject mapToCamel(JSONObject row) {
        JSONObject rowNew = new JSONObject();
        row.forEach((key, value) -> {
            if (key.contains("_")) {
                rowNew.put(toCamel(key), value);//包含"_"的key 处理为驼峰命名
            } else {
                rowNew.put(key, value);
            }

//            if (value instanceof DataDTO){
//
//            }
        });
        return rowNew;
    }

    public static Map mapToCamel(Map<String, Object> row) {
        //将要处理的 带 下划线的 keys 和值放入到 mapTempData 容器中
        Map<String, Object> mapTempData = new HashMap<>();
        for(Map.Entry<String, Object> entry : row.entrySet()){
            String key = entry.getKey();
            if (key.contains("_")) {
                mapTempData.put(entry.getKey(),entry.getValue());
            }
        }


        for(Map.Entry<String, Object> entry : mapTempData.entrySet()){
            String key = entry.getKey();
            row.put(toCamel(key), entry.getValue()); //包含"_"的key 处理为驼峰命名
            row.remove(key);
        }

        return row;
    }

    /**
     * json的key是下划线的, 转成驼峰命名
     * @param jsonString
     * @return
     */
    public static String jsonDataToCamel(String jsonString) {
        String firstString = jsonString.substring(0,1);
        if (firstString.equals(" ")){
            jsonString = jsonString.trim();
            firstString = jsonString.substring(0,1);
        }
        //下划线的keys转为驼峰命名
        if (jsonString != null) {
            try {
                if (firstString.equals("[")) {
                    //传入的是 数组格式[{},{},{}...]
                    JSONArray jsonArrayNew=new JSONArray();
                    JSONArray jsonArray = JSON.parseArray(jsonString);//服务调用服务的格式
                    if (jsonArray == null) return jsonString;
                    for(int i=0;i<jsonArray.size();i++){
                        JSONObject row = jsonArray.getJSONObject(i);  // 遍历 jsonarray 数组，把每一个对象转成 json 对象
                        jsonArrayNew.add(mapToCamel(row));
                    }
                    return jsonArrayNew.toJSONString();
                }else if (firstString.equals("{")) {
                    //传入的是 对象格式 {}
                    JSONObject row = JSON.parseObject(jsonString);//服务调用服务的格式
                    if (row == null) return jsonString;
                    return mapToCamel(row).toJSONString();
                }
            } catch (Exception e) {
                throw TopException.newInstance2(ResponseCode.STRING_TO_JSONMAP);
            }
        }

        return jsonString;
    }
}