package cnn.topfox.misc;

import cnn.topfox.common.CommonException;
import cnn.topfox.common.ResponseCode;
import cnn.topfox.data.DataHelper;
import cnn.topfox.data.DateTime;
import lombok.extern.slf4j.Slf4j;
//import java.sql.Timestamp;
//import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Slf4j
public class DateUtils {
    public static final String
            DATE_FORMAT               ="yyyy-MM-dd",
            DATETIME_FORMAT           ="yyyy-MM-dd HH:mm",
            DATETIME_SECOND_FORMAT    ="yyyy-MM-dd HH:mm:ss",
            DATETIME_MILLSECOND_FORMAT="yyyy-MM-dd HH:mm:ss SSS",
            TIME_FORMAT           ="HH:mm",
            TIME_SECOND_FORMAT    ="HH:mm:ss",
            TIME_MILLSECOND_FORMAT="HH:mm:ss SSS",
            YEARMONTH_FORMAT              ="yyyy-MM",
            DATE_FORMAT_CN                ="yyyy年MM月dd日",
            DATETIME_FORMAT_CN            ="yyyy年MM月dd日 HH:mm",
            DATETIME_SECOND_FORMAT_CN     ="yyyy年MM月dd日 HH:mm:ss",
            DATETIME_MILLSECOND_FORMAT_CN ="yyyy年MM月dd日 HH:mm:ss SSS",
            DATETIME_JAVASCTRIPT_FORMAT_CN="yyyy-MM-dd'T'HH:mm:ss'Z'",
            DATETIME_RFC3339="yyyy-MM-dd'T'HH:mm:ssXXX",
            DATE_FORMAT1    ="yyyy/MM/dd"
    ;

    // 用于存放不同模板的日期
    private static final ThreadLocal<Map<String, SimpleDateFormat>> LOCAL = new ThreadLocal<Map<String, SimpleDateFormat>>() {
        @Override
        protected Map<String, SimpleDateFormat> initialValue() {
            return new HashMap<>();
        }
    };

    /**
     * 返回一个SimpleDateFormat,每个线程只会new一次pattern对应的sdf
     *
     * @param dateFormat
     * @return
     */
    public static SimpleDateFormat getDateFormat(String dateFormat) {
        Map<String, SimpleDateFormat> map = LOCAL.get();
        SimpleDateFormat sdf = map.get(dateFormat);
        if (sdf == null) {
            sdf = new SimpleDateFormat(dateFormat);
            map.put(dateFormat, sdf);
        }
        return sdf;
    }

//    /**
//     * 获取昨天的日期
//     *
//     * @return
//     */
//    public static Date getYesterDay() {
//        Calendar c = Calendar.getInstance();
//        c.setTime(new Date());
//        c.add(Calendar.DAY_OF_MONTH, -1);
//        return c.getTime();
//    }

//    /**
//     * 获取从当前日期指定分钟以前的日期
//     *
//     * @return
//     */
//    public static Date getDateBeforeMinute(int beforeMinute) {
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(new Date());
//        int minute = calendar.get(Calendar.MINUTE);
//        calendar.set(Calendar.MINUTE, minute - beforeMinute);
//        return calendar.getTime();
//    }

//    /**
//     * 获取两个日期之间的天数
//     *
//     * @param before
//     * @param after
//     * @return
//     */
//    public static long getDistanceTwoDate(Date before, Date after) {
//        long beforeTime = before.getTime();
//        long afterTime = after.getTime();
//        return (afterTime - beforeTime) / (1000 * 60 * 60 * 24);
//    }


//    /**
//     * 获取某个date的年份
//     * @param date
//     * @return
//     */
//    public static int getYear(Date date){
//        Calendar c=Calendar.getInstance();
//        c.setTime(date);
//        return c.get(Calendar.YEAR);
//    }
//    /**
//     * 获取某个date的月份
//     * @param date
//     * @return
//     */
//    public static int getMonth(Date date){
//        Calendar c=Calendar.getInstance();
//        c.setTime(date);
//        return c.get(Calendar.MONTH)+1;
//    }
//    /**
//     * 获取某个date的day
//     * @param date
//     * @return
//     */
//    public static int getDayOfMonth(Date date){
//        Calendar c=Calendar.getInstance();
//        c.setTime(date);
//        return c.get(Calendar.DAY_OF_MONTH);
//    }
//    /**
//     * 判断字符串是否是指定的格式
//     * @param date
//     * @param dateFormat
//     * @return
//     */
//    public static boolean isDate(String date, String dateFormat) {
//        SimpleDateFormat sdf = getDateFormat(dateFormat);
//        try {
//            sdf.setLenient(false);
//            sdf.parse(date);
//            return true;
//        } catch (Exception e) {
//            return false;
//        }
//    }
//    /**
//     * 获取两个时间的时间差
//     * @param before
//     * @param after
//     * @param flag 0秒，1分，2时，3天
//     * @return
//     */
//    public static int getDifferentNum(Date before,Date after,int flag){
//        if(before==null||after==null){
//            return 0;
//        }
//        long timeInterval=after.getTime()-before.getTime();
//        switch (flag) {
//            case 0://秒
//                return (int) (timeInterval/1000);
//            case 1://分
//                return (int) (timeInterval/(60*1000));
//            case 2://时
//                return (int) (timeInterval/(60*60*1000));
//            case 3://天
//                return (int) (timeInterval/(60*60*1000*24));
//        }
//        return 0;
//    }


//    /**获得毫秒*/
//    public static String getMilliSecond() {
//        Calendar calendar = Calendar.getInstance();
//        DecimalFormat df = new DecimalFormat("00");
//        return new DecimalFormat("000").format(calendar.get(Calendar.MILLISECOND));
//    }

//    public static String long2Time(long millSec){
//        Date date= new Date(millSec);
//        return getDateFormat(TIME_SECOND_FORMAT).format(date);
//    }
//    public static String long2DateTime(long millSec){
//        Date date= new Date(millSec);
//        return getDateFormat(DATETIME_SECOND_FORMAT).format(date);
//    }
    //
//    public static Date long2Date(long millSec){
//        Date date=stringToDate(long2DateTime(millSec));
//        return date;
//    }

//    public static int currentYear() {
//        //return currentDate().getYear() + 1900;
//        Calendar calendar = Calendar.getInstance();
//        return calendar.get(Calendar.YEAR);
//
//    }


    public static LocalDate toLocalDate(java.util.Date date) {
        if (date==null) return null;

        if (date instanceof java.sql.Date){
            date= new Date(date.getTime()); //java.sql.Date转为 java.util.Date
        }
        Instant instant = date.toInstant();
        LocalDate localDate = LocalDate.ofInstant(instant, ZoneId.systemDefault());
        return localDate;
    }
    public static LocalDateTime toLocalDateTime(java.util.Date date) {
        if (date==null) return null;
        if (date instanceof java.sql.Date){
            date= new Date(date.getTime()); //java.sql.Date转为 java.util.Date
        }

        Instant instant = date.toInstant();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        return localDateTime;
    }
    public static Date toDate(LocalDate localDate) {
        if (localDate==null) return null;

        Instant instant = localDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
        return Date.from(instant);
    }
    public static Date toDate(LocalDateTime localDateTime) {
        if (localDateTime==null) return null;

        // 转换为ZonedDateTime
        ZonedDateTime zonedDateTime = localDateTime.atZone(ZoneId.systemDefault());
        return Date.from(zonedDateTime.toInstant());
    }
    /**
     * string转化为指定模板的date
     *
     * @param dateStr 日期字符串
     * @param dateFormat 日期格式
     * @return
     */
    public static Date toDate(String dateStr, String dateFormat) {
        if (Misc.isNull(dateStr)) return null;

        try {
            SimpleDateFormat sdf = getDateFormat(dateFormat);
            return sdf.parse(dateStr);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *
     * @param dateStr  格式: yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static Date toDateTime(String dateStr) {
        return DateUtils.toDate(dateStr, DATETIME_SECOND_FORMAT);
    }

//    public static Date toDate(String dateStr){
//        dateStr=dateStr.replace("年", "-");
//        dateStr=dateStr.replace("月", "-");
//        dateStr=dateStr.replace("日", "-");
//        return stringToDate(dateStr.replace("/", "-"));
//    }
//    public static java.sql.Date getSqlDate(String dateStr){
//        dateStr=dateStr.replace("年", "-");
//        dateStr=dateStr.replace("月", "-");
//        dateStr=dateStr.replace("日", "-");
//        return java.sql.Date.valueOf(dateStr.replace("/", "-"));
//    }

    /**
     * 获得 Date 的当前时间
     *
     * @return  Date
     */
    public static Date getCurrent() {
        return new Date(System.currentTimeMillis());
    }

    /**
     * yyyy-MM-dd
     * @return
     */
    public static String getDate(){
        return getDate2String(DATE_FORMAT);
    }
    /**
     * yyyy-MM-dd HH:mm
     * @return
     */
    public static String getDateByHHmm(){
        return getDate2String(DATETIME_FORMAT);
    }
    /**
     * yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String getDateByHHmmss(){
        return getDate2String(DATETIME_SECOND_FORMAT);
    }
    /**
     * yyyy-MM-dd HH:mm:ss SSS
     * @return
     */
    public static String getDateByHHmmssSSS(){
        return getDate2String(DATETIME_MILLSECOND_FORMAT);
    }
    public static String getDate2String(String format){
        SimpleDateFormat fmt = getDateFormat(format);
        Calendar calendar = Calendar.getInstance();
        Date date=calendar.getTime();
        String ts=fmt.format(date);
        return ts;
    }
//    public static java.sql.Date getSqlDate() {
//        return new java.sql.Date(System.currentTimeMillis());
//    }
//
//    public static Timestamp getTimestamp() {
//        return new Timestamp(System.currentTimeMillis());
//    }

    public static String getTimeByHHmmssSSS(){
        return getDate2String(TIME_MILLSECOND_FORMAT);
    }
    public static String getTimeByHHmm(){
        return getDate2String(TIME_FORMAT);
    }
    public static String getTimeByHHmmss(){
        return getDate2String(TIME_SECOND_FORMAT);
    }


    public static String toDateStr(LocalDate date, String dateFormat){
        if (date==null) {return "";}
        if (Misc.isNull(dateFormat)) {
            return toDateStr(date);
        }
        return DateTimeFormatter.ofPattern(dateFormat).format(date);
    }

    public static String toDateStr(LocalDate date) {
        if (date==null) {return "";}
        return date.toString();
    }

    public static String toDateStr(DateTime datetime, String dateFormat){
        if (datetime==null) {return "";}
        if (Misc.isNull(dateFormat)) {
            return toDateStr(datetime);
        }
        return datetime.format(dateFormat);
    }

    /**
     * 默认数据格式: yyyy-MM-dd HH:mm:ss
     * @param datetime
     * @return
     */
    public static String toDateStr(DateTime datetime) {
        if (datetime==null) {return "";}
        return datetime.toString();
    }

    /**
     * date类型转化为指定模板的string
     * @param date
     * @param dateFormat
     * @return
     */
    public static String toDateStr(Date date,String dateFormat){
        if (date==null) {return "";}
        if (Misc.isNull(dateFormat)) {
            return toDateStr(date);
        }
        SimpleDateFormat sdf = getDateFormat(dateFormat);
        return sdf.format(date);
    }
    public static String toDateStr(Date date) {
        if (date==null) {return "";}
        return getDateFormat(DATE_FORMAT).format(date);
    }

    public static String toDateCnStr(Date date) {
        if (date==null) {return "";}
        return getDateFormat(DATE_FORMAT_CN).format(date);
    }
    public static String toDateTimeCnStr(Date date) {
        if (date==null) {return "";}
        return getDateFormat(DATETIME_FORMAT_CN).format(date);
    }

    public static String toDateTimeStr(Date date) {
        if (date==null) {return "";}
        return getDateFormat(DATETIME_FORMAT).format(date);
    }

    public static String toDateTimeSecond(Date date) {
        if (date==null) {return "";}
        return getDateFormat(DATETIME_SECOND_FORMAT).format(date);
    }

    public static String toDateTimeMillis(Date date) {
        if (date==null) {return "";}
        return getDateFormat(DATETIME_MILLSECOND_FORMAT).format(date);
    }
    public static Date getDefaultDate(){
        return stringToDate("2000-01-01");
    }


    /**
     * 计算两个日期之间相隔的天数-不考虑 小时分钟
     * @param startDate 开始日期
     * @param endDate   结束日期
     * @return 相差天数
     * @throws ParseException
     */
    public static int daysBetween(Date startDate, Date endDate) {
        try {
            SimpleDateFormat dateFormat = getDateFormat(DATE_FORMAT);
            startDate = dateFormat.parse(dateFormat.format(startDate));
            endDate = dateFormat.parse(dateFormat.format(endDate));
        }catch (Exception e){
            throw CommonException.newInstance2(ResponseCode.DATA_IS_INVALID);
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        long time1 = cal.getTimeInMillis();
        cal.setTime(endDate);
        long time2 = cal.getTimeInMillis();
        long between_days=(time2-time1)/(1000*3600*24);

        return Integer.parseInt(String.valueOf(between_days));
    }

    /**
     * 计算两个日期之间相差的天数-不考虑 小时分钟
     * @param startDate 开始日期
     * @param endDate   结束日期
     * @return 相差天数 = startDate > endDate 正数 ; 相等为0
     */
    public static int daysBetween(String startDate, String endDate) {
        try {
            SimpleDateFormat dateFormat = getDateFormat(DATE_FORMAT);

            Calendar cal = Calendar.getInstance();
            cal.setTime(dateFormat.parse(startDate));
            long time1 = cal.getTimeInMillis();
            cal.setTime(dateFormat.parse(endDate));
            long time2 = cal.getTimeInMillis();
            long between_days = (time2 - time1) / (1000 * 3600 * 24);

            return Integer.parseInt(String.valueOf(between_days));
        }catch (Exception e){
            throw CommonException.newInstance2(ResponseCode.DATA_IS_INVALID);
        }
    }

    public static Duration between(LocalDateTime startDate, LocalDateTime endDate) {
        return Duration.between(startDate, endDate);
    }
    public static Period between(LocalDate startDate, LocalDate endDate) {
        return Period.between(startDate, endDate);
    }
    public static Duration between(DateTime startDate, DateTime endDate) {
        return Duration.between(startDate.toLocalDateTime(), endDate.toLocalDateTime());
    }
//    public static int betweenDays(LocalDateTime startDate, LocalDateTime endDate) {
//        Duration duration = Duration.between(startDate, endDate);
//        return ((Long)duration.toDays()).intValue();
//    }
    public static int betweenDays(LocalDate startDate, LocalDate endDate) {
        Long days=ChronoUnit.DAYS.between(startDate, endDate);
        return days.intValue();
    }
    public static int betweenDays(DateTime startDate, DateTime endDate) {
        return betweenDays(startDate.toLocalDate(), endDate.toLocalDate());
    }

//    /**
//     * 计算相差天数（开始和结束日期）（相差和相隔的区别，1日 11:59，和2日 00:01，相差0天，相隔1天 ）
//     * 计算两个日期之间相差的天数   考虑小时分钟
//     * @param startDateStr 开始日期
//     * @param endDateStr   结束日期
//     * @return 相差天数 = endDate>beginDate为 正数 ; 相等为0
//     */
//    public static int getDifferDays(String startDateStr, String endDateStr){
//        //格式化日期，yyyy-MM-dd 是计算相隔天数，yyyy-MM-dd HH:mm:ss 是计算相差天数
//        try {
//            java.text.SimpleDateFormat sf = getDateFormat(DATETIME_SECOND_FORMAT);
//            Date beginDate = sf.parse(startDateStr);
//            Date endDate   = sf.parse(endDateStr);
//            return getDifferDays(beginDate, endDate);
//        } catch (ParseException e) {
//            throw new RuntimeException("计算相差天数出错！",e);
//        }
//
//    }

//    /**
//     *
//     * @param startDate
//     * @param endDate
//     * @return  相差天数 = endDate>beginDate为 正数 ; 相等为0
//     */
//    public static int getDifferDays(Date startDate, Date endDate){
//        //格式化日期，yyyy-MM-dd 是计算相隔天数，yyyy-MM-dd HH:mm:ss 是计算相差天数
//        Long betweenDate = (endDate.getTime()-startDate.getTime())/(24 * 60 * 60 * 1000);
//        return betweenDate.intValue();
//    }
//    public static int getDifferDays(String endDateStr){
//        java.text.SimpleDateFormat sf = getDateFormat(DATETIME_SECOND_FORMAT);
//        String beginDateStr = toDateStr(new Date(System.currentTimeMillis()));
//        return getDifferDays(beginDateStr,endDateStr);
//    }


//    //计算相隔天数（开始和结束日期）（相差和相隔的区别，1月1日 11:59，和1月2日的00:01，相差0天(没有满24小时)，相隔1天 ）
//    public static int getSeparationDays(Date startDate,Date endDate){
//        //格式化日期，yyyy-MM-dd 是计算相隔天数，yyyy-MM-dd HH:mm:ss 是计算相差天数
//        Long betweenDate = (endDate.getTime()-startDate.getTime())/(24 * 60 * 60 * 1000);
//        return betweenDate.intValue();
//    }
//    //计算相隔天数（开始和结束日期）（相差和相隔的区别，1月1日 11:59，和1月2日的00:01，相差0天(没有满24小时)，相隔1天 ）
//    public static int getSeparationDays(String startDateStr,String endDateStr){
//        //格式化日期，yyyy-MM-dd 是计算相隔天数，yyyy-MM-dd HH:mm:ss 是计算相差天数
//        java.text.SimpleDateFormat sf = getDateFormat(DATE_FORMAT);
//        Long betweenDate = 0l;
//        try {
//            Date beginDate = sf.parse(startDateStr);
//            Date endDate = sf.parse(endDateStr);
//            return getSeparationDays(beginDate, endDate);
//        } catch (ParseException e) {
//            throw new RuntimeException("计算相隔天数出错！",e);
//        }
//    }
//    public static int getSeparationDays(String endDateStr){
//        String beginDateStr = toDateStr(new Date(System.currentTimeMillis()));
//        return getSeparationDays(beginDateStr, endDateStr);
//    }

//    public static String getChangeDateStr(Date date, String type, int value) {
//        return toDateStr(getChangeDate(date, type, value), DATE_FORMAT);
//    }
//    public static String getChangeDatetimeStr(Date date, String type, int value) {
//        return toDateStr(getChangeDate(date, type, value), DATETIME_SECOND_FORMAT);
//    }
//    public static Date getChangeDate(LocalDate localDate, String type, int value) {
//        return getChangeDate(DateUtils.toDate(localDate), type, value);
//    }
    public static Date getChangeDate(Date date, String type, int value) {
        Calendar calendar = Calendar.getInstance();
        if (date == null) {
            date = calendar.getTime();
        }else{
            calendar.setTime(date);
        }

        if(value==0){
            return date;
        }

        if("year".equals(type)) {
            calendar.add(Calendar.YEAR, value);
        }else if("month".equals(type)){
            calendar.add(Calendar.MONTH, value);
        }else if("day".equals(type)){
            calendar.add(Calendar.DAY_OF_MONTH, value);
        }else if("hour".equals(type)){
            calendar.add(Calendar.HOUR, value);
        }else if("minute".equals(type)){
            calendar.add(Calendar.MINUTE, value);
        }else if("second".equals(type)){
            calendar.add(Calendar.SECOND, value);
        }

        return calendar.getTime();
    }

//    private static Calendar getChangeDate(Integer day, Integer hour, Integer minute, Integer second, Integer millisecond){
//        return getChangeDate(Calendar.getInstance(), day, hour, minute, second, millisecond);
//    }

//    private static Calendar getChangeDate(Calendar calendar, Integer day, Integer hour, Integer minute, Integer second, Integer millisecond){
//        if(calendar==null){
//            calendar = Calendar.getInstance();
//        }
//        if(day!=null){
//            calendar.add(Calendar.DAY_OF_YEAR, day);
//        }
//        if(hour!=null){
//            if(hour==0){
//                calendar.set(Calendar.HOUR_OF_DAY, hour);
//            }else{
//                calendar.add(Calendar.HOUR_OF_DAY, hour);
//            }
//        }
//        if(minute!=null){
//            calendar.set(Calendar.MINUTE, minute);
//        }
//        if(second!=null){
//            if(second==0){
//                calendar.set(Calendar.SECOND, second);
//            }else{
//                calendar.add(Calendar.SECOND, second);
//            }
//        }
//        if(millisecond!=null){
//            calendar.set(Calendar.MILLISECOND, millisecond);
//        }
//        return calendar;
//    }

    public static Date stringToDate(String dateStr) {
        Calendar calendar = Calendar.getInstance();
        if(dateStr.length()<10){
            String[] values1=dateStr.split("-");
            String[] values2=new String[]{"00","00","00"};
            if(values1.length!=3){
                return null;
            } else {
                calendar.set(Integer.parseInt(values1[0]), Integer.parseInt(values1[1])-1, Integer.parseInt(values1[2]), Integer.parseInt(values2[0]), Integer.parseInt(values2[1]), Integer.parseInt(values2[2]));
            }
        }else if(dateStr.length()==10){
            String[] values1=dateStr.split(" ")[0].split("\\-");
            String[] values2=new String[]{"00","00","00"};
            calendar.set(Integer.parseInt(values1[0]), Integer.parseInt(values1[1])-1, Integer.parseInt(values1[2]), Integer.parseInt(values2[0]), Integer.parseInt(values2[1]), Integer.parseInt(values2[2]));
        }else if(dateStr.length()==16){
            String[] values1=dateStr.split(" ")[0].split("\\-");
            String[] values2=dateStr.split(" ")[1].split(":");
            calendar.set(Integer.parseInt(values1[0]), Integer.parseInt(values1[1])-1, Integer.parseInt(values1[2]), Integer.parseInt(values2[0]), Integer.parseInt(values2[1]));
        }else if(dateStr.length()>=19){
            String[] values1=dateStr.split(" ")[0].split("\\-");
            String[] values2=dateStr.split(" ")[1].split(":");
            calendar.set(Integer.parseInt(values1[0]), Integer.parseInt(values1[1])-1, Integer.parseInt(values1[2]), Integer.parseInt(values2[0]), Integer.parseInt(values2[1]), Integer.parseInt(values2[2]));
        }else {
            return null;
        }
        return calendar.getTime();
    }

    /**
     * 计算2个日期之间相差的 相差多少年月日
     * 比如：2011-02-02 到 2017-03-02 相差 6年，1个月，0天
     *
     * @paramfromDate YYYY-MM-DD
     * @paramtoDate   YYYY-MM-DD
     * @return年,月,日 例如 1年1月1天
     */
    public static String dayComparePrecise(LocalDate fromDate, LocalDate toDate){
        Period period= Period.between(fromDate, toDate);
        StringBuffer sb= new StringBuffer();
        if(period.getYears()>0) {
            sb.append(period.getYears()).append("年");
        }
        if(period.getMonths()>0) {
            sb.append(period.getMonths()).append("月");
        }
        if(period.getDays()>0) {
            sb.append("零").append(period.getDays()).append("天");
        }

        return sb.toString();
    }

//    /**
//     * 求指定日期加N年 ，例如：2022+10年
//     * @param date 需要增加的日期字符串
//     * @param addYears 增加的年数
//     * @return 增加n年后的指定格式日期字符串
//     */
//    public static String dateAddYear(String date, int addYears) {
//        Date dateObj = toDate(date);
//
//        return dateAddYear(dateObj, addYears);
//    }
//    public static String dateAddYear(Date date, int addYears) {
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(date);//设置起时间
//        cal.add(Calendar.YEAR, addYears);//年份加法
//        //cd.add(Calendar.MONTH, 3);//增加3个月
//        //cd.add(Calendar.DATE, 10);//增加10天
//        //cd.add(Calendar.DATE, -20);//减20天
//        String dateStr = toDateStr(cal.getTime());
//
//        return toDateStr(cal.getTime());
//    }

    //

    /**
     *
     * @param startTime
     * @param endTime
     * @return 返回样例: 731天1时10分10秒
     */
    public static String dateDiff(Date startTime, Date endTime){
        return dateDiff(startTime,endTime, true);
    }

    /**
     *
     * @param startTime
     * @param endTime
     * @param isSecond
     * @return 返回样例: 731天1时10分10秒
     */
    public static String dateDiff(Date startTime, Date endTime,boolean isSecond){
        String str="";
        try {
            //SimpleDateFormat sd = new SimpleDateFormat(DATETIME_FORMAT);
            long nd = 1000 * 24 * 60 * 60; //一天的毫秒数
            long nh = 1000 * 60 * 60;//一小时的毫秒数据
            long nm = 1000 * 60; //一分钟毫秒数
            long ns = 1000;
            long diff;
            //获得两个时间的毫秒时间差异
            diff = endTime.getTime() - startTime.getTime();
            // 计算差多少天
            long day = diff / nd;
            // 计算差多少小时
            long hour = diff % nd / nh;
            // 计算差多少分钟
            long min = diff % nd % nh / nm;
            // 计算差多少秒//输出结果
            long second = diff % nd % nh % nm / ns;

            if(day>0)str +=day + "天";
            if(hour>0)str+=hour+ "时";
            if(min>0)str +=min + "分";
            if(isSecond && second>0) str+= min + "秒";
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return str;
    }


//    public static long between(Date endDate){
//        //获得两个时间的毫秒时间差异
//        return DateUtils.getCurrent().getTime() - endDate.getTime();
//    }
//    /**
//     * @param endDate
//     * @return 返回毫秒
//     */
//    public static long between(Date startDate, Date endDate){
//        //获得两个时间的毫秒时间差异
//        return startDate.getTime() - endDate.getTime();
//    }


    /**
     * 传入开始时间 结束时间即可, 时间格式:
     *                HH:mm
     *                HH:mm:ss
     *                HH:mm:ss.SSS
     *                HH:mm:ss SSS
     * @param startTime
     * @param endTime
     * @return 返回时差毫秒数. startTime > endTime 时为正数
     */
    public static long betweenMillis(String startTime, String endTime){
        String nowDate  = DateUtils.getDate();   //当前日期,格式: yyyy-MM-dd

        Date   nowtime;
        if (Misc.isNull(startTime)){
            nowtime= DateUtils.getCurrent();
        }else{
            nowtime= DataHelper.parseDate(nowDate+" "+startTime);
        }
        Date endDate= DataHelper.parseDate(nowDate+" "+endTime);

        //获得两个时间的毫秒时间差异
        long diff= nowtime.getTime() - endDate.getTime();

        return diff;
    }





    public static long betweenMillis(String endTime){
        return betweenMillis(DateUtils.getTimeByHHmmssSSS(), endTime);
    }


    public static void main(String[] args) {
//        log.debug("");
//
//        long diff1 = DateUtils.betweenMillis("09:01", "08:59")/1000/60;
//
//        log.debug("");
//        log.debug("##diff {} {}" ,diff1/1000/60);
//        log.debug("");

        LocalTime now = LocalTime.now();
        String timeString = "03:45:00";
        try {
            LocalTime localTime = LocalTime.parse(timeString);
            System.out.println("转换成功: " + localTime);
        } catch (DateTimeParseException e) {
            System.out.println("字符串不是有效的时间格式");
        }


        LocalTime time = LocalTime.of(8, 59); // 8:30

        // 加1小时30分钟
        LocalTime addedTime = time.plusMinutes(2);
        System.out.println("加1小时30分钟后: " + addedTime);

        // 减1小时30分钟
        LocalTime subtractedTime = time.minusMinutes(1);
        System.out.println("减1小时30分钟后: " + subtractedTime);



        LocalTime time1 = LocalTime.of(10, 59); // 10:30
        LocalTime time2 = LocalTime.of(11, 01); // 11:45

        long minutesBetween = ChronoUnit.MINUTES.between(time1, time2);

        System.out.println("Minutes between time1 and time2: " + minutesBetween);

    }




}


