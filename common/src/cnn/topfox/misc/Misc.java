package cnn.topfox.misc;

import com.alibaba.fastjson2.JSONObject;
import cnn.topfox.common.AppContext;
import cnn.topfox.common.DataDTO;
import cnn.topfox.common.ResponseCode;
import cnn.topfox.common.TopException;
import cnn.topfox.data.DataHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.util.ClassUtils;
import org.springframework.util.ResourceUtils;
import java.io.*;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

@Slf4j
public class Misc {
    static String appPath;
    /**
     * 测试
     *
     * @param args
     */
    public static void main(String[] args) {
//        String str = "abc3443abcfgjhg abcgfjabc";
//        String rgex = "abc(.*?)abc";
//        log.debug(getSubUtil(str, rgex));
//        log.debug(getSubUtilSimple(str, rgex));
//
//        log.debug(getSubUtilSimple(str, rgex));
//
//        Integer value1 = 0;
//        Double  value2 = 0.00;
//        Boolean value3 = true;
//        Date  value4 = new Date();
//        log.debug(DataHelper.isBaseDataType(value1.getClass()));
//        log.debug(DataHelper.isBaseDataType(value2.getClass()));
//        log.debug(DataHelper.isBaseDataType(value3.getClass()));
//        log.debug(DataHelper.isBaseDataType(value4.getClass()));


        log.debug(toChinese(new BigDecimal(1566.78)));


    }


    private static final String[] CN_UPPER_NUMBER = { "零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖" };
    private static final String[] CN_UPPER_MONETRAY_UNIT = { "分", "角", "元", "拾", "佰", "仟", "万", "拾", "佰", "仟", "亿", "拾", "佰", "仟" };
    private static final String CN_FULL = "整";
    private static final String CN_NEGATIVE = "负";
    private static final int MONEY_PRECISION = 2;
    private static final String CN_ZEOR_FULL = "零元" + CN_FULL;

    public static String toChinese(BigDecimal amount) {
        StringBuilder result = new StringBuilder();
        amount = amount.setScale(MONEY_PRECISION, BigDecimal.ROUND_HALF_UP);
        long number = amount.movePointRight(MONEY_PRECISION).setScale(0, BigDecimal.ROUND_HALF_UP).longValue();
        boolean zero = true;
        int unitIndex = 0;
        if (number == 0) {
            return CN_ZEOR_FULL;
        }
        if (number < 0) {
            number = -number;
            result.append(CN_NEGATIVE);
        }
        long scale = 10;
        while (true) {
            if (number == 0) {
                break;
            }
            long numIndex = number % scale;
            if (zero && numIndex == 0) {
                zero = false;
            }
            if (numIndex != 0) {
                result.insert(0, CN_UPPER_MONETRAY_UNIT[unitIndex])
                        .insert(0, CN_UPPER_NUMBER[(int) numIndex]);
                zero = false;
            }
            else if (!zero) {
                result.insert(0, CN_UPPER_NUMBER[(int) numIndex]);
            }
            number = number / scale;
            unitIndex++;
        }
        if (zero) {
            result.append(CN_FULL);
        }
        String ret = result.toString();
        ret=ret.replaceAll("零零零零零零零零","零");
        ret=ret.replaceAll("零零零零零零零","零");
        ret=ret.replaceAll("零零零零零零","零");
        ret=ret.replaceAll("零零零零零","零");
        ret=ret.replaceAll("零零零零","零");
        ret=ret.replaceAll("零零零","零");
        ret=ret.replaceAll("零零","零");
        if (ret.endsWith("零")){
            ret = ret.substring(0,ret.length()-1);
        }
        if (ret.endsWith("元")){
            ret = ret+"整";
        }
        return ret;
    }


    /**
     * @method : 判断Object 是否存在 与 值是否为空 支持Object, String, List.size(), Map.key
     */
    public static boolean isNull(Object obj) {
        if (obj == null) {
            return true;
        }
        if (obj instanceof String) {
            String str = obj.toString().trim();
            if (str.equals("") || str.equals("null") || str.equals("undefined")) {
                return true;
            }
        }
        if (obj instanceof String[]) {
            String[] str = (String[]) obj;
            if (str.length == 0) {
                return true;
            }
        }
        if (obj instanceof List) {
            List list = (List) obj;
            if (list.size() == 0) {
                return true;
            }
        }
        if (obj instanceof Map) {
            Map map = (Map) obj;
            return map.isEmpty();
        }
        return false;
    }

    /**
     * @method : 判断参数值 是否不存在 与 是否不为空 支持Object, String, List.size(), Map.key
     */
    public static Boolean isNotNull(Object obj) {
        return !isNull(obj);
    }


    private static String path = null;

    public static String getClassPath() {
        if (path == null) {
            path = ClassUtils.getDefaultClassLoader().getResource("").getPath();
        }
        if (Misc.isNull(path)) {
            try {
                path = ResourceUtils.getURL("classpath:").getPath();
            } catch (FileNotFoundException e) {
                path = System.getProperty("user.dir") + "/";
            }
        }

        return path;
    }

    public static String fillStr(Object as_str, int ai_len, String as_fill_str) {
        for (int i = 0; i < ai_len; i++) {
            if (as_str.toString().length() < ai_len) as_str = as_fill_str + as_str;
        }

        return as_str.toString();
    }

    public static String fillStr2(Object as_str, int ai_len, String as_fill_str) {
        for (int i = 0; i < ai_len; i++) {
            if (as_str.toString().getBytes().length < ai_len) as_str = as_str + as_fill_str;
        }

        return as_str.toString();
    }

    //将字符按照指定的 分割符切分成数据ArrayList
    public static List<String> string2Array(String values, String split) {
        ArrayList<String> arrayList = new ArrayList();
        if (values == null) {
            return arrayList;
        }
        if (split.equals(",") && values.indexOf("，")>=0) {
            values = values.replace("，", ",");
        }
        StringTokenizer st = new StringTokenizer(values, split);
        while (st.hasMoreTokens()) {
            String _value = st.nextToken().trim();
            if (!isNull(_value) && !arrayList.contains(_value)) {
                arrayList.add(_value);
            }
        }

        return arrayList;
    }

    public static Set<String> string2Set(String values, String split) {
        Set<String> set = new HashSet();
        if (isNull(values)) return set;
        if (split.equals(",") && values.indexOf("，")>=0) {
            values = values.replace("，", ",");
        }
        StringTokenizer st = new StringTokenizer(values, split);
        while (st.hasMoreTokens()) {
            String _value = st.nextToken().trim();
            if (!isNull(_value)) {
                set.add(_value);
            }
        }

        return set;
    }

    public static List<String> string2List(String values, String split) {
        List<String> list = new ArrayList<>();
        if (isNull(values)) return list;

        if (split.equals(",") && values.indexOf("，")>=0) {
            values = values.replace("，", ",");
        }
        StringTokenizer st = new StringTokenizer(values, split);
        while (st.hasMoreTokens()) {
            String _value = st.nextToken().trim();
            //if (!isNull(_value)) {  luojp 2019-12-13
                list.add(_value);
            //}
        }

        return list;
    }

//    public static Set<Number> number2Set(String values, String split){
//        Set<Number> set = new HashSet();
//        if (isNull(values)) return set;
//
//        values=values.replace("，",",");
//        StringTokenizer st=new StringTokenizer(values,split);
//        while(st.hasMoreTokens()) {
//            Number _value=st.nextToken();
//            if (!isNull(_value)) {
//                set.add(_value);
//            }
//        }
//
//        return set;
//    }

    /**
     * 多个Ids 处理函数
     * 传入 array2Set("01,02","03,01","02")是 返回 set 01,02,03
     *
     * @param values
     * @return
     */
    public static Set<String> idsArray2Set(String... values) {
        Set<String> set = new HashSet<>();
        for (String value : values) {
            if (value == null) continue;

            set.addAll(string2Set(value, ","));
        }
        return set;
    }

    public static Set<Object> idsArray2Set(Number... values) {
        Set<Object> set = new HashSet<>();
        for (Number value : values) {
            if (value == null) continue;
            set.add(value);
        }
        return set;
    }

    /**
     * 返回 A,B,C ...
     * @param collection
     * @return
     */
    public static String array2String(Collection collection) {
        return array2String(collection, null);
    }
    public static String array2String(Collection collection,String paichude) {
        if (collection.size()==0) return "";
        StringBuilder sb = new StringBuilder();

        for (Object value : collection) {
            if (value==null || "".equals(value.toString())) value="null";
            if (paichude!=null && paichude.equals(value.toString())) continue;
            sb.append(value);
            sb.append(",");
        }
        sb.setLength(sb.length()-1);
        return sb.toString();
    }



    public static Map<String, JSONObject> toKeysMap(List<Map<String, Object>> data, String fieldName) {
        Map<String, JSONObject> rowsOriginal = new HashMap<String, JSONObject>();
        for (Map<String, Object> rowMap : data) {
            JSONObject row = new JSONObject(rowMap);
            rowsOriginal.put(row.getString(fieldName), row);
        }
        return rowsOriginal;
    }
    public static JSONObject toKeysJSONObject(List<JSONObject> data, String fieldName) {
        JSONObject rowsOriginal = new JSONObject();
        for (Map<String, Object> rowMap : data) {
            JSONObject row = new JSONObject(rowMap);
            rowsOriginal.put(row.getString(fieldName), row);
        }
        return rowsOriginal;
    }

    public static <T extends DataDTO> Map<String,T> toKeysDTO(List<T> list, String fieldName) {
        Map<String,T> mapData = new HashMap();
        for (T  dto : list) {
            mapData.put(dto.getValue(fieldName).toString(), dto);
        }
        return mapData;
    }

    //将字符按照指定的 分割符切分成数据ArrayList
    public static JSONObject urlParams2Map(String string) {
        return stringToHashMap(string, "&");
    }
    /**
     * Map转url参数形式  key1=value1&key=value2 的形式
     * @param source
     * @return
     */
    public static String asUrlParams(JSONObject source){
        Iterator<String> it = source.keySet().iterator();
        StringBuilder paramStr = new StringBuilder();
        while (it.hasNext()){
            String key = it.next();
            Object value=source.get(key);
            if (isNull(value)){
                continue;
            }
            paramStr.append("&").append(key).append("=").append(value);
        }
        // 去掉第一个&
        return paramStr.substring(1);
    }


    //将字符按照指定的 分割符切分成数据ArrayList
    public static JSONObject stringToHashMap(String string, String split) {
        if (string == null || string.equals("")) return null;

        List<String> arrayList = string2Array(string, split);
        JSONObject hashMap = new JSONObject();
        for (String str : arrayList) {
            String[] pos = str.toString().split("=");
            hashMap.put(pos[0], pos.length == 1 ? "" : pos[1]);
        }
        return hashMap;
    }

    public static HashMap<String, String> arryToHashMap(String[] str) {
        HashMap<String, String> map = new HashMap<String, String>();
        if (isNull(str) || str.length == 0) return map;
        for (String s : str) {
            map.put(s, s);
        }
        return map;
    }

    /**
     * 转换key为全小写下划线, 根据配置true : spring.jackson.property-naming-strategy=CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES
     */
    private static String caseKey(String key) {
        if (AppContext.getSysConfig() != null) {
            if (AppContext.getSysConfig().isCommitDataKeysIsUnderscore()) {
                key = CamelHelper.toUnderlineName(key).toLowerCase();
            }
        }
        return key;
    }

    /**
     * @method : obj 非空校验
     */
    public static void checkObjNotNull(Object object, String keyName) {
        if (isNull(object)) {
            throw TopException.newInstance(ResponseCode.PARAM_IS_NULL).text("参数值[" + caseKey(keyName) + "]不能为空！");
        }
    }

    /**
     * @method : javaBean 非空校验
     */
    public static <T> void checkBeansNotNull(T t, String... keys) {
        if (isNull(t)) {
            throw TopException.newInstance(ResponseCode.PARAM_IS_NULL).text("入参不存在！");
        }
        for (String key : keys) {
            try {
                Field reflectIds = t.getClass().getDeclaredField(key);
                reflectIds.setAccessible(true);
                if (isNull(reflectIds.get(t))) {
                    throw TopException.newInstance(ResponseCode.PARAM_IS_NULL).text("参数值[" + caseKey(key) + "]不能为空！");
                }
            } catch (Exception e) {
                if (e instanceof NoSuchFieldException) {
                    throw TopException.newInstance(ResponseCode.PARAM_IS_INEXISTENCE).text("参数[" + caseKey(key) + "] 不存在！");
                }
                throw TopException.newInstance(ResponseCode.NULLException).text(e.getMessage());
            }
        }
    }

    /**
     * @method: Map 非空校验
     */
    public static void checkMapsNotNull(Map<String, Object> map, String... keys) {
        if (isNull(map)) {
            throw TopException.newInstance(ResponseCode.PARAM_IS_NULL).text("入参不能为空！");
        }
        StringBuilder stringBuilderKeys = new StringBuilder("参数[");
        StringBuilder stringBuildevValues = new StringBuilder("参数值[");

        for (String key : keys) {
            if (map.containsKey(key) == false) {
                stringBuilderKeys.append(caseKey(key)).append(",");
            }
            if (isNull(map.get(key))) {
                stringBuildevValues.append(caseKey(key)).append(",");
            }
        }
        if (stringBuilderKeys.length() > 0) {
            stringBuilderKeys.setLength(stringBuilderKeys.length() - 1);//去掉最后的逗号和空格
            throw TopException.newInstance(ResponseCode.PARAM_IS_NULL).text(stringBuilderKeys.append("]不存在！").toString());
        }
        if (stringBuildevValues.length() > 0) {
            stringBuildevValues.setLength(stringBuildevValues.length() - 1);//去掉最后的逗号和空格
            throw TopException.newInstance(ResponseCode.PARAM_IS_NULL).text(stringBuildevValues.append("]不能为空！").toString());
        }
    }

    public static String set2String(Set<String> setValues) {
        return setValues.toString().substring(1).replace("]", "");
    }


    public static String getWhereOrSql(List<JSONObject> rows, String valueField, String whereField) {
        Set<String> ids = new HashSet<>();
        for (JSONObject row : rows) {
            ids.add(row.getString(valueField));
        }
        return getWhereOrSql(ids, whereField);
    }

    /**
     * @param values（传入多个值，英文逗号隔开的,如 a,b,c）
     * @param field
     * @return 返回  (field='a' OR field='b' OR field='b')
     */
    public static String getWhereOrSql(String values, String field) {
        return getWhereOrSql(Misc.string2Set(values, ","), field);
    }


    /**
     * 返回  (field='value1' OR field='value2' ...)
     * value1、value2...是第一个参数 setValues中的值
     *
     * @param setValues
     * @param field
     * @return
     */
    public static String getWhereOrSql(Collection<?> setValues, String field) {
        if (setValues == null || setValues.size() == 0) return "(1=2)";
        StringBuilder whereOrStringBuilder = new StringBuilder("(");
        for (Object key : setValues) {
            if (Misc.isNull(key)) continue;
            if (key instanceof Number){
                whereOrStringBuilder.append(field).append("=").append(key).append(" OR ");
            }else{
                whereOrStringBuilder.append(field).append("='").append(key).append("' OR ");
            }
        }
        whereOrStringBuilder.append(")");
        String temp = whereOrStringBuilder.toString();
        temp = temp.replace(" OR )", ")");
        return Misc.isNull(temp) || temp.equals("()") ? "(1=2)" : temp;
    }

    public static String getWhereLikeOrSql(List<String> arrayListValues, String field) {
        StringBuilder whereOrStringBuilder = new StringBuilder();
        if (arrayListValues.size() > 0) {
            whereOrStringBuilder.append("(");
            for (int i = 0; i < arrayListValues.size(); i++) {
                if (i == arrayListValues.size() - 1)
                    whereOrStringBuilder.append(field).append(" LIKE '").append("%").append(arrayListValues.get(i)).append("%").append("'");
                else
                    whereOrStringBuilder.append(field).append(" LIKE '").append("%").append(arrayListValues.get(i)).append("%").append("' OR ");
            }
            whereOrStringBuilder.append(")");

        }
        String temp = whereOrStringBuilder.toString();
        return Misc.isNull(temp) || temp.equals("()") ? "(1=2)" : temp;
    }

    public static String getWhereNotLikeSql(List<String> arrayListValues, String field) {
        StringBuilder whereOrStringBuilder = new StringBuilder();
        if (arrayListValues.size() > 0) {
            whereOrStringBuilder.append("(");
            for (int i = 0; i < arrayListValues.size(); i++) {
                if (i == arrayListValues.size() - 1)
                    whereOrStringBuilder.append(field).append(" NOT LIKE '").append("%").append(arrayListValues.get(i)).append("%").append("'");
                else
                    whereOrStringBuilder.append(field).append(" NOT LIKE '").append("%").append(arrayListValues.get(i)).append("%").append("' AND ");
            }
            whereOrStringBuilder.append(")");

        }
        String temp = whereOrStringBuilder.toString();
        return Misc.isNull(temp) || temp.equals("()") ? "(1=2)" : temp;
    }

    public static String getWhereNotInSql(List<String> arrayValue, String field) {
        StringBuilder whereOrStringBuilder = new StringBuilder("");
        arrayValue.remove(null);
        arrayValue.remove("");
        if (arrayValue.size() > 0) {
            whereOrStringBuilder.append("(");
            for (int i = 0; i < arrayValue.size(); i++) {
                if (i == arrayValue.size() - 1)
                    whereOrStringBuilder.append(field).append("<>'").append(arrayValue.get(i)).append("'");
                else
                    whereOrStringBuilder.append(field).append("<>'").append(arrayValue.get(i)).append("' AND ");
            }
            whereOrStringBuilder.append(")");

        }

        String temp = whereOrStringBuilder.toString();
        return Misc.isNull(temp) || temp.equals("()") ? "(1=1)" : temp;
    }

    public static String getString(List list, String column) {
        if (list.size() == 0) return "";
        ArrayList<String> arrayList = new ArrayList<String>();
        for (Object data : list) {
            DataDTO row=(DataDTO)data;
            Object value = row.getValue(column);
            String valueString = value==null?"":value.toString();
            if (arrayList.contains(valueString) == false)
                arrayList.add(valueString);
        }
        String tempString = arrayList.toString();
        String valueString = tempString.substring(1, tempString.length() - 1);
        return valueString.replace(" ","");
    }


    /**
     * 返回的值是 逗号串起来的 A,B,C
     *
     * @param list
     * @param cols 多个字段名
     * @return
     * @throws Exception
     */
    public static String getValueStringByCols(List<JSONObject> list, String cols) {
        return getValueStringByCols(list, cols, "#");
    }

    /**
     * @param list        数据集合
     * @param cols        多个字段名
     * @param splitString 返回多个字段值的分隔符号
     * @return 返回的值是 都好串起来的 如果cols是两个字段，list里面有两行数据，则返回value1#value2,value3#value4
     * @throws Exception
     */
    public static String getValueStringByCols(List<JSONObject> list, String cols, String splitString) {
        String colsArray[] = cols.split(",");
        if (list == null || colsArray == null || list.size() == 0 || colsArray.length == 0) return "";

        ArrayList<String> arrayList = new ArrayList<String>();

        for (JSONObject row : list) {
            String oneValuesString = "";
            for (int i = 0; i < colsArray.length; i++) {
                String column = colsArray[i];
                if (Misc.isNull(column)) continue;
                column = column.trim();
                String oneValue = row.getString(column);
                if (oneValue == null) oneValue = "";
                if (i == 0) oneValuesString = oneValuesString + oneValue;
                else oneValuesString = oneValuesString + splitString + oneValue;
            }

            if (arrayList.contains(oneValuesString) == false) {
                arrayList.add(oneValuesString);
            }

        }

        String tempString = arrayList.toString();
        String retValue = tempString.substring(1, tempString.length() - 1);
        return retValue;
    }


    public static String getValueForDTO(List list, String column) {
        return getValueForDTO(list, column, false);
    }
    /**
     * 返回的值是 都好串起来的 A,B,C  或其中一行的值(单个)
     *
     * @param list
     * @param column
     * @param isOne
     * @return
     * @throws Exception
     */
    public static String getValueForDTO(List list, String column, Boolean isOne) {
        if (list.size() == 0) return "";

        ArrayList<String> arrayList = new ArrayList<>();
        String valueString;
        for (Object object : list) {
            DataDTO dto;
            if (object instanceof DataDTO ) {
                dto = (DataDTO) object;
            }else{
                break;
            }
            if ("d".equals(dto.dataState())) continue;

            Object value=dto.getValue(column);
            if (Misc.isNull(value)) continue;
            valueString = value.toString().trim();
            if (arrayList.contains(valueString) == false)
                arrayList.add(valueString);
        }
        if (isOne){
            return arrayList.size()==0?"":arrayList.get(0);
        }
        String tempString = arrayList.toString();
        valueString = tempString.substring(1, tempString.length() - 1);
        valueString = valueString.replaceAll(", ",",");
        return valueString;
    }

    public static String getValueString(List<JSONObject> list, String column) {
        if (list.size() == 0) return "";
        ArrayList<String> arrayList = new ArrayList<String>();
        String valueString;
        for (JSONObject row : list) {
            valueString = row.getString(column);
            if (arrayList.contains(valueString) == false)
                arrayList.add(row.getString(column));
        }
        String tempString = arrayList.toString();
        valueString = tempString.substring(1, tempString.length() - 1);
        return valueString;
    }

    public static String getValueString(List list, String keyField, String keyFieldValue, String column) {
        return getValueString(list, new String[]{keyField},
                new String[]{keyFieldValue}, column);
    }

    public static String getValueString(List<JSONObject> list, String[] keyFields,
                                        String[] keyFieldsValue, String column) {
        JSONObject row = null;
        String valueString = "";
        String keyFieldsValueStringCurrent = "";
        String keyFieldsValueString = "";// 传进来的值 串起来的

        for (int j = 0; j < keyFieldsValue.length; j++) {
            keyFieldsValueString += keyFieldsValue[j];
        }
        for (int k = 0; k < list.size(); k++) {
            row = list.get(k);
            keyFieldsValueStringCurrent = "";
            for (int i = 0; i < keyFields.length; i++) {
                keyFieldsValueStringCurrent += row.getString(keyFields[i]);
            }

            if (keyFieldsValueStringCurrent.equals(keyFieldsValueString)) {
                String tempString = (String) row.getString(column);
                if (tempString == null) {
                    tempString = "";
                }
                if (valueString == "") {
                    valueString = tempString;
                } else {
                    valueString = valueString + "," + tempString;
                }
            }
        }
        String array[] = valueString.split(",");
        List<String> listString = new ArrayList<String>();
        for (int i = 0; i < array.length; i++) {
            if (!listString.contains(array[i]) && !array[i].equals("*")) {
                listString.add(array[i]);
            }
        }
        String tempString = listString.toString();
        valueString = tempString.substring(1, tempString.length() - 1);
        return valueString;
    }


    /**
     * 正则表达式匹配两个指定字符串中间的内容
     *
     * @param soap
     * @return
     */
    public static List<String> getSubUtil(String soap, String rgex) {
        List<String> list = new ArrayList<String>();
        Pattern pattern = Pattern.compile(rgex);// 匹配的模式
        Matcher m = pattern.matcher(soap);
        while (m.find()) {
            int i = 1;
            list.add(m.group(i));
            i++;
        }
        return list;
    }

    /**
     * 返回单个字符串，若匹配到多个的话就返回第一个，方法与getSubUtil一样
     *
     * @param soap
     * @param rgex
     * @return
     */
    public static String getSubUtilSimple(String soap, String rgex) {
        Pattern pattern = Pattern.compile(rgex);// 匹配的模式
        Matcher m = pattern.matcher(soap);
        while (m.find()) {
            return m.group(1);
        }
        return "";
    }

    /**
     * 正则表达式匹配两个指定字符串中间的内容
     *
     * @param string
     * @return
     */
    public static String getSubLeft(String string, String find) {
        int pos = string.lastIndexOf(find);
        return string.substring(pos + find.length());
    }


    public static double round(double number,String format){
        java.text.DecimalFormat   df=new   java.text.DecimalFormat(format);
        String amt=df.format(number);
        return Double.parseDouble(amt);
    }

    /**
     *
     * @param number
     * @param format   ###0.00
     * @return
     */
    public static String roundToString(Double number,String format){
        java.text.DecimalFormat df = new java.text.DecimalFormat(format);
        String amt=df.format(number);
        return amt;
    }
    public static double round(double dSource, int weishu){

//	    double iRound;
//	    //BigDecimal的构造函数参数类型是double
//	    BigDecimal deSource = new BigDecimal(dSource);
//	    //deSource.setScale(0,BigDecimal.ROUND_HALF_UP) 返回值类型 BigDecimal
//	    //intValue() 方法将BigDecimal转化为int
//	    iRound= deSource.setScale(weishu,BigDecimal.ROUND_HALF_UP).doubleValue();
//	    deSource=null;
        return round(dSource, weishu, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 对double数据进行取精度.
     * @param value  double数据.
     * @param scale  精度位数(保留的小数位数).
     * @param roundingMode  精度取值方式.
     *        四舍五入：BigDecimal.ROUND_HALF_UP
     *        截取               BigDecimal.ROUND_DOWN
     * @return double 精度计算后的数据.
     */
    public static double round(double value, int scale,
                               int roundingMode) {
        double value2=value;
        if (value<0)value2=-value;
        BigDecimal bd = new BigDecimal(Double.toString(value2));
        bd = bd.setScale(scale, roundingMode);
        double ret = bd.doubleValue();
        bd = null;
        return value<0?-ret:ret ;
    }




    /**
     * 字符串压缩为字节数组
     */
    public static byte[] gzip2byte(String str) {
        return gzip2byte(str, null);
    }

    public static byte[] gzip2byte(String str, String encoding) {
        if (str == null || str.length() == 0) {
            return null;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        GZIPOutputStream gzipout = null;
        try {
            gzipout = new GZIPOutputStream(out);
            if (encoding == null) {
                gzipout.write(str.getBytes());
            } else {
                gzipout.write(str.getBytes(encoding));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (gzipout != null) try {
                gzipout.close();
            } catch (IOException e) {
            }
            if (out != null) try {
                out.close();
            } catch (IOException e) {
            }
        }
        return out.toByteArray();
    }


    /**
     * 压缩为字符串
     */
    public static String gzip2String(String primStr) {
        return Base64.getEncoder().encodeToString(gzip2byte(primStr));
    }

    public static String gzip2String(String primStr, String encoding) {
        return Base64.getEncoder().encodeToString(gzip2byte(primStr, encoding));
    }

    /* 字节数组解压缩后返回字符串     */
    public static String gunzip(byte[] bytes) {
        return gunzip(bytes, null);
    }

    public static String gunzip(byte[] bytes, String charsetName) {
        if (bytes == null || bytes.length == 0) {
            return null;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayInputStream in = new ByteArrayInputStream(bytes);
        GZIPInputStream gunzip = null;
        try {
            gunzip = new GZIPInputStream(in);
            byte[] buffer = new byte[1024];
            int n;
            while ((n = gunzip.read(buffer)) >= 0) {
                out.write(buffer, 0, n);
            }

            if (charsetName == null || charsetName.equals("")) {
                return out.toString();
            } else {
                return out.toString(charsetName);
            }
        } catch (java.util.zip.ZipException e2) {
            throw new RuntimeException(e2);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (gunzip != null) try {
                gunzip.close();
            } catch (IOException e) {
            }
            if (in != null) try {
                in.close();
            } catch (IOException e) {
            }
            if (out != null) try {
                out.close();
            } catch (IOException e) {
            }
        }
    }

    /**
     * 解压
     */
    public static String gunzip(String compressedStr) {
        return gunzip(compressedStr, null);
    }

    /**
     * 解压
     */
    public static String gunzip(String compressedStr, String charsetName) {
        return gunzip(Base64.getDecoder().decode(compressedStr), charsetName);
    }

    /**
     * 判断字符串是否为数字 纯数字return:true 否则false
     */
    public static boolean isNumeric(String str) {
        if (isNull(str)) {
            return false;
        }
        Pattern pattern = Pattern.compile("[0-9]*");
        Matcher isNum = pattern.matcher(str);
        if (!isNum.matches()) {
            return false;
        }
        return true;
    }

    public static boolean isDebugEnabled() {
        return log.isDebugEnabled();
    }
    /**
     * 得到项目的根路径  如jar文件所在的路径  最后以 / 结束
     */
    public static String getAppPath() {
        //第五种
        ApplicationHome h = new ApplicationHome(new MD5().getClass());
        File jarFile = h.getSource();
        if (Misc.isNull(appPath) ) {
            if (log.isDebugEnabled()){
                appPath = getSystemResourcePath();
            }else {
                appPath = jarFile.getParentFile().toString() + "/";
            }

            /**
             * jar包运行时,解决
             * appPath为 file:/java/zcwl/target/app.jar!/BOOT-INF/classes!
             *      返回 /java/zcwl/target/
             */
            log.info("app路径(原始) {}",appPath);

            if (appPath.contains("file:")){
                appPath=appPath.replace("file:","");

                int indexJar=appPath.indexOf(".jar");
                if (indexJar>0){
                    appPath = appPath.substring(0,indexJar);

                    appPath = appPath.substring(0,appPath.lastIndexOf("/")+1);
                }
            }
            if (appPath.equals("//")){
                appPath="/"; //linux下的根目录
            }
            log.info("app路径(处理后) {}",appPath);
        }

        return appPath;
    }
    //获得项目路径 luojp  改进
    public static String getSystemResourcePath() {
        String path = ClassUtils.getDefaultClassLoader().getResource("").getPath();
        if (Misc.isNull(path)){
            try {
                path = ResourceUtils.getURL("classpath:").getPath();
            } catch (FileNotFoundException e) {
                path = System.getProperty("user.dir") + "/";
            }
        }

        if (Misc.isNull(path)){
            File f = new File(AppContext.getApplicationContext().getClass().getResource("/").getPath());
            path = f.getPath()+ "/";
        }

//		log.debug("getSystemResourcePath: "+path);
//
//		log.debug("getAppPath: "+getAppPath());

        return path;
    }

    //	Java中判断字符串是否为数字
//	转载:https://blog.csdn.net/u013066244/article/details/53197756
//
//	获得字符串包含数字的个数
    public static int getNumberCount(String str) {
        int numberCount=0;//字符有几位数字
        boolean isInteger=false;
        for (int i = 0; i < str.length(); i++) {
            char temp =str.charAt(i);
            isInteger=Character.isDigit(str.charAt(i));//是否数字
            if (isInteger) {
                numberCount++;
            }
        }
        return numberCount;//字符包含5位以上的数字, 识别位电话号码或者手机号
    }


    /**
     * 16位16进制
     * @return
     */
    public static String get16Number(){
        String keyId="";
        long number= Long.parseLong(System.currentTimeMillis()
                +Misc.fillStr(String.valueOf((int)(Math.random()*999)), 3, "0"));
        keyId=Long.toHexString(number);
        if(keyId.length()>16) {
            keyId= keyId.substring(0, 16);
        }else {
            int length=16-keyId.length();
            if(keyId.length()<16) keyId=String.format("%"+16+"s",keyId).replace(" ","F");

        }
        return keyId.toUpperCase();
    }

    public static int countString(String str,String s) {
        String str1 = str.replaceAll(s, "");
        int len1 = str.length(),len2 = str1.length(),len3 = s.length();
        int count = (len1 - len2) / len3;
        return count;
    }

}