package cnn.topfox.common;

import cnn.topfox.data.SqlUnderscore;
import org.springframework.core.env.Environment;

import java.util.concurrent.TimeUnit;

/**
 * 实现类: com.topfox.spring.SysConfigDefault
 */
public interface SysConfigRead extends IBean{

    Environment environment();
    void environment(Environment environment);

    String getAppName();

    /**
     * 默认100
     * 分页时,默认的每页条数
     */
    int getPageSize();

    /**
     * 默认20000
     * 不分页时(pageSize小于等于0),查询时最多返回的条数
     */
    int getMaxPageSize();

    /**
     # 默认3
     # 重要参数:更新时DTO序列化策略 和 更新SQL生成策略
     # 1 时, service的DTO=提交的数据.               更新SQL 提交数据不等null 的字段 生成 set field=value
     # 2 时, service的DTO=修改前的原始数据+提交的数据. 更新SQL (当前值 != 原始数据) 的字段 生成 set field=value
     # 3 时, service的DTO=修改前的原始数据+提交的数据. 更新SQL (当前值 != 原始数据 + 提交数据的所有字段)生成 set field=value
     #   值为3, 则始终保证了前台(调用方)提交的字段, 不管有没有修改, 都能生成更新SQL, 这是与2最本质的区别
     */
    int getUpdateMode();

    /**
     * 默认 true
     * # redis序列化支持两种, true:jackson2JsonRedisSerializer false:JdkSerializationRedisSerializer
     * # 注意, 推荐生产环境下更改为 false, 类库将采用JdkSerializationRedisSerializer 序列化对象,
     * # 这时必须禁用devtools(pom.xml 注释掉devtools), 否则报错.
     */
    boolean isRedisSerializerJson();

    /**
     * 一级缓存开关 当前线程的缓存
     * 默认 false
     * service层是否开启redis缓存, 增删改查 将不会启用redis
     */
    boolean isThreadCache();

    /**
     * 二级缓存开关 redis缓存
     * 默认 false
     * service层是否开启redis缓存, 增删改查 将不会启用redis
     */
    boolean isRedisCache();

    /**
     * 默认 false 控制台是否打印操作redis的日志
     */
    boolean isRedisLog();

    /**
     * redis过期时间,单位为小时, 默认 24
     * top.service.redis-expire
     */
    int getRedisExpire();

    /**
     * redis过期时间单位
     */
    TimeUnit getTimeUnit();

    /**
     * 默认 true
     * 更新记录返回0时 是否抛出异常
     * true  抛出异常
     * false 不抛异常
     */
    boolean isUpdateNotResultError();

    /**
     * DTO自动生成SQL 是否启用 驼峰命名转下划线,且大写
     * 例子: 参数设置为true时则将 DTO中的 orderCustomerName 转为 ORDER_CUSTOMER_NAME
     * 默认 OFF, 值为以下3种
     *  1. OFF 关闭
     *  2. ON-UPPER 打开并大写
     *  3. ON-LOWER 打开并小写
     */
    SqlUnderscore getSqlCamelToUnderscore();

    /**
     * 默认 false 表示是驼峰命名方式, 与QTO DTO 的field对应的
     * 设置为 true时, 提交的数据后台自动转换, 保证序列化Wie DTO  List<DTO>不出错
     * 提交的数据的字段名是否是带下划线的小写
     */
    boolean isCommitDataKeysIsUnderscore();

    boolean isSelectByBeforeUpdate();


    String getLogStart();

    String getLogEnd();

    String getLogPrefix();

    String getSqlQtoOperator();

    /**
     * 获得数据库类型  mysql  postgresql
     * @return
     */
    String getDatabaseType();

    String getString(String key);

}
