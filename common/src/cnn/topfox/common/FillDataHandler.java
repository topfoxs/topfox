package cnn.topfox.common;

import cnn.topfox.data.Field;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface FillDataHandler {

    /**
     * 填充字段的事件
     * @param fillFields
     * @param list
     */
    void fillData(Map<String, Field> fillFields, List<DataDTO> list);

    /**
     * 定义插入时 需要填充字段的名称, 模糊匹配,不分大小写
     * @return
     */
    Set<String> getInsertFillFields();

    /**
     * 定义更新时 需要填充字段的名称, 模糊匹配,不分大小写
     * @return
     */
    Set<String> getUpdateFillFields();
}


