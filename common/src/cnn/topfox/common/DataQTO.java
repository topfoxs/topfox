package cnn.topfox.common;

import cnn.topfox.annotation.Ignore;

import cnn.topfox.data.Field;
import cnn.topfox.misc.BeanUtil;
import cnn.topfox.misc.JsonUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Map;

/**
 * 基础QTO
 */
@Accessors(chain = true)
public class DataQTO extends DataPojo {

    /**
     * Eq:等   Ne:不等
     * 任何字段名后缀说明(加入该字段的值是 value1,value2)
     * fieldOrEq       则生成SQL: (field = valus1 OR field = value2)
     * fieldAndNe      则生成SQL: (field <> valus1 AND field = value2)
     * fieldOrLike     则生成SQL: (field LIKE CONCAT("%",valus1,"%") OR field LIKE CONCAT("%",valus2,"%"))
     * fieldAndNotLike 则生成SQL: (field NOT LIKE CONCAT("%",valus1,"%") AND field NOT LIKE CONCAT("%",valus2,"%"))
     */

    /**
     *  与之对应的mybatis 的 mapper.xml文件配置如下
     <where>
     <if test = "idOrEq != null">AND ${idOrEq}</if>
     <if test = "idAndNe != null">AND ${idAndNe}</if>
     <if test = "idOrLike != null">AND ${idOrLike}</if>
     <if test = "idAndNotLike != null">AND ${idAndNotLike}</if>
     </where>
     */


    /**
     * 允许返回null
     */
    @Getter @Setter
    private Boolean allowNull;// = true;

    /**
     * 是否读取 一级 二级缓存的数据,  null 和true都会读取,  false不读取
     */
    @Ignore transient
    private boolean readCache=true;
    public DataQTO readCache(boolean value){
        readCache = value;
        return this;
    }
    public boolean readCache(){
        return readCache;
    }

    /**
     * set方法
     * 开启分页, 默认值 开启 true, 如果关闭后sql语句后 始终不生成 limit x,y .
     * 例如根据Id 字段的查询 getObject() , 就应该关闭 这个.
     */
    @Getter @Setter
    private Boolean openPage=true;  // 是否开启 mysql Limit  ,  false 时SQL语句就没有 limit 了
//    public DataQTO openPage(boolean value){
//        this.openPage = value;
//        return this;
//    }
//    public boolean openPage(){
//        return this.openPage;
//    }

    /**
     * 查询页码
     */
    @Getter @Setter
    private Integer pageIndex=0;

    /**
     * 每页条数
     */
    @Getter @Setter
    private Integer pageSize;


    /**
     * 类库专用字段. 类库根据 pageIndex 和 pageSize写值
     */
    @Getter @Setter
    private String limit;

    /**
     * 排序字段, 多个字段用逗号串起来
     */
    @Getter @Setter
    private String orderBy;

    /**
     * 分组字段
     */
    @Getter @Setter
    private String groupBy;

    @Getter @Setter
    private String having;

    /**
     * 蒋QTO的数据转为map
     * @return
     */
    public Map mapData(){
        Map map= BeanUtil.bean2Map(this);
        map.remove("pageIndex");map.remove("pageSize");map.remove("allowNull");map.remove("readCache");
        map.remove("limit");map.remove("orderBy");map.remove("groupBy");map.remove("having");
        return map;
    }

//    /**
//     * 检查qto中是否只存在完整的主键值(多主键时), 且无其他字段的值
//     * @return
//     */
//    public boolean checkOnlyKeysValue(){
//        Map mapQTO=mapData();
//        if (mapQTO.size() != tableInfo().getMapIdFields().size()){
//            mapQTO.clear();mapQTO=null;
//            return false;
//        }
//
//        //条件匹配器中是否只有主键字段的条件
//        boolean isOnlyIdValues = true;
//
//        //Id字段设置为null, 不支持修改的
//        Map<String, Field> mapKeyFields = tableInfo().getMapIdFields();
//        for(Map.Entry<String, Field> entry : mapKeyFields.entrySet()){
//            String key = entry.getKey();
//            if (isOnlyIdValues == true && !mapQTO.containsKey(key) ){
//                isOnlyIdValues = false; break;
//            }
//        }
//        mapQTO.clear();mapQTO=null;
//        return isOnlyIdValues;
//    }

    private Field findField(String fieldName){
        Field field = tableInfo().getField(fieldName);
        if (field == null ) {
            throw TopException.newInstance(ResponseCode.SYS_FIELD_ISNOT_EXIST).text("找不到DTO的主键字段:",fieldName);
        }
        return field;
    }

    public DataQTO setValue(String fieldName, Object value){
        Field field = findField(fieldName);
        BeanUtil.setValue(tableInfo(), this, field, value);
        return this;
    }
    public DataQTO getValue(String fieldName){
        Field field = findField(fieldName);
        BeanUtil.getValue(tableInfo(), this, field);
        return this;
    }
    @Override
    public String toString(){
        return JsonUtil.toString(this);
    }

}
