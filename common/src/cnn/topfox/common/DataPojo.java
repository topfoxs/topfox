package cnn.topfox.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cnn.topfox.annotation.Ignore;
import cnn.topfox.annotation.Other;
import cnn.topfox.data.DataHelper;
import cnn.topfox.data.TableInfo;
import cnn.topfox.misc.BeanUtil;
import cnn.topfox.misc.JsonUtil;
import cnn.topfox.misc.Misc;

import java.util.Map;

public class DataPojo implements IBean{

    @Other
    @JsonIgnore
    @Ignore
    transient private TableInfo tableInfo = null;
    public TableInfo tableInfo(){
        if (tableInfo == null) {
            tableInfo = TableInfo.get(this.getClass());
        }
        return tableInfo;
    }

    /**
     * 获得实体Id 解决多主键字段值用 - 号串起来
     * @return
     */
    public String dataId(){
        if (tableInfo().getIdField()==null){
            throw TopException.newInstance(ResponseCode.SYS_PK_FIELD_NOT_EXIST)
                    .text(getClass().getName()," 中找不到的主键字段(必须有一个字段用@Id注解)");
        }
        return DataHelper.parseString(BeanUtil.getValue(this, tableInfo().getIdField()));
    }

    /**
     * 获得实体主键值, 并检查存在空值则报错
     * @return
     */
    public String dataIdAndCheckNull(){
        String id=dataId();
        if (id == null || Misc.isNull(id)) {
            throw TopException.newInstance(ResponseCode.DATA_KEY_ISNULL)
                    .text("主键字段",tableInfo().getIdField(),"的值",id,"不能为空");
        }
        return id;
    }
//    @Deprecated
//    public String dataAllId(){
//        String ids=dataId();
//        if (Misc.isNull(ids) || ids.indexOf("-null")>=0 || ids.indexOf("null-")>=0 ) {
//            return null;
//        }
//        return ids;
//    }

    @Override
    public String toString(){
        return JsonUtil.toString(this);
    }
}
