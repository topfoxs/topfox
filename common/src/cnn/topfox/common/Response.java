package cnn.topfox.common;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import cnn.topfox.annotation.Ignore;
//import com.topfox.misc.JsonUtil;
import cnn.topfox.data.DataHelper;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import java.lang.reflect.Type;
import java.util.*;

@Slf4j
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Data
@Accessors(fluent = true)
public class Response<T> implements IBean {
    private static final long serialVersionUID = 201801001L;

    @JsonIgnore @Ignore transient boolean printStackTrace = true;


    private JSONObject appInfo;

    /**
     * 是否成功
     */
    private Boolean success;

    //http-status
    private Integer status=200;
//    /**
//     * 异常编码
//     */
//    private String code;

    /**
     * 详细信息
     */
    private String msg;

    /**
     * 前端(请求方)每次请求传入的 唯一 执行Id, 如果没有, 后端会自动生成一个唯一的
     */
    private String executeId;


    private String token;//对外接口用


    /**
     * 1.查询结果的所有页的行数合计,
     * 2.update insert delete SQL执行结果影响的记录数
     */
    private Integer totalCount;


    /**
     * 查询结果(当前页)的行数合计
     */
    private Integer pageCount;

    /**
     * 查询时每页多少行, 前台传入的参数
     */
    private Integer pageSize;

    /**
     * 数据, List<UserDTO> 或者是一个DTO
     */
    @JsonSerialize(using = ListJsonSerialize.class)
    private T data;


    /**
     * data 泛型 T 的实际类型
     */
    @JsonIgnore
    Type dataType;
    public Type dataType() {
        if (dataType == null && data != null) {
            dataType = data.getClass();
        }
        return dataType;
    }
    public void dataType(Type dataType) {
        if (this.dataType == null) {
            this.dataType = dataType;
        }
    }
    /**
     * 异常类名
     */
    private String exception;

    /**
     * 报错执行的SQL
     */
    private String sql;

    /**
     * 方法
     */
    private String method;


    /**
     * 始终取值 取 restSession.getAttributes()
     */

    private JSONObject attributes = new JSONObject();

    //临时返回的数据
    private JSONObject map = new JSONObject();

    public Response() {
        super();//spring cloud 序列化必须
//        this.appInfo = ExceptionBuilder.sessionInfo();
    }

    public Response(Integer status, String msg) {
        this.status = status;
        this.msg = msg;
        this.success = status==200;
//        this.status  = DataHelper.parseInt(code);
//        this.appInfo = ExceptionBuilder.sessionInfo();
    }

    public Response(ResponseCode responseCode) {
        this(responseCode.getStatus(), responseCode.getMsg());
    }

    public Response(ResponseCode responseCode, String msg) {
        this(responseCode.getStatus(), msg);
    }

    public Response(Throwable e, ResponseCode responseCode) {
        this(responseCode.getStatus(), responseCode.getMsg());
        setError(e);
    }
    public Response(Throwable e, ResponseCode responseCode, String msg) {
        this(responseCode.getStatus(), msg);
        setError(e);
    }
    public Response(Throwable e, ResponseCode responseCode, String msg, String sql) {
        this(responseCode.getStatus(), msg);
        this.sql=sql;
        setError(e);
    }

    public Response(Throwable e, boolean printStackTrace) {
        this(ResponseCode.SYSTEM_ERROR.getStatus(), e.getMessage());
        this.printStackTrace = printStackTrace;
        setError(e);
    }
    public Response(Throwable e) {
        this(e, true);
    }

    public Response(T data) {
        this(ResponseCode.SUCCESS);
        this.data = data;
        if (data != null) {
            dataType = data.getClass();
            if (data instanceof Collection) {
                this.totalCount = ((Collection) data).size();
            }else {
                this.totalCount = 1;
            }
        } else {
            this.totalCount = 0;
        }
    }

    /**
     *
     * @param data
     * @param totalCount 查询时 为 所有页的行数,  插入 更新 删除时, 为SQL执行成功所影响记录的行数
     */
    public Response(T data, Integer totalCount) {
        this(ResponseCode.SUCCESS);
        this.data = data;
        this.totalCount = totalCount;

        if (data != null) {
            dataType = data.getClass();
            if (data instanceof Collection) {
                this.pageCount = ((Collection) data).size();//当前页的行数
            } else {
                this.totalCount = 1;
            }
        } else {
            this.totalCount = 0;
        }
    }

    public void setError(Throwable e){
        success = false;
        if (status==200) {
            status= Integer.valueOf(ResponseCode.SYSTEM_ERROR.getStatus());
        }
        msg = msg==null?"":msg;

        if (e instanceof TopException){
            TopException ex = (TopException)e;
            this.status= ex.getStatus();
        }

        msg = ExceptionBuilder.substringMessage2(msg);

        this.exception=e.getCause()!=null?e.getCause().getClass().getName():e.getClass().getName();
        this.appInfo = ExceptionBuilder.sessionInfo();
        //logger.error(sessionInfo);
        //logger.error(" ResponseError >>> Exception:{} Method:{}", this.exception, this.method);

        //打印异常信息及处理; 对 Response.method赋值
        ExceptionBuilder.printStackTrace(e, this, printStackTrace);
    }

//    public String toString(){
//        String temp= JsonUtil.toString(this);
//        return "执行结果 "+ (temp.length()>500?temp.substring(0,500):temp);
//    }

    public String toJSONString(){
        return JSON.toJSONString(this);
    }
}