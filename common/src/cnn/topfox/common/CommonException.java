package cnn.topfox.common;

/**
 * 业务用的 抛出的异常类
 */
public class CommonException extends TopException{
    public CommonException(Integer status, String msg) {
        super(status, msg);
    }

    public CommonException(ResponseCode responseCode){
        super(responseCode);
    }

    public CommonException(ResponseCode responseCode, String msg){
        super(responseCode,msg);
    }

    public CommonException(String stackTraceDubbo, Response response) {
        super(stackTraceDubbo, response);
    }

    public static CommonException newInstance2(ResponseCode responseCode){
        ExceptionBuilder exceptionBuilder= create(responseCode.getStatus(),null);//new CommonString(responseCode.getStatus(),null);
        exceptionBuilder.getMsg().append(responseCode.getMsg());
        return new CommonException(responseCode, exceptionBuilder.getMsg().toString());
    }

    public static ExceptionBuilder newInstance(ResponseCode responseCode){
        return create(responseCode.getStatus(), null);
    }
    public static ExceptionBuilder newInstance(Integer responseCode){
        return create(responseCode, null);
    }

    private static ExceptionBuilder create(Integer responseCode, Object target){
        return new ExceptionBuilder(CommonException.class,responseCode, target);
    }



}