package cnn.topfox.common;


import cnn.topfox.annotation.Other;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import cnn.topfox.annotation.Id;
import cnn.topfox.annotation.Ignore;
import cnn.topfox.data.DateTime;
import cnn.topfox.misc.JsonUtil;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.util.*;

@Setter
@Getter
@Accessors(chain = true)
public abstract class AbstractRestSession extends DataDTO {
    private static final long serialVersionUID = 1L;

    @Id private String sessionId;
    @Other @JsonIgnore @Ignore transient public  JSONObject dataSession=null;//token信息对象 redis或数据库中
    @Other @JsonIgnore @Ignore transient private String executeId;           //分布式事务-主事务号
    @Other @JsonIgnore @Ignore transient private String subExecuteId;        //分布式事务-子事务号
    @Other @JsonIgnore @Ignore transient private String routeExecuteIds="";  //分布式事务-事务号调用链路/路径
    @Other @JsonIgnore @Ignore transient private String router="";           //前台页面的路径
    @Other @JsonIgnore @Ignore transient private String requestURI="";       //后端控制层路径

    @Other private String appName=""; //当前的 spring.application.name
    private String phoneOS=""; //手机操作系统
    private String openId =""; //微信id

    private String userId    ="*";
    private String userName  ="*";
    private String userCode  ="";
    private String userMobile="";
    private String userRole  ="";
    private String userRole1="";
    private String userBuildingNo="";

    /** 第一次创建Session 的时间，即登陆系统的时间 */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    Date createDate;

    /** 活动时间，登陆后，任何一次请求都会更新 activeDate activeTimeMillis */
    private String activeDate;
    @JsonIgnore @Ignore transient private long activeTimeMillis;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Other @JsonIgnore @Ignore
    transient private Date nowTime=null;

    @Other @JsonIgnore @Ignore
    transient private LocalDate localDate=LocalDate.now();

    @Other @JsonIgnore @Ignore
    transient private DateTime dateTime = DateTime.now();

    /**
     * 自定义的数据
     * 生命周期是尽在当前线程有效
     */
    @Other @JsonIgnore @Ignore
    transient JSONObject attributes=new JSONObject();

    /**
     * 自定义需要持久化的数据, 可用于保存登录信息
     * 生命周期是贯穿整个RestSession的
     */
    private JSONObject datas=new JSONObject();



    /****************************************
     * @JsonIgnore  //不需要转json的属性用这个 注解
     * @JsonIgnoreProperties(ignoreUnknown = true)
     * ******************************/
    //@JsonIgnore @Ignore transient static public long maxId=0;

    /**
     * 请求地址?后的参数 + 请求头信息 + form-data的数据
     */
    @JsonIgnore @Ignore transient private JSONObject requestData;

    /**
     * 请求头信息
     */
    @JsonIgnore @Ignore transient private JSONObject headers;

    /**
     * 请求 body 数据
     */
    @JsonIgnore @Ignore transient private JSONArray bodyData;

    /**
     * 创建当前对象的线程Id
     */
    @JsonIgnore @Ignore private Long createThreadId;

    /**
     * 用户代理头, 用来判断调用方是什么软件, 可能会是
     *  java 微服务调用        Apache-HttpClient
     *  Postman               PostmanRuntime/7.15.0
     *  macos chrmoe浏览器    Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36
     *  华为手机 微信内置浏览器 Mozilla/5.0 (Linux; Android 9; SEA-AL10 Build/HUAWEISEA-AL10; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/66.0.3359.126 MQQBrowser/6.2 TBS/044705 Mobile Safari/537.36 MMWEBID/4122 MicroMessenger/7.0.5.1440(0x27000537) Process/tools NetType/WIFI Language/zh_CN
     *  window10 chrome浏览器 Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36
     *  window10 ie          Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko
     */
    @JsonIgnore @Ignore transient private String sysUserAgent;

    /**
     * 构造函数
     */
    public AbstractRestSession(){
        requestData = new JSONObject();
        createThreadId = Thread.currentThread().getId();
    }

    @Deprecated
    public void init(){}

    @Override
    public String toString(){
        return JsonUtil.toString(this);
    }

}