package cnn.topfox.common;

import cnn.topfox.misc.Misc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AppContext implements ApplicationContextAware {
	public static String userId;
	/**
	 * 上下文对象实例
	 */
	private static ApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext value) throws BeansException {
		applicationContext = value;
	}

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public static Object getBean(String name){
		return getApplicationContext().getBean(name);
	}

	public static <T> T getBean(Class<T> clazz){
		return getApplicationContext().getBean(clazz);
	}

	public static <T> T getBean(String name,Class<T> clazz){
		return getApplicationContext().getBean(name, clazz);
	}

	private static IRestSessionHandler restSessionHandler;
	public static <T extends IRestSessionHandler> T getRestSessionHandler() {
		// 双重检查锁
		if (restSessionHandler == null ) {
			synchronized(AppContext.class){
				if (restSessionHandler == null && getApplicationContext() != null) {
					restSessionHandler = getApplicationContext().getBean(IRestSessionHandler.class);
				}
			}
		}
		return (T)restSessionHandler;
	}

	public static <T extends AbstractRestSession> T getRestSession() {
		if (getRestSessionHandler()==null){
			return null;
		}
		return (T) getRestSessionHandler().get();
	}

	public static <T extends AbstractRestSession> T getRestSession(Class<T> clazz) {
		if (getRestSessionHandler()==null){
			return null;
		}
		return (T) getRestSessionHandler().get();
	}

	public static SysConfigRead getSysConfig() {
		return getRestSessionHandler()==null?null:getRestSessionHandler().getSysConfig();
	}

	private static Environment environment=null;
	public static void environment(Environment value){
		if (value!=null){
			environment=value;
		}
	}
	public static Environment environment(){
		if (environment==null){
			environment=getRestSessionHandler()==null?null:getRestSessionHandler().environment();
		}
		if (environment==null){
			environment=getBean(Environment.class);
		}

		return environment;
	}

	public static String project(){
		return environment().getProperty("project");
	}
	public static String database(){
		return environment().getProperty("spring.datasource.database");
	}

	public static Environment initStart(long start, ApplicationContext value) {
		applicationContext = value;

		long end = System.currentTimeMillis();

		org.springframework.core.env.Environment environment = environment();

		log.info("------------------------------------------------------------------------");
		log.info("启动完毕 耗时:" +(end - start)/1000+"秒");
		String project=environment.getProperty("project");
		if (Misc.isNotNull(project)){
			log.info("project                   : {}{}", project,"(账套)");
		}
		log.info("server.port               : {}{}", environment.getProperty("server.port")," (服务端口)");
		log.info("spring.profiles.active    : {}", environment.getProperty("spring.profiles.active"));
		log.info("isRunTimer:               {}", environment.getProperty("isRunTimer"));
		//log.info("spring.datasource.driver  : {}", environment.getProperty("spring.datasource.driver-class-name"));

		String database = environment.getProperty("spring.datasource.database");
		database = database!=null?database:environment.getProperty("user.database.name");
		database = database!=null?database:environment.getProperty("spring.datasource.hikari.database");
		if (database != null) {
			int pos = database.indexOf("?");
			log.info("spring.datasource.database: {}", pos > 0 ? database.substring(0, pos) : database + "(连接的数据库名)");
		}


		String url = environment.getProperty("spring.datasource.url");
		url = url!=null?url:environment.getProperty("spring.datasource.jdbc-url");
		url = url!=null?url:environment.getProperty("spring.datasource.hikari.jdbc-url");
		if (url != null) {
			int pos = url.indexOf("?");
			log.info("spring.datasource.url     : {}", pos > 0 ? url.substring(0, pos) : url + " ...");
		}


		String elkRedisKey = environment.getProperty("elk.redis.key");
		if (Misc.isNotNull(elkRedisKey)) {
			log.info("elk.redis.key             : {}", elkRedisKey);
		}

		if (Misc.isNotNull(environment.getProperty("spring.redis.host"))) {
			log.info("spring.redis.host         : {}:{}", environment.getProperty("spring.redis.host"),environment.getProperty("spring.redis.port"));
		}
		if (Misc.isNotNull(environment.getProperty("spring.data.mongodb.uri"))) {
			log.info("spring.data.mongodb.uri   : {}", environment.getProperty("spring.data.mongodb.uri"));
		}

		log.info("------------------------------------------------------------------------");
		String dubbo_application_name = environment.getProperty("dubbo.application.name");
		if (Misc.isNotNull(dubbo_application_name)) {
			log.info("dubbo.application.name : {}", dubbo_application_name);
		}

		String dubboAddress = environment.getProperty("dubbo.registry.address");
		if (Misc.isNotNull(dubboAddress)) {
			log.info("dubbo.registry.address : {}", dubboAddress);
		}
		String dubbo_consumer_check = environment.getProperty("dubbo.consumer.check");
		if (Misc.isNotNull(dubboAddress)) {
			log.info("dubbo.consumer.check   : {}", dubbo_consumer_check);
		}


		String dubbo_protocol_host = environment.getProperty("dubbo.protocol.host");
		if (Misc.isNotNull(dubbo_protocol_host)) {
			log.info("dubbo.protocol.host    : {}", dubbo_protocol_host);
		}
		String dubbo_protocol_port = environment.getProperty("dubbo.protocol.port");
		if (Misc.isNotNull(dubbo_protocol_port)) {
			log.info("dubbo.protocol.port    : {}", dubbo_protocol_port);
		}

		String nacosAddress = environment.getProperty("spring.cloud.nacos.discovery.server-addr");
		if (Misc.isNotNull(nacosAddress)) {
			log.info("spring.cloud.nacos.discovery.server-addr : {}", nacosAddress);
		}

		log.info("jre的目录: " + System.getProperty("java.home"));
		log.info("jdk的规范: " + System.getProperty("java.vm.specification.vendor"));
		log.info("jdk版本号: " + System.getProperty("java.vm.version"));
		log.info("jdk提供商: " + System.getProperty("java.vm.vendor"));
		log.info("OS: " + System.getProperty("os.name"));
		log.info("------------------------------------------------------------------------");

		return environment;
	}
}