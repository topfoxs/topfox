package cnn.topfox.common;

import cnn.topfox.data.DateTime;
import cnn.topfox.misc.DateUtils;

import java.lang.reflect.Method;
import java.util.Date;

public interface IRestSessionHandler<Session extends AbstractRestSession> {

    void initRestSession(Session session, Method method);

    Session create();

    void updateDate(Session restSession);

    Session get() ;

    void dispose();

    SysConfigRead getSysConfig();

    //DataCache getDataCache();

    org.springframework.core.env.Environment environment();
}
