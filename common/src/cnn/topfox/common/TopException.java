package cnn.topfox.common;

import com.alibaba.fastjson2.JSONObject;

/**
 * TopFox框架 专用的 异常类
 */
public class TopException extends RuntimeException{
	private static final long serialVersionUID = 1471447353907832702L;

    private JSONObject appInfo;

    private Integer status;
    private String msg;
    private Response response;
//    private StringBuilder sbMsg;

    public Integer getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    /**
     * 支持Dubbo的更改
     * @return Response
     */
    public Response getResponse() {
        return this.response;
    }

    /**
     * 支持Dubbo的更改
     * @return TopException
     */
    public TopException(String stackTraceDubbo, Response response) {
        super(stackTraceDubbo);
        //super(throwable);
        this.status = response.status();
        this.msg = response.msg();
        this.response = response;
    }

    public TopException(Integer status, String msg) {
        super(msg);
        this.status = status;
        this.msg = msg;
        this.appInfo = getSessionInfo();
    }

    public TopException(ResponseCode responseCode){
        this(responseCode.getStatus(),responseCode.getMsg());
    }

    public TopException(ResponseCode responseCode, String msg){
        this(responseCode.getStatus(),msg);
    }

    public static TopException newInstance2(ResponseCode responseCode){
        ExceptionBuilder exceptionBuilder= create(responseCode.getStatus(),null);//new ExceptionBuilder(responseCode.getStatus(),null);
        exceptionBuilder.getMsg().append(responseCode.getMsg());
        return new TopException(responseCode, exceptionBuilder.getMsg().toString());
    }

    public static ExceptionBuilder newInstance(ResponseCode responseCode){
        return create(responseCode.getStatus(), null);
    }

    public static ExceptionBuilder newInstance(Integer responseCode){
        return create(responseCode, null);
    }
    private static ExceptionBuilder create(Integer responseCode, Object target){
        return new ExceptionBuilder(TopException.class, responseCode, target);
    }


    public JSONObject getSessionInfo(){
        JSONObject info = ExceptionBuilder.sessionInfo();
        info.put("class", getClass().getName());
        return info;
    }
}