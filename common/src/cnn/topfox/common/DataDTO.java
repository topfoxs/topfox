package cnn.topfox.common;

import com.alibaba.fastjson2.JSONObject;
import com.fasterxml.jackson.annotation.JsonIgnore;
import cnn.topfox.annotation.Other;
import cnn.topfox.annotation.State;
import cnn.topfox.annotation.TableField;
import cnn.topfox.annotation.Ignore;
import cnn.topfox.data.DbState;
import cnn.topfox.misc.BeanUtil;
import cnn.topfox.misc.JsonUtil;
import cnn.topfox.misc.Misc;
import cnn.topfox.data.DataHelper;
import cnn.topfox.data.Field;
import cnn.topfox.data.TableInfo;

import java.util.*;

public class DataDTO extends DataPojo {
    /**
     * update时 是否执行 beforeInsertOrUpdate afterInsertOrUpdate
     */
    @Other @JsonIgnore @Ignore
    transient public Boolean isBeforeOrAfterUpdate;
    public Boolean isBeforeOrAfterUpdate(){
        String state=dataState();
        if (DbState.INSERT.equals(state) || DbState.DELETE.equals(state) || DbState.NONE.equals(state) ){
            return true;
        }
//        return (DbState.UPDATE.equals(dataState()) && this.isBeforeOrAfterUpdate);
        return (DbState.UPDATE.equals(dataState()) && (this.isBeforeOrAfterUpdate==null || this.isBeforeOrAfterUpdate));

    }

    @Other @JsonIgnore @Ignore
    transient private IBean originDTO;

    @Other @JsonIgnore @Ignore
    transient private IBean megerDTO;

    /**
     * 前台提交修改的数据
     */
    @Other @JsonIgnore @Ignore
    transient private JSONObject mapModify;

    @Other @JsonIgnore @Ignore
    transient private Map<String, Object> mapSave;

    /**
     * 指定要更新为 null的字段
     */
    @Other @JsonIgnore @Ignore
    transient private List<String> nullFields;
    public DataDTO addNullFields(String... nullfields) {
        initFields(nullFields(),nullfields);
        return this;
    }

    /**
     * 获得更新为空的只读.  不能改
     * @return
     */
    public List<String> nullFields() {
        if (this.nullFields==null){
            this.nullFields=new ArrayList();
        }
        return this.nullFields;
    }
    /**
     * 不管值 有没有变化, 都要更新 指定的字段
     */
    @Other @JsonIgnore @Ignore
    transient private List<String> updateFields;
    public DataDTO addUpdateFields(String... fields) {
        initFields(updateFields(), fields);
        return this;
    }
    public List<String> updateFields() {
        if (this.updateFields==null){
            this.updateFields=new ArrayList();
        }
        return this.updateFields;
    }

    private void initFields(List<String> listFields, String... fields){
        for (String keys : fields){
            for (String key : Misc.string2Array(keys,",")) {
                if (!listFields.contains(key)){
                    listFields.add(key);
                }
            }
        }
    }

    /**
     * set 一个原始没有修改的DTO
     * @param originDTO
     */
    public IBean addOrigin(IBean originDTO) {
        this.originDTO = originDTO;
        //isChangeUpdateSql=true;
        return this;
    }

    @Other @JsonIgnore @Ignore
    transient private Boolean isChangeUpdateSql;
    public Boolean isChangeUpdateSql(){
        return this.isChangeUpdateSql;
    }
    public void isChangeUpdateSql(Boolean value){
        this.isChangeUpdateSql = value;
    }
    /**
     * 将DTO的当前值Copy到原始值
     * top.updateMode 大于1 时, 后台直接查询出来的DTO做更新,需要根据变化值生成更新SQL时调用本方法
     * @return
     */
    public DataDTO addOriginFromCurrent() {
        this.originDTO = BeanUtil.cloneBean(this);
        isChangeUpdateSql=true;
        return this;
    }

    /**
     * 获得更新前的原始数据
     */
    public <T extends DataDTO> T origin(){
        return (T)this.originDTO;
    }


    /**
     * 前台提交修改的数据
     */
    public JSONObject mapModify() {
        if (this.mapModify==null){
            this.mapModify=new JSONObject();
        }
        return this.mapModify;
    }
    public DataDTO addModifyMap(JSONObject mapModify) {
        this.mapModify = mapModify;
        return this;
    }

//    public <T> T dtoModify(){
//        return (T)dtoModify(getClass());
//    }

//    /**
//     * 调用本方法将 创建一个新的DTO
//     * 当 SysConfig.getUpdateMode==1, 应该是 自己, 本逻辑需要开发者自己实现
//     *
//     * @param clazz
//     * @param <T>
//     * @return
//     */
//    public <T> T dtoModify(Class<T> clazz){
//        T modifyDTO = BeanUtil.map2Bean(mapModify, clazz);
//        return modifyDTO;
//    }

    /**
     * 更新时,  获得 原始数据+修改数据的合并数据
     */
    public <T extends DataDTO> T merge(){
        if (DbState.INSERT.equals(dataState()) || DbState.DELETE.equals(dataState())){
            throw TopException.newInstance(ResponseCode.NULLException).text("新增或删除时,不存在合并的DTO");
        }
        if (origin() == null){
            throw TopException.newInstance(ResponseCode.NULLException).text("DTO的原始数据还没有设值, 不能获得合并的DTO");
        }

        //参数配置为 合并DTO, 就是 当前自己
        if (1==2){
            return (T)this;
        }

        try {
            if (megerDTO == null) {
                megerDTO = getClass().newInstance();
            }
            //拷贝原始数据
            BeanUtil.copyBean(origin(), megerDTO);
            //拷贝当前数据
            BeanUtil.copyBean(this, megerDTO);
            return (T) megerDTO;
        } catch (TopException ce){
            throw ce;
        } catch (Exception e) {
            throw TopException.newInstance(ResponseCode.ERROR).text("合并DTO时 new DTO出错");
        }
    }

    /**
     * 获得实体版本号
     * @return
     */
    public Integer dataVersion(){
        Field field = tableInfo().getField(tableInfo().getVersionFieldName());
        if (field == null) {
            return null;
        }
        Object value = BeanUtil.getValue(this, field);
        if (value == null) {
            return null;
        }
        return DataHelper.parseInt(value);
    }
    public DataDTO dataVersion(Integer value){
        Field field = tableInfo().getField(tableInfo().getVersionFieldName());
        if (field != null) {
            BeanUtil.setValue(tableInfo(), this, field.getName(), value);
        }
        return this;
    }

    /**
     * 获得实体行号(没有Id时增行管用)
     * @return
     */
    public String dataRowId(){
        String fieldName = tableInfo().getRowNoFieldName();
        if (Misc.isNull(fieldName)) {
            return null;
        }
        return DataHelper.parseString(BeanUtil.getValue(tableInfo(),this, fieldName));
    }
    public DataDTO dataRowId(String value){
        String fieldName = tableInfo().getRowNoFieldName();
        if (Misc.isNotNull(fieldName)) {
            BeanUtil.setValue(tableInfo(), this, fieldName, value);
        }
        return this;
    }

    /**
     *数据状态字段  数据库不存在的字段, 用于描述   transient修饰符 表示 改字段不会被序列化
     * @see DbState ;
     */
    @Other @JsonIgnore @Ignore transient String dataState;
    public String dataState(){
        String fieldName = tableInfo().getStateFieldName();
        if (Misc.isNull(fieldName)) {
            return dataState;
        }
        return DataHelper.parseString(BeanUtil.getValue(tableInfo(),this, fieldName));
    }
    public DataDTO dataState(String value){
        String fieldName = tableInfo().getStateFieldName();
        if (Misc.isNull(fieldName)) {
            this.dataState = value;
        }else{
            BeanUtil.setValue(tableInfo(), this, fieldName, value);
        }
        return this;
    }

    private Field findField(String fieldName){
        Field field = tableInfo().getField(fieldName);
        if (field == null ) {
            throw TopException.newInstance(ResponseCode.SYS_FIELD_ISNOT_EXIST).text("找不到DTO的字段:",fieldName);
        }
        return field;
    }

    public DataDTO setValue(String fieldName, Object value){
        Field field = findField(fieldName);
        addUpdateFields(fieldName);//luojp 2021-05-26 调用这个方法赋值的字段, 强制更新.
        BeanUtil.setValue(tableInfo(), this, field, value);
        return this;
    }


    public Object getValue(String fieldName){
        return BeanUtil.getValue(tableInfo(), this, findField(fieldName));
    }


    /**
     * 判断某个字段 是否有修改
     * @param fieldName
     * @return
     */
    public boolean checkChange(String fieldName) {
        return checkChange(fieldName, null, null);
    }
    private boolean checkChange(String fieldName, DataObject current, DataObject origin){
        Field field = tableInfo().getField(fieldName);
        if (field == null) {
            throw TopException.newInstance(ResponseCode.DATA_IS_INVALID).text(fieldName+"不存在");
        }

        Object currentValue = BeanUtil.getValue(tableInfo(), this, field);
        Object originValue = null;
        if (DbState.UPDATE.equals(dataState())) {
            if (origin() ==null) {
                //logger.error("dto更新checkChange()时, {}.origin()==null, 只会更新dto不为null字段的值, 且不能获得修改日志", getClass().getName());
            }else{
                originValue = BeanUtil.getValue(tableInfo(), origin(), field);
            }
        }

        boolean result=true;
        if (DbState.INSERT.equals(dataState()) || DbState.DELETE.equals(dataState())){

        }else if (currentValue == null && originValue != null
                || currentValue != null && originValue == null){

        }else if (currentValue == null && originValue == null){
            result = false;
        }else if (currentValue == null && originValue == null ||
                currentValue.toString().equals(originValue.toString())){
            result = false;
        }

        if (current!=null) {current.setValue(currentValue);}
        if (origin!=null) {origin.setValue(originValue);}

        return result;
    }
    /**
     * 获取某一个字段的修改日志用
     * 可以获得 新旧值
     * @param fieldName
     * @return
     */
    public ChangeData newChangeData(String fieldName, FormatConfig formatConfig){
        DataObject current = DataObject.newInstance();
        DataObject origin  = DataObject.newInstance();
        boolean result = checkChange(fieldName, current,  origin);
        ChangeData changeData = new ChangeData(result, current,  origin, TableInfo.get(getClass()).getField(fieldName));
        changeData.setFormatConfig(formatConfig);
        return changeData;
    }

    public ChangeData newChangeData(String fieldName){
        return newChangeData(fieldName, null);
    }

    /**
     * 获得所有修改的字段, 按照DTO的字段顺序
     * @return
     */
    public Map<String, ChangeData> changeDataForMap(){
        Map<String, Object> mapChange = BeanUtil.getChangeRowData(this);
        Map<String, ChangeData> mapChangeData = new LinkedHashMap<>(mapChange.size());
        mapChange.forEach((key, current)->{
            mapChangeData.put(key, newChangeData(key));
        });
        return mapChangeData;
    }

    /**
     * 返回调用方(如前端)的数据
     * @return
     */
    public Map<String, Object> mapSave(){
        return this.mapSave;
    }

    /**
     * 本方法 生成更新SQL时自动调用
     * @param mapSave
     * @return 设置调用方(如前端)的数据
     */
    public Map<String, Object> mapSave(Map<String, Object> mapSave){
        this.mapSave = mapSave;
        return this.mapSave;
    }


    /**
     * 始终输出到前台的数据
     */
    @Other @JsonIgnore @Ignore
    transient private Map<String, Object> mapAlwaysOutData;
    public Map<String, Object> mapAlwaysOutData(){
        if ( this.mapAlwaysOutData==null){
            this.mapAlwaysOutData = new HashMap<>();
        }
        return this.mapAlwaysOutData;
    }



    /**
     * 所有字段设置为 null
     */
    public DataDTO clearAllData(){
        for (java.lang.reflect.Field field : tableInfo().getJavaFields() ){
            try {
                field.setAccessible(true);
                if (tableInfo().getFields().containsKey(field.getName()) && field.get(this) != null) {
                    field.set(this, null);
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
        return this;
    }

//    @Override
//    public String toString(){
//        return JsonUtil.toString(this);
//    }
}