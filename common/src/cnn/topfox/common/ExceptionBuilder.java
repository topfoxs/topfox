package cnn.topfox.common;

import com.alibaba.fastjson2.JSONObject;
import cnn.topfox.misc.Misc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public final class ExceptionBuilder {
    private Integer status;
    private StringBuilder sbMsg;
//    private JSONObject appInfo;
    private Class clazz;

    public ExceptionBuilder(Class clazz, Integer status, Object target){
        this.clazz = clazz;
        this.status = status;
        this.sbMsg = new StringBuilder();


//        IRestSessionHandler restSessionHandler = AppContext.getRestSessionHandler();
//        AbstractRestSession restSession = null;
//        if (restSessionHandler != null) {
//            restSession = restSessionHandler.get();
//        }
//
//
//        if (restSession != null) {
//            this.sbMsg.append("[");
//            if (Misc.isNotNull(restSession.getAppName())){
//                this.sbMsg.append("appcationName:").append(restSession.getAppName());
//            }
//            String sessionId =restSession.getSessionId();
//            String executeId =restSession.getExecuteId();
//            String userId    =restSession.getSysUserId();
//            String userCode  =restSession.getSysUserCode();
//            String userName  =restSession.getSysUserName();
//            String userMobile=restSession.getSysUserMobile();
//
//            if (sessionId!=null && sessionId.length()>2) {
//                if (this.sbMsg.length()>1) {this.sbMsg.append(", ");}
//                this.sbMsg.append("sessionId:").append(sessionId);
//            }
//            if (executeId!=null && executeId.length()>2) {
//                if (this.sbMsg.length()>1) {this.sbMsg.append(", ");}
//                this.sbMsg.append("executeId:").append(executeId);
//            }
//            if (userId!=null    && userId.length()>2   ) {this.sbMsg.append(", userId:").append(userId);}
//            if (userCode!=null  && userCode.length()>2 ) {this.sbMsg.append(", userCode:").append(userCode);}
//            if (userName!=null  && userName.length()>2 ) {this.sbMsg.append(", userName:").append(userName);}
//            if (userMobile!=null&& userMobile.length()>2){this.sbMsg.append(", userMobile:").append(userMobile);}
//        }
//        if(target !=null ) {
//            this.sbMsg.append(", class:").append(target.getClass().getName());
//        }
//        this.sbMsg.append("]\n\r");

    }

    public TopException text(Object... values){
        if (values==null){
            this.sbMsg.append("null");
        }
        String tempMsg;
        for (Object value : values) {
            value = value==null?"null":value;
            tempMsg=substringMessage(value.toString());
            this.sbMsg.append(tempMsg);
        }

        if (TopException.class.getName().equals(clazz.getName())) {
            return new TopException(status, this.sbMsg.toString());
        }else{
            return new CommonException(status, this.sbMsg.toString());
        }
    }

    public StringBuilder getMsg(){
        return this.sbMsg;
    }
    static String exceptionExcludeTrace=null;
    static List<String> list;
    public static boolean findExcludeTrace(StackTraceElement ste) {
        if (log.isDebugEnabled() || exceptionExcludeTrace==null) {
            exceptionExcludeTrace = AppContext.environment().getProperty("top.exception.exclude-trace");
            if (exceptionExcludeTrace == null) {
                exceptionExcludeTrace = "";
            }
            list=Misc.string2Array(exceptionExcludeTrace,",");
        }

        boolean ret=false;

        if (list!=null) {
            for (String findStr : list) {
                ret = ste.getClassName().contains(findStr);
                if (ret == true) {
                    break;
                }
            }
        }
        return ret;
    }

    public static void printStackTrace(Throwable e, Response response) {
        printStackTrace(e, response, true);
    }

    public static void printStackTrace(Throwable e, Response response, boolean printStackTrace){
        List<StackTraceElement> list = new ArrayList();
        String method=null;
        Throwable out;

        if (e instanceof TopException) {
            out=e;
        }else{
            //异常处理优化
            out = TopException.newInstance(ResponseCode.ERROR).text(response.msg());
            StackTraceElement[] steTemp1=out.getStackTrace();
            for (int i=0;i<steTemp1.length;i++){
                if (i>5){break;}
                if (steTemp1[i].getClassName().contains("com.topfox")){
                    list.add(steTemp1[i]);
                }
            }
        }

        StackTraceElement[] steTemp2 = e.getStackTrace();
        for (StackTraceElement ste:steTemp2){
            if (findExcludeTrace(ste) ){
                continue;
            }

            list.add(ste);
            if (StringUtils.isEmpty(method)){
                method = findCauseElement(ste);
            }
        }

        out.setStackTrace(list.toArray(new StackTraceElement[0]));
        if (printStackTrace) {
            out.printStackTrace();
        }

        response.method(method);
    }

    /**
     * 原始异常的  tackTrace 太多, 过略掉一些不需要的
     * @param e
     */
    public static void fiterStackTrace(Throwable e) {
        List<StackTraceElement> list = new ArrayList();
        StackTraceElement[] steTemp2 = e.getStackTrace();
        for (StackTraceElement ste:steTemp2){
            if (findExcludeTrace(ste) ){
                continue;
            }
            list.add(ste);
        }
        e.setStackTrace(list.toArray(new StackTraceElement[0]));
    }

    public static String findCauseElement(StackTraceElement ste){
        if (ste.getClassName().startsWith("org.")
                || ste.getClassName().startsWith("java")
                || ste.getClassName().startsWith("com.topfox.common.ExceptionBuilder")
                || ste.getClassName().startsWith("com.topfox.sql.Condition") && "endWhere".equals(ste.getMethodName())
                || ste.getClassName().startsWith("com.sun")
                || ste.getClassName().startsWith("feign.")
        ) {
            //异常抛出层次优化
            return null;
        }

        StringBuilder sb=new StringBuilder();
        sb.append(ste.getClassName())
                .append(".")
                .append(ste.getMethodName())
                .append(":")
                .append(ste.getLineNumber());
        return sb.toString();
    }

    public static String substringMessage(String message) {
        int pos = message.lastIndexOf("]\n\r");
        if (pos > 0 ) {
            message = message.substring(pos + 3);
        }
        return message;
    }

    public static String substringMessage2(String message) {
        message = substringMessage(message);

        //以下为 Dubbo异常字符串处理
        int pos2 = message.indexOf("\n\tat");
        if (pos2 > 0 ) {
            message = message.substring(0, pos2);
        }

        return message;
    }


    public static JSONObject sessionInfo(){
        JSONObject info = new JSONObject();
        IRestSessionHandler restSessionHandler = AppContext.getRestSessionHandler();
        AbstractRestSession restSession = null;
        if (restSessionHandler != null) {
            restSession = restSessionHandler.get();
        }

        if (restSession != null) {
            info.put("appName", restSession.getAppName());
            info.put("sessionId", restSession.getSessionId());
            info.put("executeId", restSession.getSessionId());
            info.put("userId", restSession.getUserId());
            info.put("userCode", restSession.getUserCode());
            info.put("userName", restSession.getUserName());
            info.put("userMobile", restSession.getUserMobile());

        }
        return info;
    }

//    public static JSONObject getSessionInfo(Object target){
//        JSONObject info = new JSONObject(true);
//        IRestSessionHandler restSessionHandler = AppContext.getRestSessionHandler();
//        AbstractRestSession restSession = null;
//        if (restSessionHandler != null) {
//            restSession = restSessionHandler.get();
//        }
//
//        if (restSession != null) {
//            info.put("appName", restSession.getAppName());
//            info.put("sessionId", restSession.getSessionId());
//            info.put("executeId", restSession.getSessionId());
//            info.put("userId", restSession.getUserId());
//            info.put("userCode", restSession.getUserCode());
//            info.put("userName", restSession.getUserName());
//            info.put("userMobile", restSession.getUserMobile());
//
//        }
//        if (target != null)
//            info.put("class", target.getClass().getName());
//
//        return info;
//    }
}