package cnn.topfox.common;

public enum ResponseCode {

    SUCCESS(200, "请求成功"),

    /** system exception */
    SYSTEM_ERROR(500, "系统异常"),
    ERROR(500, "发生异常"),

    /** param_key is inexistence */
    PARAM_IS_INEXISTENCE(400, "参数不存在"),

    /** param_value is null */
    PARAM_IS_NULL(400, "参数值为空"),

    /** param invalid */
    PARAM_IS_INVALID(400, "参数无效"),

    /** user_token is null/invalid */
    TOKEN_ERROR441 (441, "用户未验证, 请重新登录"),
    TOKEN_ERROR442(442, "token丢失, 请重新登录"),


    JSON_TO_OBJECT(30020, "JSON字符串转对象报错"),
    OBJECT_TO_JSON(30022, "对象转JSON字符串报错"),
    STRING_TO_JSONMAP(30022, "字符串转JSON MAP报错"),
    DATA_IS_INVALID(30024,"数据无效"),
    DATA_KEY_ISNULL(30025, "主键字段的值不能为空"),
    DATA_IS_NULL(30026,"数据不能空白"),     //CommonException
    DATA_IS_TOO_LONG(30027,"数据过长"),
    DATA_IS_DUPLICATE(30028,"数据不能重复"),//CommonException

    SYS_KEY_FIELD_DATATYPE_ISINVALID(30029,"主键字段类型不对"),//luojp add SYS0001
    SYS_FIELD_ISNOT_EXIST(41000, "字段不存在"),//luojp add
    SYS_OPEN_REDIS(41001, "需要开启redis"),//luojp add
    SYS_PK_FIELD_NOT_EXIST(41002,"找不到主键字段"),//luojp add
    SYS_ARRAY_INDEX_OUT(41003,"数组索引越界"),


    /**
     * dbsql exception error
     */
    DB_NOT_EXIST(404,"查无记录"),
    DB_SELECT_ERROR(401, "数据库查询错误"),
    DB_INSERT_ERROR(402, "数据库插入出错"),
    DB_UPDATE_ERROR(403, "数据库更新出错"),
    DB_UPDATE_VERSION_ISNOT_NEW(405, "更新时,传入的版本号不是最新的"),//luojp add
    DB_UPDATE_FIND_NO_DATA(406, "更新的记录为0, 依据更新条件查无记录"),//luojp add

    DB_DELETE_ERROR(410, "数据库删除出错"),
    DB_DELETE_FOREIGN_FAILS(411, "删除的数据已经被引用"),
    DB_DELETE_FIND_NO_DATA(412, "依据删除条件查无记录, 即删除的数据已经不存在"),//luojp add
    DB_SQL_ERROR(413, "SQL语法错误"),
    DB_UNKNOWN_COLUMN(414, "字段不存在"),
    DB_UNKNOWN_TABLE(415, "表不存在"),

    /**
     * redis_lock exception error
     */
    ERROR_LOCK_KEY_IS_NULL(60002,"获取锁，key不能为空"),
    ERROR_LOCK_TAKEN(60004,"获取锁失败，已经被其他线程获取"),
    ERROR_UNLOCK_KEY_IS_NULL(60006,"解锁，key不能为空"),
    ERROR_LOCK_EXCEPTION(60008,"操作redis锁异常,请检查redis服务"),

    /**
     * topfox exception error
     */
    EX_RUNTIME_ERROR(90000, "RuntimeException异常"),
    EX_INTERRUPTED_ERROR(90010, "InterruptedException异常"),
    EX_IO_ERROR(90020, "IOException异常"),
    EX_ERROR(90030, "Exception异常"),
    NULLException(90050, "空指针异常"),

    /**
     * cloud hystrix exception error, 80000开头让给网关相关等
     */
    HYSTRIX_CODE(80000, "服务器繁忙, 请稍后再试"),
    HYSTRIX_GATEWAY(80002, "服务器繁忙, 请稍后再试"),
    HYSTRIX_SERVICE(80004, "服务器繁忙, 请稍后再试");

    private Integer status;
    private String msg;

    public Integer getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    ResponseCode(Integer status, String msg) {
        this.status = status;
        this.msg = msg;
    }
}
