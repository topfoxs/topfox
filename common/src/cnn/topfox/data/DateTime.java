package cnn.topfox.data;


import cnn.topfox.common.ResponseCode;
import cnn.topfox.misc.Misc;
import com.fasterxml.jackson.annotation.JsonIgnore;
import cnn.topfox.annotation.Ignore;
import cnn.topfox.common.CommonException;
import cnn.topfox.misc.DateUtils;

import java.beans.Transient;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalField;
import java.time.temporal.TemporalUnit;
import java.util.Date;

/**
 * 仅支持一下两种格式:
 * 第一种: yyyy-MM-dd HH:mm
 * 第二种: yyyy-MM-dd HH:mm:ss
 */
public class DateTime implements java.io.Serializable,Cloneable, Comparable<DateTime> {
    private static final long serialVersionUID = 210530001;

//    private String valueString=null;

    @JsonIgnore
    private LocalDateTime localDateTime;


    //实例函数/////////////////////////////////////////////////////////////////////////////////////////////////////
    public static DateTime now(){
        return new DateTime(LocalDateTime.now());
    }
    public static DateTime newInstance(LocalDateTime value){
        return new DateTime(value);
    }
    public static DateTime of(int year, int month, int dayOfMonth) {
        return new DateTime(LocalDateTime.of(year, month, dayOfMonth, 0, 0, 0,0) ) ;
    }
    public static DateTime of(int year, int month, int dayOfMonth, int hour) {
        return new DateTime(LocalDateTime.of(year, month, dayOfMonth, hour, 0) ) ;
    }
    public static DateTime of(int year, int month, int dayOfMonth, int hour, int minute) {
        return new DateTime(LocalDateTime.of(year, month, dayOfMonth, hour, minute) ) ;
    }
    public static DateTime of(int year, int month, int dayOfMonth, int hour, int minute, int second) {
        return new DateTime(LocalDateTime.of(year, month, dayOfMonth, hour, minute, second) ) ;
    }
    public static DateTime of(LocalDate loaldate, LocalTime localTime) {
        return new DateTime(LocalDateTime.of(loaldate, localTime) ) ;
    }
    public static DateTime of(LocalDate loaldate) {
        return new DateTime(loaldate);
    }
    public static DateTime of(Date date) {
        return new DateTime(date);
    }
    public static DateTime parse(String datetime) {
        return new DateTime(DataHelper.parseLocalDateTime(datetime)) ;
    }
    public static DateTime parse(String datetime, String format) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format);
        return new DateTime(LocalDateTime.parse(datetime, dtf)) ;
    }
    public static DateTime parse(long millis) {
        return new DateTime(LocalDateTime.ofInstant(Instant.ofEpochMilli(millis),ZoneId.of("Asia/Shanghai"))) ;
    }




    //构造函数
    public DateTime(){
        this.localDateTime = LocalDateTime.now();
    }



    public DateTime(Date date){
        LocalDate     localDate = LocalDate.ofInstant(date.toInstant(), ZoneId.systemDefault());
        LocalTime     localTime = LocalTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        this.localDateTime = LocalDateTime.of(localDate, localTime);
    }

    public DateTime(LocalDate loaldate){
        this.localDateTime = LocalDateTime.of(loaldate, LocalTime.of(0,0,0,0));
    }
    public DateTime(LocalDateTime value){
        this.localDateTime = value;
    }

    public DateTime(String dateString){
        if (Misc.isNull(dateString)){
            this.localDateTime=null;
        }else{
            dateString=dateString.trim();
            if (dateString.indexOf("T")==10){
                //2023-11-11T08:00 改为 2023-11-11 08:00
                dateString=dateString.replace("T"," ");
            }
            if (dateString.length()==10){
                dateString=dateString+" 00:00:00";
            }

            if (dateString.length()==16){
                this.localDateTime = LocalDateTime.parse(dateString, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
            }else if (dateString.length()==19){
                this.localDateTime = LocalDateTime.parse(dateString, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            }else if (dateString.length()>19){
                this.localDateTime = LocalDateTime.parse(dateString.substring(0,19), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            }else{
                throw CommonException.newInstance2(ResponseCode.PARAM_IS_INVALID);
            }
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////





    //类型转换函数
    @JsonIgnore @Transient
    public Date toDate(){
        return DateUtils.toDate(this.localDateTime);
    }
    @JsonIgnore @Transient
    public LocalDate toLocalDate(){
        return this.localDateTime==null?null:this.localDateTime.toLocalDate();
    }
    @JsonIgnore @Transient
    public LocalDateTime toLocalDateTime(){
        return this.localDateTime;
    }
    @JsonIgnore @Transient
    public LocalTime toLocalTime(){
        return this.localDateTime==null?null:this.localDateTime.toLocalTime();
    }


    public long toEpochSecond(){
        return toEpochSecond(ZoneOffset.of("+8"));
    }
    public long toEpochSecond(ZoneOffset zoneOffset){
        return localDateTime.toEpochSecond(zoneOffset);
    }
    public long get(TemporalField field){
        return localDateTime.get(field);
    }
    public long getLong(TemporalField field){
        return localDateTime.get(field);
    }
    public int getDayOfYear(){//当年中第几天
        return localDateTime.getDayOfYear();
    }
    public int getYear(){//年
        return localDateTime.getYear();
    }
    public Month getMonth(){//月
        return localDateTime.getMonth();
    }
    public int getMonthValue(){//月
        return localDateTime.getMonthValue();
    }
    public int getDayOfMonth(){//日
        return localDateTime.getDayOfYear();
    }

    public int getHour(){//小时
        return localDateTime.getHour();
    }
    public int getMinute(){//分钟
        return localDateTime.getMinute();
    }
    public int getSecond(){//秒
        return localDateTime.getSecond();
    }

    public DateTime plus(long amountToAdd, TemporalUnit unit){
        return DateTime.newInstance(localDateTime.plus(amountToAdd, unit));
    }
    public DateTime plusYears(long years){
        return DateTime.newInstance(localDateTime.plusYears(years));
    }
    public DateTime plusMonths(long months){
        return DateTime.newInstance(localDateTime.plusMonths(months));
    }
    public DateTime plusDays(long days){
        return DateTime.newInstance(localDateTime.plusDays(days));
    }
    public DateTime plusHours(long hours){
        return DateTime.newInstance(localDateTime.plusHours(hours));
    }
    public DateTime plusMinutes(long minutes){
        return DateTime.newInstance(localDateTime.plusMinutes(minutes));
    }
    public DateTime plusSeconds(long seconds){
        return DateTime.newInstance(localDateTime.plusSeconds(seconds));
    }


    public DateTime withYear(int year){
        return DateTime.newInstance(localDateTime.withYear(year));
    }
    public DateTime withMonth(int month){
        return DateTime.newInstance(localDateTime.withMonth(month));
    }
    public DateTime withDayOfMonth(int dayOfMonth){
        return DateTime.newInstance(localDateTime.withDayOfMonth(dayOfMonth));
    }
    public DateTime withDayOfYear(int dayOfYear){
        return DateTime.newInstance(localDateTime.withDayOfYear(dayOfYear));
    }
    public DateTime withHour(int hour){
        return DateTime.newInstance(localDateTime.withHour(hour));
    }
    public DateTime withMinute(int minute){
        return DateTime.newInstance(localDateTime.withMinute(minute));
    }
    public DateTime withSecond(int second){
        return DateTime.newInstance(localDateTime.withSecond(second));
    }

    public DayOfWeek getDayOfWeek(){
        return localDateTime.getDayOfWeek();
    }
    /**
     * 星期
     * @return 1234560
     */
    public int getWeekValue(){
        DayOfWeek dayOfWeek=localDateTime.getDayOfWeek();
        if      (dayOfWeek==DayOfWeek.MONDAY)    return 1;
        else if (dayOfWeek==DayOfWeek.TUESDAY)   return 2;
        else if (dayOfWeek==DayOfWeek.WEDNESDAY) return 3;
        else if (dayOfWeek==DayOfWeek.THURSDAY)  return 4;
        else if (dayOfWeek==DayOfWeek.FRIDAY)    return 5;
        else if (dayOfWeek==DayOfWeek.SATURDAY)  return 6;

        return 0;//周日
    }
    public String getWeek1(){
        DayOfWeek dayOfWeek=localDateTime.getDayOfWeek();
        if      (dayOfWeek==DayOfWeek.MONDAY)    return "周一";
        else if (dayOfWeek==DayOfWeek.TUESDAY)   return "周二";
        else if (dayOfWeek==DayOfWeek.WEDNESDAY) return "周三";
        else if (dayOfWeek==DayOfWeek.THURSDAY)  return "周四";
        else if (dayOfWeek==DayOfWeek.FRIDAY)    return "周伍";
        else if (dayOfWeek==DayOfWeek.SATURDAY)  return "周六";

        return "周日";
    }
    public String getWeek2(){
        DayOfWeek dayOfWeek=localDateTime.getDayOfWeek();
        if      (dayOfWeek==DayOfWeek.MONDAY)    return "星期一";
        else if (dayOfWeek==DayOfWeek.TUESDAY)   return "星期二";
        else if (dayOfWeek==DayOfWeek.WEDNESDAY) return "星期三";
        else if (dayOfWeek==DayOfWeek.THURSDAY)  return "星期四";
        else if (dayOfWeek==DayOfWeek.FRIDAY)    return "星期伍";
        else if (dayOfWeek==DayOfWeek.SATURDAY)  return "星期六";

        return "星期日";
    }




    public String valueString() {
        if (this.localDateTime==null){
            return null;
        }
        String dateTimeString=this.localDateTime.toString();
        dateTimeString=dateTimeString.replaceAll("T"," ");
        if (dateTimeString.length()>19) {
            return dateTimeString.substring(0, 19);
        }else{
            return dateTimeString;
        }
    }


    public String format(){
        return valueString();
    }
    public String format(String format){
        if (Misc.isNull(format)) {
            return valueString();
        }
        if (this.localDateTime==null) {
            return null;
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return formatter.format(this.localDateTime);
    }

    /**
     *
     * @return 默认数据格式: yyyy-MM-dd HH:mm:ss
     */
    @Override
    public String toString() {
        if (this.localDateTime==null) return null;

        String dateTimeString=this.localDateTime.toString();
        dateTimeString=dateTimeString.replaceAll("T"," ");
        if (dateTimeString.length()>19) {
            return dateTimeString.substring(0, 19);
        }else if (dateTimeString.length()==16) {
            return dateTimeString+":00";
        }else{
            return dateTimeString ;
        }
    }

    @Override
    public int compareTo(DateTime dateTime) {
        return this.localDateTime.compareTo(dateTime.localDateTime);
    }

}

