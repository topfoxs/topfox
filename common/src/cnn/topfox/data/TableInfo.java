package cnn.topfox.data;

import cnn.topfox.annotation.*;
import cnn.topfox.common.*;
import cnn.topfox.misc.CamelHelper;
import cnn.topfox.misc.Misc;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.MethodDescriptor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/**\
 * 本对象目的是 解析实体的结构
 */
public class TableInfo extends ConfigHandler {
    public Class<?> clazzEntity;

    Table table;
    private String tableName;           //数据库库表名
    private String tableCnName;         //注解的中文标名
    private Field  idField;             //主键字段
    private String redisKey;            //缓存到redis数据库的表名, 开发者在DTO中自定义

    private String createDateFieldName; //创建时间 字段名
    private String createUserFieldName; //创建人   字段名
    private String updateDateFieldName; //修改时间 字段名
    private String updateUserFieldName; //修改人   字段名

    private String versionFieldName;    //版本号字段
    private String stateFieldName;      //不在数据库
    private String rowNoFieldName;      //不在数据库

    //复杂对象类型的字段, 如dto, map, list set .
    //即非基本类型的字段. 基本类型为 String Long Integer Long Doulbe Date LocalTime LocalDate LocalDateTime 等为基本类型
    private Map<String, java.lang.reflect.Field> fieldsObjectDataType;

    private List<java.lang.reflect.Field> listJavafield; //获得所有层次类的Field
    private Map<String, Field> fields;                   //所有在数据库的字段集合
    private Map<String, Field> fillFields;               //所有注解填充的字段集合
    private Map<String, Field> fieldsForIncremental;     //注解为 自增减的字段
//    private Map<String, Field> fieldsByIds;            //多Id注解的field, 放入本map
    private Map<String, Method> mapSetter; //所有set方法对象集合
    private Map<String, Method> mapGetter; //所有get方法对象集合

    private boolean isDataDTO = false;     //是否DataDTO的子类, 紧紧用于检查是否标准的注解用, 如DataDTO 必须注解 表名, id字段

    /**
     * 获得标注 Incremental 的字段
     * @return
     */
    public Map<String,Field> getFieldsForIncremental(){
        return fieldsForIncremental;
    }

    public TableInfo setSysConfig(SysConfigRead sysConfig){
        super.setSysConfig(sysConfig);
        return this;
    }

    private TableInfo(Class<?> clazz) {
        if (clazz == null) {
            throw TopException.newInstance(ResponseCode.ERROR).text("new TableInfo, clazz not is null");
        }
        this.clazzEntity=clazz;
        this.isDataDTO= DataDTO.class.isAssignableFrom(clazz);

        if (Misc.isNull(tableName)) {
            Table table = clazz.getAnnotation(Table.class);
            if (table != null) {
                this.table = table;
                tableName   = table.name();
                tableCnName = table.cnName();
                redisKey    = table.redisKey();
            }
            if (table == null && isDataDTO) {
                mapTableInfo.remove(clazzEntity.getName());
                throw TopException.newInstance(ResponseCode.ERROR)
                        .text(clazz.getName() + " 是DataDTO的子类, 必须设置 @Table(name = 表名)");
            }
        }

        //获得所有层次类的Field
        listJavafield = new ArrayList<>(Arrays.asList(clazzEntity.getDeclaredFields()));
        Class<?> clazzParent=clazzEntity.getSuperclass();//得到父类
        while (clazzParent != null && !clazzParent.getName().toLowerCase().equals("java.lang.object")){
            if (clazzParent.getName().contains("com.topfox.common.DataDTO")){
                break;
            }
            listJavafield.addAll(Arrays.asList(clazzParent.getDeclaredFields()));
            clazzParent = clazzParent.getSuperclass(); //得到父类,然后赋给自己
        }

        fieldsObjectDataType = new ConcurrentHashMap<>();
        fields               = new LinkedHashMap<>();
        fieldsForIncremental = new HashMap<>();
        fillFields           = new ConcurrentHashMap<>();
//        fieldsByIds          = new LinkedHashMap<>();
        mapSetter            = new ConcurrentHashMap<>();
        mapGetter            = new ConcurrentHashMap<>();

        initFields();
        initGetSetMethod();

        if (idField==null && isDataDTO) {
            mapTableInfo.remove(clazzEntity.getName());
            throw TopException.newInstance(ResponseCode.ERROR)
                    .text( clazzEntity.getName() + " 是DataDTO的子类, 必须设置  主键字段");
        }
    }

    private static ConcurrentHashMap<Class,TableInfo> mapTableInfo;//双锁 缓存
    public static TableInfo get(Class<?> clazz) {
        if (clazz == null || clazz == Object.class) {
            return null;
        }
        if (mapTableInfo==null){
            synchronized(clazz){
                if (mapTableInfo==null){
                    mapTableInfo=new ConcurrentHashMap();
                }
            }
        }

        TableInfo tableInfo = mapTableInfo.get(clazz);
        if (tableInfo==null){
            synchronized (clazz){
                tableInfo = mapTableInfo.get(clazz);
                if (tableInfo==null) {
                    tableInfo=new TableInfo(clazz);
                    mapTableInfo.put(clazz, tableInfo);
                }
                tableInfo = mapTableInfo.get(clazz);
            }
        }
        return tableInfo;
    }

    public static TableInfo get(String clazzName){
        Class clazz;
        try {
            clazz = Class.forName(clazzName);
        } catch (ClassNotFoundException e) {
            throw TopException.newInstance(ResponseCode.ERROR).text("找不到 ", clazzName);
        }
        //拦截器 保存修改的DTO到redis使用
        return get(clazz);
    }


    public List<java.lang.reflect.Field> getJavaFields(){
        return listJavafield;
    }

    public Map<String,java.lang.reflect.Field> getJavaFieldsByMap(){
        return listJavafield.stream().collect(Collectors.toMap(java.lang.reflect.Field::getName,Function.identity()));
    }

    /**
     * 获得对象所有的字段信息
     */
    private void initFields() {
        //过滤出 Id字段,增加到fields中, 目的是将Id字段排列到第一
        listJavafield.stream().filter((field) -> field.getName().equals("id")  || field.isAnnotationPresent(Id.class))
                .collect(toList()).forEach((jdkField)->
        {
            idField = new Field(null, jdkField.getName(), DataType.getDataType(jdkField.getType()), sqlUnderscore());
            fields.put(jdkField.getName(), idField);
        });
        //过滤出 version字段,增加到fields中, 目的是将version字段排列到第二
        listJavafield.stream().filter((field) -> field.getName().equals("version") || field.isAnnotationPresent(Version.class))
                .collect(toList()).forEach((jdkField)-> {
            versionFieldName=jdkField.getName();
            fields.put(versionFieldName, new Field(null, versionFieldName, DataType.getDataType(jdkField.getType()),sqlUnderscore()));
        });

        //初始化 字段表结构
        listJavafield.forEach((jdkField)->{
            String name = jdkField.getName(), nameLower=name.toLowerCase();
            if(name.equals("serialVersionUID")){return;}
            if(Modifier.isStatic(jdkField.getModifiers())){return;} //静态变量的字段剔除
            if (jdkField.getDeclaredAnnotation(Ignore.class)!=null){
                return;// 等同普通遍历的 continue
            }

            if(nameLower.endsWith("createdate") || name.endsWith("create_date")){
                createDateFieldName=name;
            }
            if(nameLower.endsWith("createuser") || name.endsWith("create_user")){
                createUserFieldName=name;
            }
            if(    nameLower.endsWith("modifydate") || name.endsWith("modify_date")
                || nameLower.endsWith("updatedate") || name.endsWith("update_date")
            ){
                updateDateFieldName=name;
            }
            if(        nameLower.endsWith("modifyuser") || name.endsWith("modify_user")
                    || nameLower.endsWith("updateuser") || name.endsWith("update_user")
            ){
                updateUserFieldName=name;
            }

//            String fieldClass= jdkField.getType().getName();
//            if (fieldClass.equals(List.class.getName()) || fieldClass.equals(ArrayList.class.getName())) {
//                return;//明细容器的Field不能算作字段,否则将生成SQL(错误的)
//            }

            if (!DataHelper.isBaseDataType(jdkField.getType())){
                //复杂对象类型的字段, 如dto, map, list set
                fieldsObjectDataType.put(name,jdkField);
                return;
            }

            Other other; TableField tableField; FillBoth fillBoth; FillInsert fillInsert; FillUpdate fillUpdate;
            //识别出注解的字段
            if(jdkField.isAnnotationPresent(State.class)){
                stateFieldName=name;
                return;
            }else if(jdkField.isAnnotationPresent(RowId.class)) {
                rowNoFieldName=name;
                return;
            }else {
                tableField = jdkField.getDeclaredAnnotation(TableField.class);
                other      = jdkField.getDeclaredAnnotation(Other.class);
                fillBoth   = jdkField.getDeclaredAnnotation(FillBoth.class);
                fillInsert = jdkField.getDeclaredAnnotation(FillInsert.class);
                fillUpdate = jdkField.getDeclaredAnnotation(FillUpdate.class);

            }

            if (name.indexOf("_")>0) {
                mapTableInfo.remove(clazzEntity.getName());
                throw TopException.newInstance(ResponseCode.ERROR).text( clazzEntity.getName() + "."+name+" 不支持包含下划线, 请标准书写");
            }

            if (fields.containsKey(name)) {return;}
            if (name.equals("omtDateTime")){
                System.out.println("###");
            }
            Field fieldTopfox=new Field(tableField, name, DataType.getDataType(jdkField.getType()), sqlUnderscore());
            if (tableField != null ) {
                /** 注解为 自增减的字段 **/
                if (tableField.incremental() == Incremental.ADDITION || tableField.incremental() == Incremental.SUBTRACT) {
                    if (!DataType.isNumber(fieldTopfox.getDataType())){
                        mapTableInfo.remove(clazzEntity.getName());
                        throw TopException.newInstance(ResponseCode.ERROR).text( clazzEntity.getName() + "."+name+"类型不对,不能注解为自增减(必须是Long/Integer/Double/Decimal)");
                    }
                    fieldTopfox.setIncremental(tableField.incremental());
                    fieldsForIncremental.put(name, fieldTopfox);
                }
            }

            fieldTopfox.setFillInsert(fillBoth!=null||fillInsert!=null?true:false);
            fieldTopfox.setFillUpdate(fillBoth!=null||fillUpdate!=null?true:false);
            fieldTopfox.exist(other==null);
            if (fieldTopfox.isFillInsert() || fieldTopfox.isFillUpdate()) {
                fillFields.put(name, fieldTopfox);
            }

            //依据配置 获取默认需要 填充的字段
            initFillDataHandler();
            if (tableField==null && fillDataHandler!=null){
                if (fillDataHandler.getInsertFillFields()!=null){
                    fillDataHandler.getInsertFillFields().forEach(fieldName->{
                        if (name.toLowerCase().endsWith(fieldName.toLowerCase())) {
                            fieldTopfox.setFillInsert(true);
                            fillFields.put(name, fieldTopfox);
                        }
                    });
                }
                if (fillDataHandler.getUpdateFillFields()!=null){
                    fillDataHandler.getUpdateFillFields().forEach(fieldName->{
                        if (name.toLowerCase().endsWith(fieldName.toLowerCase())) {
                            fieldTopfox.setFillUpdate(true);
                            fillFields.put(name, fieldTopfox);
                        }
                    });
                }
            }


            //获取注解的 格式化信息,  主要是 日期类型  小数精确位数的 格式化信息
            JsonFormat jsonFormat = jdkField.getDeclaredAnnotation(JsonFormat.class);

            if (jsonFormat != null) {
                fieldTopfox.setFormat(jsonFormat.pattern());
            }

            if (fieldTopfox.getFormat() ==null && fieldTopfox.getDataType() == DataType.DATE){
                fieldTopfox.setFormat(getSysConfig().getString("spring.jackson.date-format"));
            }

            //实体的字段集合
            fields.put(name, fieldTopfox);
        });
    }

    private static FillDataHandler fillDataHandler;             //每个表独立一个配置文件
    public static FillDataHandler initFillDataHandler(){
        if (fillDataHandler != null){
            return fillDataHandler;
        }
        if (fillDataHandler == null) {
            fillDataHandler = AppContext.getBean(FillDataHandler.class);
        }

        return fillDataHandler;
    }

//    /**
//     * 获得 对象的所有 get set方法
//     */
//    private void initGetSetMethod() {
//        Method[] methods = clazzEntity.getDeclaredMethods();
//
//        for (Method method : methods) {
//            String fieldName;
//            String methodName = method.getName();
//
//            // 把get set去掉, 并将剩下的第一个字母转为小写, 从而推算出字段名, 与DTO的字段名一致
//            // boolean类型时, get 方法 lombok 会 生成isFieldName
//            if (methodName.startsWith("is")){
//                fieldName = methodName.substring(2, 3).toLowerCase() + methodName.substring(3);
//            }else if (methodName.startsWith("set") || methodName.startsWith("get")){
//                fieldName = methodName.substring(3, 4).toLowerCase() + methodName.substring(4);
//            }else{
//                continue;
//            }
//            Field field = getFields().get(fieldName);
//            if (fieldName.equals(getStateFieldName())
//                    || fieldName.equals(getVersionFieldName())
//                    || fieldName.equals(getRowNoFieldName())) {
//                //系统定义的 3个字段, 不能执行 continue; 不然那 找不到get set方法,要报错. logger.debug();
//            } else if (field == null) {
//                continue;//说明不是 POJO定义的字段, 跳过.
//            }
//
//            if (methodName.startsWith("set")){
//                mapSetter.put(fieldName, method);
//            }else if(methodName.startsWith("get") || methodName.startsWith("is")){
//                mapGetter.put(fieldName, method);
//            }
//        }
//    }

    /**
     * 获得 对象的所有 get set方法
     */
    private void initGetSetMethod() {
        BeanInfo beanInfo;
        try {
            beanInfo = Introspector.getBeanInfo(clazzEntity, 1);
        } catch (IntrospectionException e1) {
            throw new RuntimeException(e1);
        }

        MethodDescriptor[] methodDescriptors = beanInfo.getMethodDescriptors();
        for (MethodDescriptor methodDescriptor : methodDescriptors) {
            String fieldName;
            Method method = methodDescriptor.getMethod();
            String methodName = methodDescriptor.getName();
            if (methodName.equals("getClass")){
                continue;
            }
            int parameterCount = method.getParameterCount();
            // 把get set去掉, 并将剩下的第一个字母转为小写, 从而推算出字段名, 与DTO的字段名一致
            // boolean类型时, get 方法 lombok 会 生成isFieldName
            //if (methodName.startsWith("is")){
            //    fieldName = methodName.substring(2, 3).toLowerCase() + methodName.substring(3);
            //}else
            if (methodName.startsWith("set") || methodName.startsWith("get")){
                fieldName = methodName.substring(3, 4).toLowerCase() + methodName.substring(4);
            }else{
                fieldName = methodName;
                //continue;
            }
            Field field = getFields().get(fieldName);
//            if (field==null && !fieldName.equals(getRowNoFieldName()) && !fieldName.equals(getStateFieldName())) {
//                //解决字段名 叫isAdmin 不会自动生成 getIsAdmin的问题.  get方法就是 isAdmin();
//                fieldName = "is"+fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
//                field = fields.get(fieldName);
//            }
            if (fieldName.equals(getStateFieldName())
                    || fieldName.equals(getVersionFieldName())
                    || fieldName.equals(getRowNoFieldName())) {
                //系统定义的 3个字段, 不能执行 continue; 不然那 找不到get set方法,要报错. logger.debug();
            } else if (field == null) {
                // 2020 luojp 注释 continue;//说明不是 POJO定义的字段, 跳过.
            }

            if (parameterCount==1 || methodName.startsWith("set")){
                mapSetter.put(fieldName,method);
            }else if(parameterCount==0 || methodName.startsWith("get") || methodName.startsWith("is")){
                mapGetter.put(fieldName,method);
            }
        }
    }

    public Method getSetter(String fieldName){
        return mapSetter.get(fieldName);
    }
    public Method getGetter(String fieldName){
        return mapGetter.get(fieldName);
    }

//    public Map<String,Field> getMapIdFields(){
//        return fieldsByIds;
//    }

//    /**
//     * 多个主键字段 用 - 串起来
//     * 例如: userId-deptId
//     * @return
//     */
//    String idFields=null;
//    public String getIdFields(){
//        if (idFields == null){
//            if (fieldsByIds.size()>1) {
//                StringBuilder stringBuilder = new StringBuilder();
//                fieldsByIds.forEach((key,field) ->
//                    stringBuilder.append(field.getName()).append("-")
//                );
//                stringBuilder.setLength(stringBuilder.length() - 1);
//                this.idFields=stringBuilder.toString();
//            }else{
//                this.idFields=this.idFieldName;
//            }
//        }
//
//        return idFields;
//    }

    /**
     * 获得一个主键字段
     * @return
     */
    public Field getIdField(){
        return this.idField;
    }

    public String getVersionFieldName(){
        return this.versionFieldName;
    }

    public String getCreateDateFieldName(){
        return this.createDateFieldName;
    }
    public String getCreateUserFieldName(){
        return this.createUserFieldName;
    }
    public String getUpdateDateFieldName(){
        return this.updateDateFieldName;
    }
    public String getUpdateUserFieldName(){
        return this.updateUserFieldName;
    }

    public String getStateFieldName(){
        return this.stateFieldName;
    }

    public String getRowNoFieldName(){
        return this.rowNoFieldName;
    }

    public String getTableName(){
        return this.tableName;
    }
    public String getTableCnName(){
        return this.tableCnName;
    }

    public String getRedisKey(){
        return Misc.isNull(this.redisKey)?clazzEntity.getName():redisKey;
    }


    public Map<String,Field> getFields(){
        return fields;
    }

    public Field getField(String fieldName){
        if (fieldName==null) {return null;}
        fieldName=fieldName.trim();
        return getFields().get(fieldName);
    }

    public Map<String, java.lang.reflect.Field> getFieldsByObject(){
        return this.fieldsObjectDataType;
    }


    /**
     * 获得所有有新增 修改 填充的字段
     * @return
     */
    public Map<String,Field> getFillFields(){
        return fillFields;
    }


    public SqlUnderscore sqlUnderscore(){
        if (table!=null && table.sqlUnderscore()!= SqlUnderscore.OFF) {
            return table.sqlUnderscore();
        }
        return AppContext.getSysConfig().getSqlCamelToUnderscore();
    }

    /**
     * 驼峰命名转换 为 有下划线的字段名
     * @param fieldName
     * @return
     */
    public String getColumn(String fieldName){
        Field field = getFields().get(fieldName);
        if (field != null ){
            return field.getDbName();
        }

        return getColumn2(fieldName);
    }
    /**
     *
     * 纯字符串处理
     * @param fieldName
     * @return
     */
    public String getColumn2(String fieldName){
        return TableInfo.getDbName(sqlUnderscore(), fieldName);
    }



    /**
     * 依据全局的 SqlCamelToUnderscore 配置 获取数据库字段名
     * @param fieldName
     * @return
     */
    public static String getDbName(String fieldName) {
        return getDbName(AppContext.getSysConfig().getSqlCamelToUnderscore(), fieldName);
    }
    public static String getDbName(SqlUnderscore sqlUnderscore, String fieldName){
        if (sqlUnderscore == SqlUnderscore.ON_UPPER) {
            //开启 字段名依据驼峰命名转下划线, 并全部 大写
            if (!fieldName.contains("_")){
                return CamelHelper.toUnderlineName(fieldName);//默认全部转大写 .toUpperCase()
            }
            fieldName=fieldName.toUpperCase();
        }else if (sqlUnderscore == SqlUnderscore.ON_LOWER){
            //开启 字段名依据驼峰命名转下划线, 并全部 小写
            if (!fieldName.contains("_")) {
                return CamelHelper.toUnderlineName(fieldName).toLowerCase();
            }
            fieldName=fieldName.toLowerCase();
        }
        return fieldName;
    }
}
