package cnn.topfox.data;

import cnn.topfox.common.CommonException;
import cnn.topfox.common.ResponseCode;
import cnn.topfox.misc.DateUtils;
import cnn.topfox.misc.Misc;
import com.alibaba.fastjson2.JSONObject;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.regex.Pattern;

public final class DataHelper {
	public static Object toObject(byte value) {
		return new Byte(value);
	}

	public static Object toObject(short value) {
		return new Short(value);
	}

	public static Object toObject(int value) {
		return new Integer(value);
	}

	public static Object toObject(long value) {
		return new Long(value);
	}

	public static Object toObject(float value) {
		return new Float(value);
	}

	public static Object toObject(double value) {
		return new Double(value);
	}

	public static Object toObject(boolean value) {
		return new Boolean(value);
	}

	public static Object toObject(Date value) {
		return value;
	}

	public static Object toObject(BigDecimal value) {
		return value;
	}

	public static Object toObject(String value) {
		return value;
	}

	public static String parseString(Object o) {
		if (o == null || "undefined".equals(o.toString()) || "null".equals(o.toString())) {
			return "";
		}
		if (o instanceof Date) {
			return DateUtils.toDateTimeMillis((Date)o);
			//return String.valueOf(((Date) o).getTime());
		}
		String stringValue = o.toString();
		stringValue=stringValue.trim();
		return stringValue;
	}

	/**
	 * 处理 英文单引号 保存 查询的问题
	 * @param o
	 * @return
	 */
	public static String parseString2(Object o) {
		String stringValue = parseString(o);
		if(stringValue.indexOf("\\")>=0) stringValue=stringValue.replace("\\","\\\\");
		if(stringValue.indexOf("'")>=0) stringValue=stringValue.replace("'","\\'");
		return stringValue;
	}


	public static byte parseByte(Object o) {
		if (o == null)
			return 0;
		if (o instanceof Number)
			return ((Number) o).byteValue();
		if (o instanceof Boolean) {
			return (byte) ((((Boolean) o).booleanValue()) ? 1 : 0);
		}
		String s = o.toString();
		if (s.equals("") || s.equals("NaN")) {
			return 0;
		}
		return Byte.parseByte(s);
	}

	public static int parseIntByNull2Zero(Object o) {
		if (o == null) {
			return 0;
		}
		return parseInt(o);
	}

	public static Integer parseInt(Object o) {
		if (o == null){
			return null;
		}
		if (o.toString().indexOf("'")>=0){
			throw new RuntimeException(o.toString()+" 不能转换为整型");
		}

		if (o instanceof Number)
			return ((Number) o).intValue();
		if (o instanceof Boolean) {
			return ((((Boolean) o).booleanValue()) ? 1 : 0);
		}
		String s = o.toString().replace(",","").replace("'","").trim();
		if (s.equals("") || s.equals("NaN")) {
			return 0;
		}

		if (s.length()>9){
			throw new RuntimeException("内容过长，超出设计限制");
		}
		return Integer.parseInt(s);
	}

	public static Long parseLongByNull2Zero(Object o) {
		if (o == null) {
			return 0L;
		}
		return parseLong(o);
	}

	public static Long parseLong(Object o) {
		if (o == null)
			return null;
		if (o instanceof Number)
			return ((Number) o).longValue();
		if (o instanceof Boolean)
			return ((((Boolean) o).booleanValue()) ? 1L : 0L);
		if (o instanceof Date) {
			return ((Date) o).getTime();
		}
		String s = o.toString().replace(",","").trim();
		if (s.equals("") || s.equals("NaN")) {
			return 0L;
		}
		if (s.length()>18){
			throw new RuntimeException(" 内容过长，超出设计限制");
		}
		return Long.parseLong(s);
	}

	public static float parseFloat(Object o) {
		if (o == null)
			return 0.0F;
		if (o instanceof Number)
			return ((Number) o).floatValue();
		if (o instanceof Boolean) {
			return ((((Boolean) o).booleanValue()) ? 1.0F : 0.0F);
		}
		String s = o.toString().trim();
		if (s.equals("")) {
			return 0.0F;
		}
		return Float.parseFloat(s);
	}

	public static double parseDoubleByNull2Zero(Object o) {
		if (o == null) {
			return 0.0D;
		}
		return parseDouble(o);
	}

	public static Double parseDouble(Object o) {
		if (o == null)
			return null;
		if (o instanceof Number)
			return ((Number) o).doubleValue();
		if (o instanceof Boolean) {
			return ((((Boolean) o).booleanValue()) ? 1.0D : 0.0D);
		}
		String s = o.toString().replace(",","").trim();
		if (s.equals("") || s.equals("NaN")) {
			return 0.0D;
		}
		return Double.parseDouble(s);
	}
	public static BigDecimal parseBigDecimalByNull2Zero(Object o) {
		if (o == null) {
			return new BigDecimal(0);
		}
		return parseBigDecimal(o);
	}
	public static BigDecimal parseBigDecimal(Object o) {
		if (o == null)
			return null;
		if (o instanceof BigDecimal)
			return ((BigDecimal) o);
		if (o instanceof Number)
			return new BigDecimal(((Number) o).doubleValue());
		if (o instanceof Boolean) {
			return new BigDecimal((((Boolean) o).booleanValue()) ? 1 : 0);
		}
		String s = o.toString().replace(",","").trim();
		if (s.equals("") || s.equals("NaN")) {
			return new BigDecimal(0);
		}
		return new BigDecimal(s);
	}

	public static Boolean parseBoolean(String s) {
		if (s == null) {
			return null;
		}
		return ((s.equalsIgnoreCase("true")) || (s.equals("1")) || (s.equals("-1")) || (s.equalsIgnoreCase("T"))
				|| (s.equalsIgnoreCase("Y")));
	}

	public static boolean parseBoolean(Object o) {
		if (o == null)
			return false;
		if (o instanceof Boolean) {
			return ((Boolean) o).booleanValue();
		}
		return parseBoolean(o.toString());
	}

	private static boolean isNumeric(String s) {
		int length = s.length();
		for (int i = 0; i < length; ++i) {
			char c = s.charAt(i);
			if ((((c < '0') || (c > '9'))) && (c != '.') && (((i != 0) || (c != '-')))) {
				return false;
			}
		}

		return true;
	}

	public static Date parseDate(Object o) {
		if (o == null)	return null;
		if ((o instanceof String) && (((String)o).length() == 0)) return null;

		if (o instanceof Date) return ((Date) o);
		if (o instanceof Number) {
			return new Date(((Number) o).longValue());
		}
		if (o instanceof LocalDateTime){
			LocalDateTime localDateTime=(LocalDateTime) o;
			return DateUtils.toDate(localDateTime);
		}
		if (o instanceof LocalDate){
			LocalDate localDate=(LocalDate) o;
			return DateUtils.toDate(localDate);
		}

		String stringValue = String.valueOf(o).trim();
		if (Misc.isNull(stringValue)) {
			return null;
		}

		if (isNumeric(stringValue)) {
			long time = Long.parseLong(stringValue);
			return new Date(time);
		}else if(stringValue!=null && stringValue.indexOf("T")>0){
			//处理javascript默认格式字符 yyyy-MM-dd'T'HH:mm:ss'Z'
			try{
				StringBuilder sb = new StringBuilder();
				sb.append("DataHelper 原始值stringValue=").append(stringValue);

				String stringValue2=stringValue.replaceAll("T"," ");
				stringValue2=stringValue2.substring(0,stringValue2.indexOf("."));
				Date d1 = DateUtils.stringToDate(stringValue2);
				Date d2 = DateUtils.getChangeDate(d1, "hour", 8);

				      sb.append(" d1=").append(DateUtils.toDateStr(d1,DateUtils.DATETIME_SECOND_FORMAT))
						.append(" d2=").append(DateUtils.toDateStr(d2,DateUtils.DATETIME_SECOND_FORMAT))
				;
				sendByJSB(sb.toString());

				return d2;
			}catch(Exception ex){
				throw new RuntimeException(ex);
			}
		}
		if (stringValue.contains("年") || stringValue.contains("月") || stringValue.contains("日") || stringValue.contains("/")){
			stringValue=stringValue.replace("年", "-");
			stringValue=stringValue.replace("月", "-");
			stringValue=stringValue.replace("日", "-");
			stringValue=stringValue.replace("/",  "-");
		}

		int len = stringValue.length();
		if (stringValue.indexOf(":") > 0) {
			if (len == 16) {
				return DateUtils.toDate(stringValue, DateUtils.DATETIME_FORMAT);           //"yyyy-MM-dd HH:mm"
			}else if (len == 17||len == 18||len == 19) {
				return DateUtils.toDate(stringValue, DateUtils.DATETIME_SECOND_FORMAT);    //yyyy-MM-dd HH:mm:ss
			}else if (len >= 20) {
				if (stringValue.indexOf(".")==19){
					return DateUtils.toDate(stringValue, "yyyy-MM-dd HH:mm:ss.SSS");
				}else{
					return DateUtils.toDate(stringValue, "yyyy-MM-dd HH:mm:ss SSS");
				}
			}else if (len == 8) {
				return DateUtils.toDate(stringValue, DateUtils.TIME_SECOND_FORMAT); //HH:mm:ss
			}else{//len == 5
				return DateUtils.toDate(stringValue, DateUtils.TIME_FORMAT); //HH:mm
			}
		}else{
			return DateUtils.toDate(stringValue, DateUtils.DATE_FORMAT);
		}
	}

	public static java.time.LocalDate parseLocalDate(Object o) {
		if (o == null)	return null;
		if (o instanceof String && ((String)o).length() == 0 ) {
			return null;
		}else if (o instanceof java.time.LocalDate){
			return (java.time.LocalDate) o ;
		} else  if (o instanceof java.time.LocalDateTime){
			return ((java.time.LocalDateTime) o ).toLocalDate();
		}else if (o instanceof Date){
			return ((Date) o).toInstant().atZone(ZoneId.systemDefault()).toLocalDate() ;
		}else if (o instanceof String){
			String valueString = o.toString();
			if (valueString.contains("/")){
				valueString=valueString.replaceAll("/","-");
			}
			if (valueString.contains("年")){
				valueString=valueString.replaceAll("年","-");
				valueString=valueString.replaceAll("月","-");
				valueString=valueString.replaceAll("日","");
			}
			if (valueString.length()>10){
				valueString=valueString.substring(0,10);
			}
			return LocalDate.parse(valueString);
		}else{
			throw CommonException.newInstance2(ResponseCode.PARAM_IS_INVALID);
		}
	}

	public static DateTime parseDateTime(Object o) {
		if (o == null)	return null;
		if ((o instanceof String) && (((String)o).length() == 0)) return null;

		DateTime ret=null;
		if (o instanceof DateTime){
			return (DateTime) o;
		}else if (o instanceof Date){
			ret= new DateTime((Date) o);
		}else if (o instanceof LocalDate){
			ret= new DateTime((LocalDate) o);
		}else if (o instanceof LocalDateTime){
			ret= new DateTime((LocalDateTime) o);
		}else if (o instanceof String){
			ret=new DateTime(o.toString());
		}else{
			throw CommonException.newInstance2(ResponseCode.PARAM_IS_INVALID);
		}
		return ret;
	}

	public static java.time.LocalTime parseLocalTime(Object o) {
		if (o == null)	return null;
		if (o instanceof String && ((String)o).length() == 0 ) return null;

		if (o instanceof java.time.LocalTime){
			return (java.time.LocalTime) o ;
		}

		LocalTime ret=null;
		if (o instanceof String){
			try {
				ret = LocalTime.parse(o.toString());
			} catch (DateTimeParseException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}

	public static java.time.LocalDateTime parseLocalDateTime(Object o) {
		if (o == null)	return null;
		if (o instanceof String && ((String)o).length() == 0 ) return null;

		if (o instanceof java.time.LocalDateTime){
			return (java.time.LocalDateTime) o ;
		}

		LocalDateTime ret=null;
		if (o instanceof String){
			try {
				String dateString = o.toString();
				dateString=dateString.trim();
				if (dateString.indexOf("T")==10){
					//2023-11-11T08:00 改为 2023-11-11 08:00
					dateString=dateString.replace("T"," ");
				}
				if (dateString.length()==10){
					dateString=dateString+" 00:00:00";
				}

				if (dateString.length()==16){
					ret = LocalDateTime.parse(dateString, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
				}else if (dateString.length()==19){
					ret = LocalDateTime.parse(dateString, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
				}else if (dateString.length()>19){
					ret = LocalDateTime.parse(dateString.substring(0,19), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
				}
			} catch (DateTimeParseException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}



	/***
	* 判断 String 是否是 int
	*
	* @param strValue
	* @return
	*/
	public static boolean isInteger(Object strValue){
		if (strValue != null && !strValue.toString().contains("--")) {
			Pattern pattern = Pattern.compile("-?[0-9]");
			return pattern.matcher(strValue.toString()).matches();
		}else{
			return false;
		}

	}

	/***
	 * 判断 String 是否是 number
	 *
	 * @param strValue
	 * @return
	 */
	public static boolean isNumber(Object strValue){
		if (strValue != null && !strValue.toString().contains("--")) {
			Pattern pattern = Pattern.compile("-?[0-9]+\\.*[0-9]*");
			return pattern.matcher(strValue.toString()).matches();
		}else{
			return false;
		}
	}

//	public static Object translate(int dataType, Object value) {
//		if ((value == null) || ((value instanceof String) && (((String)value).length() == 0))) {
//			if (dataType == 1) {
//				return value;
//			}
//			return null;
//		}
//
//		switch (dataType) {
//		case 1:
//			return parseString(value);
//		case 9:
//			return toObject(parseBoolean(value));
//		case 10:
//		case 11:
//		case 12:
//			return parseDate(value);
//		case 4:
//			return toObject(parseInt(value));
//		case 7:
//			return toObject(parseDouble(value));
//		case 5:
//			return toObject(parseLong(value));
//		case 6:
//			return toObject(parseFloat(value));
//		case 8:
//			return parseBigDecimal(value);
//		case 2:
//			return toObject(parseByte(value));
//		case 3:
//			return toObject(parseShort(value));
//		}
//
//		return value;
//	}



	public static Double round(Double number,String format){
		java.text.DecimalFormat   df=new   java.text.DecimalFormat(format);
		String amt=df.format(number);
		return Double.parseDouble(amt);
	}
	public static Double round(Double dSource, int weishu){
		return round(dSource,weishu,BigDecimal.ROUND_HALF_UP);
	}

	public static String roundToString(Double number,String format){
		format = Misc.isNull(format)?"###0.00":format;//没有格式化信息  则默认 精确到两位小数
		java.text.DecimalFormat   df=new   java.text.DecimalFormat(format);
		String amt=df.format(number);
		return amt;
	}
	public static String roundToString(Object number,String format){
		return roundToString(parseDouble(number), format);
	}
	public static String roundWeight2String(Double weight){
		return roundToString(weight,"###0.000");
	}

	public static String roundCubage2String(Double cubage){
		return roundToString(cubage,"###0.0000");
	}

	/**
	 * 对double数据进行取精度.
	 * @param value  double数据.
	 * @param scale  精度位数(保留的小数位数).
	 * @param roundingMode  精度取值方式.
	 *        四舍五入：BigDecimal.ROUND_HALF_UP
	 *        截取               BigDecimal.ROUND_DOWN
	 * @return double 精度计算后的数据.
	 */
	public static double round(Double value, int scale,
							   int roundingMode) {
		double value2=value;
		if (value<0)value2=-value;
		BigDecimal bd = new BigDecimal(Double.toString(value2));
		bd = bd.setScale(scale, roundingMode);
		double ret = bd.doubleValue();
		bd = null;
		return value<0?-ret:ret ;
	}

	/**
	 * 判断是否为对象, Java 基本数据类型除外, 即视为对象
	 *
	 */
	public static Boolean isBaseDataType(Class clazz){
		if (clazz ==null ) return null;
		if (String.class.equals(clazz)
				|| Number.class.isAssignableFrom(clazz)
				|| Number.class.equals(clazz)
				|| java.sql.Date.class.equals(clazz)
				|| Boolean.class.equals(clazz)
				|| boolean.class.equals(clazz)
				|| byte.class.equals(clazz)
				|| char.class.equals(clazz)
				|| Integer.class.equals(clazz)
				|| int.class.equals(clazz)
				|| Long.class.equals(clazz)
				|| long.class.equals(clazz)
				|| float.class.equals(clazz)
				|| Double.class.equals(clazz)
				|| double.class.equals(clazz)
				|| Date.class.equals(clazz)
				|| DateTime.class.equals(clazz)
				|| LocalTime.class.equals(clazz)
				|| LocalDate.class.equals(clazz)
				|| LocalDateTime.class.equals(clazz)

		){
			return true;
		}

		return false;
	}

	static RestTemplate restTemplate;
	public static RestTemplate restTemplate() {
		if (restTemplate == null) {
			SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
			factory.setReadTimeout(10000);   //毫秒
			factory.setConnectTimeout(5000); //毫秒
			restTemplate = new RestTemplate(factory);

			HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
			httpRequestFactory.setConnectionRequestTimeout(10000);
			httpRequestFactory.setConnectTimeout(5000);
			httpRequestFactory.setReadTimeout(5000);
			restTemplate = new RestTemplate(httpRequestFactory);
			// 支持中文编码
			restTemplate.getMessageConverters().set(1, new StringHttpMessageConverter(Charset.forName("UTF-8")));

		}
		return restTemplate;
	}

	//发送到技术部消息
	public static void  sendByJSB (String content){
		JSONObject params=new JSONObject();
		params.put("msgtype","text");
		JSONObject text=new JSONObject();
		text.put("content",content);
		params.put("text",text);
		restTemplate().postForEntity("https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=1825d1ff-6779-46ed-b377-0a3ba43f3e91", params, JSONObject.class);
	}

	public static void main(String[] args) {
//		String date1= DateUtils.long2DateTime(System.currentTimeMillis());
//		Date date2=DateUtils.stringToDate(date1);

		//sendByJSB("IT Test" + DateUtils.getDateByHHmmss());

	}

}