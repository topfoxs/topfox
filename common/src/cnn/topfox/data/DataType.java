package cnn.topfox.data;

public enum DataType {
    NONE    ( 0, "NONE"),
    STRING  ( 1, "STRING"),
    INTEGER ( 4, "INTEGER"),
    LONG    ( 5, "LONG"),
    DOUBLE  ( 7, "DOUBLE"),
    DECIMAL ( 8, "DECIMAL"),
    BOOLEAN ( 9, "BOOLEAN"),

    DATE    (10, "DATE"),
    DATETIME(11, "DATETIME"),//cnn.topfox.data.DateTime
    LocalDate    (12, "LocalDate"),
    LocalTime    (13, "LocalTime"),
    LocalDateTime(14, "LocalDateTime"),

    OBJECT  (99, "OBJECT");//DTO 中包含 DTO的情况

    private Integer code;
    private String value;

    DataType(Integer code, String value) {
        this.code = code;
        this.value = value;
    }

    public Integer getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public static String getValueByCode(Integer code) {
        if (code != null) {
            for (DataType current : DataType.values()) {
                if (current.getCode().equals(code)) {
                    return current.getValue();
                }
            }
        }
        return null;
    }

    public static DataType getByValue(String value) {
        if (value != null) {
            for (DataType current : DataType.values()) {
                if (current.getValue().equals(value)) {
                    return current;
                }
            }
        }
        return null;
    }

    public static DataType getByValue2(String value) {
        //前端数据用(转excel)
        if (value==null) {
            return DataType.NONE;
        }

        value=value.toLowerCase();
        if (value.equals("string")){
            return DataType.STRING;
        } else if (value.equals("integer") || value.equals("int")){
            return DataType.INTEGER;
        } else if (value.equals("double")){
            return DataType.DOUBLE;
        } else if (value.equals("boolean")){
            return DataType.BOOLEAN;
        } else if (value.equals("date")){
            return DataType.DATE;
        } else if (value.equals("datetime")){
            return DataType.DATETIME;
        }else {
            return DataType.NONE;
        }
    }

    public static DataType getDataType(Class<?> type) {
        String clazzName = type.getName();

        if (clazzName.equals("java.util.Date"))
            return DataType.DATE;
        if (clazzName.equals("java.time.LocalDateTime"))
            return DataType.LocalDateTime;
        if (clazzName.equals("java.time.LocalTime"))
            return DataType.LocalTime;
        if (clazzName.equals("java.time.LocalDate"))
            return DataType.LocalDate;
        if (clazzName.equals("cnn.topfox.data.DateTime"))
            return DataType.DATETIME;

        if (clazzName.equals("java.math.BigDecimal"))
            return DataType.DECIMAL;
        if (clazzName.equals("double") || clazzName.equals("java.math.Double") || clazzName.equals("java.lang.Double"))
            return DataType.DOUBLE;
        if (clazzName.equals("int") ||
                clazzName.equals("java.lang.Integer") || clazzName.equals("integer"))
            return DataType.INTEGER;
        if (clazzName.equals("long") || clazzName.equals("java.lang.Long"))
            return DataType.LONG;
        if (clazzName.equals("boolean") || clazzName.equals("java.lang.Boolean"))
            return DataType.BOOLEAN;
        if (clazzName.equals("java.lang.String"))
            return DataType.STRING;

        //throw new CommonException(ResponseCode.SYSTEM_ERROR,"设计上不支持类型"+dataType2);
        return DataType.OBJECT;
    }

    public static boolean isNumber(DataType dataType) {
        return dataType==DataType.DOUBLE || dataType==DataType.INTEGER || dataType==DataType.DECIMAL || dataType==DataType.LONG;
    }
}
