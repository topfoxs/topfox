
# 1. TopFox 用户使用手册 - 目录
- [快速使用](https://gitee.com/topfoxs/topfox/blob/dev/guide/explain-sample.md)
- [高级应用](https://gitee.com/topfoxs/topfox/blob/dev/guide/explain-advanced.md)
- [TopFox配置参数](https://gitee.com/topfoxs/topfox/blob/dev/guide/explain-configuration.md)
- [上下文对象](https://gitee.com/topfoxs/topfox/blob/dev/guide/explain-appcontext.md)
- [核心使用](https://gitee.com/topfoxs/topfox/blob/dev/guide/explain-core.md)
- [条件匹配器](https://gitee.com/topfoxs/topfox/blob/dev/guide/explain-condition.md)
- [实体查询构造器](https://gitee.com/topfoxs/topfox/blob/dev/guide/explain-entityselect.md)
- [流水号生成器](https://gitee.com/topfoxs/topfox/blob/dev/guide/explain-keybuild.md)
- [数据校验组件](https://gitee.com/topfoxs/topfox/blob/dev/guide/explain-checkdata.md)
- [更新日志组件](https://gitee.com/topfoxs/topfox/blob/dev/guide/explain-updatelog.md)
- [自动填充组件](https://gitee.com/topfoxs/topfox/blob/dev/guide/explain-filldata.md)
- [Response 返回结果对象](https://gitee.com/topfoxs/topfox/blob/dev/guide/explain-response.md)
- [其他](https://gitee.com/topfoxs/topfox/blob/dev/guide/explain-other.md)

## 1.1. 必备
- 文中涉及的例子源码网址: https://gitee.com/topfoxs/topfox-sample
- TopFox 技术交流群 QQ: 874732179

## 1.2. topfox 介绍
在 SrpingBoot2.x.x 和MyBatis 的基础上只做增强不做改变，为简化开发、提高效率而生。

编程规范参考《阿里巴巴Java开发手册》

借鉴mybaties plus部分思想

特性:

- **无侵入**：只做增强不做改变，引入它不会对现有工程产生影响
- **损耗小**：启动即会自动注入基本 CURD，性能基本无损耗，直接面向对象操作
- **集成Redis缓存**: 自带Redis缓存功能, 支持多主键模式, 自定义redis-key. 实现对数据库的所有操作, 自动更新到Redis, 而不需要你自己写任何代码; 当然也可以针对某个表关闭. 
- **强大的 CRUD 操作**：内置通用 Mapper、通用 Service，仅仅通过少量配置即可实现单表大部分 CRUD 操作，更有强大的条件构造器，满足各类使用需求
- **支持 Lambda 形式调用**：通过 Lambda 表达式，方便的编写各类查询条件，无需再担心字段写错
- **支持主键自动生成**：可自由配置，充分利用Redis提高性能, 完美解决主键问题.  支持多主键查询、修改等
- **内置分页实现**：基于 MyBatis 物理分页，开发者无需关心具体操作，写分页等同于普通查询
- **支持devtools/jrebel热部署**
- **热加载** 支持在不使用devtools/jrebel的情况下, 热加载 mybatis的mapper文件
- 内置全局、局部拦截插件：提供delete、update 自定义拦截功能
- **拥有预防Sql注入攻击功能**
- **无缝支持spring cloud**: 后续提供分布式调用的例子

## 1.3. topfox 使用
> topfox 已经上传到 maven中央库, 在业务项目进行以下依赖

```
<!--topfox-->
<dependency>
    <groupId>com.topfoxs</groupId>
    <artifactId>topfox</artifactId>
    <version>1.2.18</version>
    <type>pom</type>
    <scope>import</scope>
</dependency>
```

# 2. 更新日志

## 2.1. 版本1.2.18  更新日志 2019-12-16

- DataQTO 增加 openPage, 是否关闭sql limit
- DataDTO.addNullFields 支持递增追加字段, 原来是替换
- 条件SQL 移除 使用concat函数连接字段, 多字段用 and  or 代替
- SpringBoot版本更新到 2.2.1
- 

